FROM node:16.9.1-alpine3.14 AS base

WORKDIR /src

FROM base AS build

ARG GOOGLE_CLIENT_ID
ARG STRIPE_PUB_KEY_TEST
ARG API_URL

ENV GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID}
ENV STRIPE_PUB_KEY_TEST=${STRIPE_PUB_KEY_TEST}
ENV API_URL=${API_URL}

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY ./ ./
RUN NODE_ENV=production yarn build

FROM base

WORKDIR /src
COPY package.json .
COPY public/ public/
COPY --from=build /src/node_modules/ node_modules/
COPY --from=build /src/.next ./.next

CMD ["yarn", "start"]
