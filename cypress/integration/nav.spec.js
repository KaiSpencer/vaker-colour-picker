/// <reference types="cypress" />

describe("Test", () => {

    it("Main page has a title", () => {
        cy.visit("http://localhost:3000/")
        cy.contains("Add Subtitles Automatically To A Video With The Powerful Online Videoeditor.")
        .and("be.visible")
    })

    it("Pricing page has a title", () => {
        cy.visit("http://localhost:3000/pricing")
        cy.contains("Pricing")
        .and("be.visible")
    })

    it("Pricing page has a sub-title", () => {
        cy.visit("http://localhost:3000/pricing")
        cy.contains("Choose your subscription model")
        .and("be.visible")
    })

    it("Contact page has a title", () => {
        cy.visit("http://localhost:3000/contact")
        cy.contains("Contact")
        .and("be.visible")
    })

})