/**
 * * ==== FOR FUTURE USE ==== *
 *  const nextTranslate = require('next-translate');
 */

const { API_URL, GOOGLE_CLIENT_ID, STRIPE_PUB_KEY_TEST } = process.env;

module.exports = {
  /**
   * * ==== FOR FUTURE USE ==== *
   * ...nextTranslate(),
   */

  //dev-api.vaker.ai will need to become api.vaker.ai at launch but this should work in Gitlab
  //Why can't we feed one value into the next without getting an "undefined"?
  env: {
    STRIPE_PUB_KEY_TEST,
    GOOGLE_CLIENT_ID,
    API_URL,
    BASE_API_URL: API_URL,
    GOOGLE_AUTH_URL: `${API_URL}/auth/google`,
  },
};
