import React, { ReactNode } from 'react';

interface IBillingInputWrapperProps {
  children: ReactNode;
  label: string;
  optional?: boolean;
}

const BillingInputWrapper = ({
  children,
  label,
  optional,
}: IBillingInputWrapperProps) => (
  <div className="md:grid md:grid-cols-9">
    <div className="md:col-span-3 lg:col-span-3 leading-4">
      <label className="inline text-sm text-font-2 mr-5">
        {label}: {optional && '(optional)'}
      </label>
    </div>
    <div className="md:col-span-6 lg:col-span-6 ">{children}</div>
  </div>
);

export default BillingInputWrapper;
