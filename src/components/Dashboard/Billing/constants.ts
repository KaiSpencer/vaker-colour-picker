import { object, string } from 'zod';

export const INVOICE_LIST = ['March 2021', 'February 2021', 'January 2021'];

export const VALIDATION_MESSAGE = 'Mandatory Information';

export const BILLING_SCHEMA = object({
  company: string().nullable(),
  name: string().length(3, { message: VALIDATION_MESSAGE }),
  lastName: string().length(3, { message: VALIDATION_MESSAGE }),
  address: string().length(3, { message: VALIDATION_MESSAGE }),
  city: string().length(3, { message: VALIDATION_MESSAGE }),
  postCode: string().length(3, { message: VALIDATION_MESSAGE }),
  country: string().length(3, { message: VALIDATION_MESSAGE }),
  vat: string().nullable(),
});
