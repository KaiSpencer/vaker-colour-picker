import { DownloadIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';
import DashboardWrapper from '~/components/ui/DashboardWrapper';
import { Input } from '~/components/ui/Form/Input';
import { Form } from '~/components/ui/Form';
import { Button } from '~/components/ui/Button';
import { GoogleIcon } from '~/components/icons/GoogleIcon';
import { useZodForm } from '~/components/ui/Form/hooks/useZodForm';
import {
  BILLING_SCHEMA,
  INVOICE_LIST,
} from '~/components/Dashboard/Billing/constants';
import BillingInputWrapper from '../../../components/Dashboard/Billing/BillingInputWrapper';

export default function Billing() {
  const router = useRouter();

  const form = useZodForm({
    schema: BILLING_SCHEMA,
  });

  return (
    <DashboardWrapper pageTitle="Billing">
      <div>
        <div className="relative rounded-xl lg:grid lg:grid-cols-6 lg:gap-12 bg-bg-3 shadow-md px-2 py-8 sm:p-8">
          <div className="lg:col-span-4">
            <div className="pt-2 text-font-1 pb-8 px-4 flex items-center justify-center space-x-3">
              <GoogleIcon size={40} />
              <div className="text-xl">
                <span className="text-font-2 text-xs lg:text-base">
                  logged in with Google:
                </span>{' '}
                leonf@gmail.com
              </div>
            </div>
            <div className="rounded-xl shadow-md bg-white p-8 relative z-20">
              <Form form={form} onSubmit={() => {}}>
                <div className="space-y-2">
                  <BillingInputWrapper label="Company Name" optional>
                    <Input
                      type="text"
                      placeholder="The official company name"
                      {...form.register('company')}
                    />
                  </BillingInputWrapper>
                  <BillingInputWrapper label="First Name">
                    <Input
                      type="text"
                      placeholder=""
                      {...form.register('name')}
                    />
                  </BillingInputWrapper>
                  <BillingInputWrapper label="Last Name">
                    <Input
                      className=""
                      type="text"
                      placeholder=""
                      {...form.register('lastName')}
                    />
                  </BillingInputWrapper>
                </div>

                <br />

                <div className="space-y-2">
                  <BillingInputWrapper label="Address">
                    <Input
                      type="text"
                      placeholder=""
                      {...form.register('address')}
                    />
                  </BillingInputWrapper>
                  <BillingInputWrapper label="City">
                    <Input
                      type="text"
                      placeholder=""
                      {...form.register('city')}
                    />
                  </BillingInputWrapper>
                  <BillingInputWrapper label="Post Code">
                    <Input
                      type="text"
                      placeholder=""
                      {...form.register('postCode')}
                    />
                  </BillingInputWrapper>
                  <BillingInputWrapper label="Country">
                    <Input
                      type="text"
                      placeholder=""
                      {...form.register('country')}
                    />
                  </BillingInputWrapper>
                </div>

                <br />

                <div>
                  <BillingInputWrapper label="VAT" optional>
                    <Input
                      type="text"
                      placeholder="Your company VAT number"
                      {...form.register('vat')}
                    />
                  </BillingInputWrapper>
                </div>

                <div className="w-full">
                  <button
                    type="submit"
                    className="mt-12 w-full block max-w-md px-4 py-3 mx-auto text-lg font-bold uppercase text-white bg-main rounded-lg hover:opacity-80"
                  >
                    Update Data
                  </button>
                </div>
              </Form>
            </div>
          </div>

          <div className="rounded-xl lg:col-span-2 bg-white py-4 shadow-md mt-12 lg:mt-0 relative z-20">
            <div className="uppercase font-bold text-lg tracking-wider text-center">
              invoices
            </div>
            <div className="my-4 lg:space-y-3 space-y-2 md:space-y-0 md:grid md:grid-cols-2 lg:block px-8 gap-2">
              {INVOICE_LIST.map(invoice => (
                <div className="rounded text-center py-2 bg-bg-3" key={invoice}>
                  {invoice} <DownloadIcon className="h-4 w-4 inline" />
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="bg-bg-3 rounded-xl shadow-md p-8 sm:max-w-xs mt-12 relative">
          <p className="text-font-2 mb-4 text-sm">
            If you want to delete your entire Vaker.ai account and all
            associated data with it, click here.
          </p>
          <div className="relative z-20">
            <Button variant="danger">Delete Account</Button>
          </div>
        </div>
      </div>
    </DashboardWrapper>
  );
}
