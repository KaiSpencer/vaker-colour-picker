import { ChevronLeftIcon, XIcon } from '@heroicons/react/solid';
import { useRouter } from 'next/router';
import React, {
  Dispatch,
  SetStateAction,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useSelector } from 'react-redux';
import DestructiveActionModal from '~/components/ui/DestructiveActionModal';
import PATHS from '~/constants/paths';
import { RootState } from '~/redux/rootStore';
import { getProject, uploadVideoFile } from '~/utils/projectFetchHelper';
import FinalMessageStep from './FinalMessageStep';
import KeywordsStep from './KeywordsStep';
import SelectLanguageStep from './SelectLanguageStep';

interface DashboardModalProps {
  setShowModal: Dispatch<SetStateAction<boolean>>;
  file: File;
}

function DashboardModal({ setShowModal, file }: DashboardModalProps) {
  const router = useRouter();

  const [progress, setProgress] = useState<number>(1);
  const [openCancelModal, setOpenCancelModal] = useState<boolean>(false);
  const intervalRef = useRef<NodeJS.Timeout | null>(null);

  //Get user's login token from the store
  const user = useSelector(({ user }: RootState) => user);

  // I coded the step process like this so it would be easier to add more steps if needed. Just add the step component in the array of steps,
  // Pass the setIndexStep and setUserVideoOptions to the component to get the data and to control the step process.
  const [indexStep, setIndexStep] = useState<number>(0);
  const [userVideoOptions, setUserVideoOptions] = useState({
    language: '',
    keywords: '',
    file,
  });

  // An array of all steps a user will go through after a file upload begins
  const steps: Array<JSX.Element> = [
    <SelectLanguageStep
      setIndexStep={setIndexStep}
      setUserVideoOptions={setUserVideoOptions}
    />,
    <KeywordsStep
      setIndexStep={setIndexStep}
      setUserVideoOptions={setUserVideoOptions}
    />,
    <FinalMessageStep progress={progress} />,
  ];

  const handleCancelUpload = () => {
    //TODO CANCEL UPLOAD
    setShowModal(false);
  };

  // This is where the file upload happens
  useEffect(() => {
    if (indexStep === steps.length - 1) {
      uploadVideo();
    }
  }, [indexStep, userVideoOptions]);

  // When the user completed all the steps, send the data to the backend
  const uploadVideo = () => {
    uploadVideoFile(userVideoOptions.file, user)
      .then(res => {
        const newProjectUUID = res.data;
        handleProgressBar(newProjectUUID);
      })
      .catch(err => {
        console.error('promise rejected: ', err);
      });
  };

  // Handles async nature of video upload
  function handleProgressBar(newProjectUUID) {
    if (progress < 100)
      intervalRef.current = setTimeout(() => {
        setProgress(prev => prev + 5);
        getProject(newProjectUUID, user)
          .then(res => {
            const uploadProgress: number = res.data.uploadProgress ?? 0;
            uploadProgress > progress ? setProgress(uploadProgress) : '';
            if (uploadProgress >= 99) {
              awaitTranscribe(newProjectUUID);
            } else {
              handleProgressBar(newProjectUUID);
            }
          })
          .catch(err => {
            console.error('promise rejected', err);
          });
      }, 5000);
    return () => {
      intervalRef.current && clearTimeout(intervalRef.current);
    };
  }

  // Handles async nature of video transcription
  function awaitTranscribe(newProjectUUID) {
    intervalRef.current = setTimeout(() => {
      getProject(newProjectUUID, user)
        .then(res => {
          const resObj: Object = res.data;
          if (resObj.hasOwnProperty('fullText')) {
            // Redirect to editor once transcription is complete
            router.push(`${PATHS.DASHBOARD.EDITOR}/${newProjectUUID}`);
          } else {
            awaitTranscribe(newProjectUUID);
          }
        })
        .catch(err => {
          console.log('promise rejected', err);
        });
    }, 5000);
    return () => {
      intervalRef.current && clearTimeout(intervalRef.current);
    };
  }

  // When the modal renders, this blocks the scrolling of the page
  useEffect(() => {
    window.scrollTo(0, 0);
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  return (
    <div className="absolute inset-0 bg-black bg-opacity-30 z-40 h-screen">
      <DestructiveActionModal
        state={openCancelModal}
        title="Cancel Upload"
        setState={setOpenCancelModal}
        description="Do you really want to cancel the video upload?"
        onAccept={handleCancelUpload}
        actionButtonName="Yes"
      />

      <div className="relative top-36 max-w-4xl mx-auto px-2 md:px-8">
        <div className="bg-bg-2 rounded-xl shadow-md bg-white">
          <div className="mt-2 mx-1 sm:mx-6 flex items-center justify-between pt-2 pb-4">
            <button
              className={`text-font-icon hover:text-font-1 ${indexStep === 0 &&
                'invisible'}`}
              onClick={() => indexStep > 0 && setIndexStep(prev => prev - 1)}
            >
              <span className="sr-only">Back</span>
              <ChevronLeftIcon className="h-7 w-7" aria-hidden="true" />
            </button>

            <button className="flex justify-center items-center text-font-icon hover:text-red-600 cursor-pointer">
              <span className="sr-only">Cancel Upload</span>
              <XIcon
                onClick={() => setOpenCancelModal(true)}
                className="h-7 w-7"
                aria-hidden="true"
              />
            </button>
          </div>
          <div className="pb-4 px-4 sm:px-8">
            <div>{steps[indexStep]}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DashboardModal;
