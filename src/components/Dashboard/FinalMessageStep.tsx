import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import ProgressBar from '~/components/ui/ProgressBar';

interface IFinalMessageStepProps {
  progress: number;
}

function FinalMessageStep({ progress }: IFinalMessageStepProps) {
  const router = useRouter();

  useEffect(() => {
    // When the progress is 100 the user will be redirected in the my projects page
    if (progress >= 100) {
      // show user video options);
      // setTimeout(() => router.push(PATHS.DASHBOARD.MY_PROJECTS), 2000);
    }
  }, [progress]);

  return (
    <div className="pb-4">
      <h3 className="text-xl text-font-1">
        {progress < 100
          ? 'Wait until the upload is finished...'
          : 'Upload Complete!'}
      </h3>
      <ProgressBar progress={progress}></ProgressBar>
      {progress >= 100 && (
        <p className="text-font-2">
          You will be redirected as soon as the video is transcribed. This may
          take some time
        </p>
      )}
    </div>
  );
}

export default FinalMessageStep;
