import { InformationCircleIcon } from '@heroicons/react/solid';
import React, { useState } from 'react';
import IStepProps from './types';

function KeywordsStep({ setUserVideoOptions, setIndexStep }: IStepProps) {
  const [keywords, setKeywords] = useState('');

  const handleContinue = () => {
    setUserVideoOptions(prev => ({ ...prev, keywords }));
    setIndexStep(prev => prev + 1);
  };

  return (
    <div>
      <div className="sm:grid sm:grid-cols-9 gap-4">
        <div className="col-span-4">
          <h2 className="uppercase font-bold text-lg text-font-1 tracking-wider">
            optional: spot keywords
          </h2>
          <div className="text-font-icon flex gap-2 mt-2">
            <InformationCircleIcon className="w-8 h-8 flex-grow-0 flex-shrink-0" />
            <div className="text-xs text-font-2">
              Spot Keywords can save you time when editing the subtitles.
              <br />
              Technical terms of difficult names, which most likely will be
              misinterpreted by the AI, can be written down here.
              <br />
              <br />A comma between each keyword is necessary.
            </div>
          </div>
        </div>
        <div className="col-span-5 mt-6 sm:mt-0">
          <textarea
            value={keywords}
            onChange={e => setKeywords(e.target.value)}
            className="w-full h-64 border-1 focus:outline-none focus:ring-1 border-font-icon resize-none shadow-md rounded-xl"
            placeholder="Example, Example2, ..."
          />
        </div>
      </div>
      <button
        type="button"
        onClick={handleContinue}
        className="mt-12 w-full block py-3 text-lg font-bold uppercase text-white bg-[#425B7D] rounded-lg hover:opacity-80"
      >
        Continue
      </button>
    </div>
  );
}

export default KeywordsStep;
