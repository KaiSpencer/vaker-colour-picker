import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import DashboardWrapper from '~/components/ui/DashboardWrapper';
import { DownloadIcon } from '@heroicons/react/outline';
import { PlusIcon } from '@heroicons/react/solid';
import { useSelector, useDispatch } from 'react-redux';
import PATHS from '~/constants/paths';
import { RootState } from '~/redux/rootStore';
import VideoElement from './VideoElement';
import { ISummary } from './types';

function MyProject() {
  const router = useRouter();
  const user = useSelector(({ user }: RootState) => user);
  const [projectDescriptions, setProjectDescriptions] = useState(
    Array<ISummary>(),
  );

  useEffect(() => {
    downloadSummaries();
  }, []);

  // Download array of thumbnail photo examples from the server
  const downloadSummaries = async () => {
    const thirdPartyId = user.tokenData.thirdPartyId;

    // Get a list of UUIDs associated with user
    const response = await fetch(
      `${process.env.API_URL}/videos/fetch-projects/${thirdPartyId}`,
      {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${user.jwtToken}`,
        },
      },
    );
    const projectUuids = await response.json();
    const projectUuidsArray = projectUuids.map(
      (projectUuid) => projectUuid.uuid,
    );

    // Get detailed summary info from a protected route on each project
    await projectUuidsArray.forEach(async (project) => {
      // Fetch the project summary from the server
      const response2 = await fetch(
        `${process.env.API_URL}/videos/fetch-project-summary/${project}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${user.jwtToken}`,
          },
        },
      );
      const display = await response2.json();
      const descrip: string = display[0].description;
      const title: string = display[0].name;
      const tnUrl: string = display[0].thumbnailUrl;
      const addThis: ISummary = {
        uuid: project,
        title,
        description: descrip,
        thumbnailUrl: tnUrl ?? 'https://via.placeholder.com/300',
      };

      const newDescArr: Array<ISummary> = projectDescriptions;
      newDescArr.push(addThis);
      // Ensure deterministic ordering, more fine grained control can be added with CSS
      newDescArr.sort((a, b) => {
        if (a.uuid < b.uuid) {
          return -1;
        } else if (a.uuid > b.uuid) {
          return 1;
        } else {
          return 0;
        }
      });
      //newDescArr.sort();
      console.log(newDescArr);
      setProjectDescriptions(newDescArr.slice());
    });
  };

  return (
    <>
      <DashboardWrapper pageTitle="My Projects">
        <section className="pt-6 px-4 mb-24">
          <div className="mx-auto w-full sm:w-[300px]">
            <button
              onClick={() => router.push(PATHS.DASHBOARD.INDEX)}
              className="bg-gray-50 hover:bg-gray-100 text-font-2 hover:text-font-1 w-full h-[240px] focus:outline-none shadow-md focus:ring-4 focus:ring-blue-dashboard group rounded-lg overflow-hidden flex flex-col justify-center items-center uppercase"
            >
              <PlusIcon className="h-8 w-8" />

              <div className="text-center">
                <div className="tracking-wider">new project</div>
              </div>
            </button>
          </div>
        </section>
        <ul
          role="list"
          className={
            'gap-y-8 gap-x-4 sm:gap-x-12 xl:gap-x-16 px-4 grid grid-cols-1  ' +
            (projectDescriptions.length > 3
              ? 'sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4'
              : projectDescriptions.length === 3
              ? 'sm:grid-cols-2 lg:grid-cols-3'
              : projectDescriptions.length === 2
              ? 'sm:grid-cols-2 max-w-3xl mx-auto'
              : 'grid-col-1 max-w-lg mx-auto')
          }
        >
          {projectDescriptions.map((project, i) => (
            // Fallback values only exist to make page more fault-tolerant
            <>
              <VideoElement
                id={i}
                key={project.uuid}
                uuid={project.uuid}
                thumbnailUrl={project.thumbnailUrl}
                title={project.title}
              />
            </>
          ))}
        </ul>
      </DashboardWrapper>
    </>
  );
}

export default MyProject;
