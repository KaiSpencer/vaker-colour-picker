import React from 'react';
import Select from '~/components/ui/Form/Select';
import IStepProps from './types';

function SelectLanguageStep({ setUserVideoOptions, setIndexStep }: IStepProps) {
  const selectItems = [
    'English US',
    'English UK',
    'English Australia',
    'German',
  ].map(x => ({ label: x, value: x }));

  const handleChange = (language: string) => {
    setUserVideoOptions(prev => ({ ...prev, language }));
    setIndexStep((prev: number) => prev + 1);
  };

  return (
    <div className="pb-8">
      <label className="text-font-1 block text-center text-xl uppercase font-bold">
        choose the language
      </label>
      <Select
        className="max-w-md mx-auto shadow-lg rounded-md"
        items={selectItems}
        initialValue={'1'}
        onChange={lang => lang && handleChange(lang)}
      />
    </div>
  );
}

export default SelectLanguageStep;
