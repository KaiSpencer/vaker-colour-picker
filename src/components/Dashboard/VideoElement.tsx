import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { setProject } from '~/redux/project/project.actions';
import { getLoginToken } from '~/utils/loginHelper';
import { downloadBlob } from 'src/utils/api/getBlob';
import { patchProject } from '~/utils/projectFetchHelper';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '~/redux/rootStore';
import PATHS from '~/constants/paths';
import { DownloadIcon } from '@heroicons/react/solid';

interface IVideoElementProps {
  // ...video's metadata
  id: number;
  title: string;
  uuid: number;
  thumbnailUrl: string;
  hasFinishedRendering?: boolean;
}

const VideoElement = ({
  id,
  title,
  uuid,
  thumbnailUrl,
  hasFinishedRendering,
}: IVideoElementProps) => {
  //Create a router instance
  const router = useRouter();
  const dispatch = useDispatch();

  //Get the user object from the store
  const user = useSelector(({ user }: RootState) => user);

  useEffect(() => {
    if (!user.isLoggedIn) {
      router.push('/');
    }
  }, []);

  // Once the user clicks on the thumbnail the "finished rendering"'s style will disappear
  // This state controls this behavior
  const [finishedRenderingStyle, setFinishedRenderingStyle] =
    useState(hasFinishedRendering);

  const handleDelete = async (id: number) => {
    const confirm = window.confirm(
      'Are you sure you would like to delete this file?',
    );
    if (confirm) {
      console.log('Deleting: ', id);
      // Create axios instance
      const authorizedAxios = axios.create({
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${getLoginToken()}`,
          'Content-Type': 'multipart/form-data',
        },
      });
      // Delete the file
      const res = await authorizedAxios
        .delete(`${process.env.API_URL}/videos/delete/${id}`)
        .then(() => {
          router.push(PATHS.DASHBOARD.MY_PROJECTS);
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };
  const handleDuplicate = (id: number) => {
    // ...
    console.log('duplicate', id);
  };
  const handleDownload = async (id: number) => {
    // ...
    const response = await downloadBlob(id.toString(), getLoginToken());
    const blobData = response.data;

    //Create download link
    const blobURL = URL.createObjectURL(blobData);
    const link = document.createElement('a');
    link.href = blobURL;
    link.download = `${id}.mp4`;
    link.click();
    URL.revokeObjectURL(blobURL);
  };
  const handleRename = (id: number) => {
    // id = uuid
    const newTitle = window.prompt('Please enter a new title', title);
    if (newTitle && newTitle !== title) {
      console.log('renaming', title, newTitle);
      const data = {
        uuid: id,
        name: newTitle,
      };
      patchProject(user, data)
        .then((res) => {
          console.log('renamed', newTitle);
          window.location.reload();
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };
  const handleEdit = (uuid: number) => {
    dispatch(setProject(uuid.toString()));
    router.push(`${PATHS.DASHBOARD.EDITOR}/${uuid}`);
  };

  return (
    <div
      onClick={() => setFinishedRenderingStyle(false)}
      className={`group relative shadow-md rounded-lg overflow-hidden ${
        finishedRenderingStyle && 'ring-4 ring-blue-dashboard'
      }`}
    >
      <div className="w-full h-full aspect-w-10 aspect-h-8 relative">
        <img
          src={thumbnailUrl}
          alt={'thumbnail video ' + title}
          className="object-cover transition-transform transform group-hover:scale-110"
        />
        {/* Colored overlay */}
        <div
          className={`absolute inset-0 bg-main opacity-50 transition-opacity group-hover:opacity-0`}
        >
          {finishedRenderingStyle && (
            <DownloadIcon className="absolute top-1/2 right-1/2 transform translate-x-1/2 -translate-y-1/2 w-16 h-16 text-white" />
          )}
        </div>
        {/* Black overlay on hover  */}
        <div className="absolute inset-0 filter bg-black opacity-0 transition-opacity group-hover:opacity-50"></div>
        {/* Buttons wrapper */}
        <div className="w-full h-full opacity-0 transition-opacity group-hover:opacity-100 text-white grid grid-rows-3">
          <div className="flex justify-center items-center">
            <button onClick={(_, id = 1) => handleDownload(uuid)}>
              <DownloadIcon
                className={`w-8 h-8 ${
                  finishedRenderingStyle &&
                  'text-blue-dashboard transform scale-125'
                }`}
              />
            </button>
          </div>
          <div className="flex justify-center items-center">
            <button
              onClick={() => {
                handleEdit(uuid);
              }}
              className={`font-extrabold px-3 py-1 bg-white rounded-md text-main text-font-1 ${
                finishedRenderingStyle && 'opacity-50'
              }`}
            >
              Edit
            </button>
          </div>
          <div className="grid grid-cols-3 w-full">
            <div className="flex justify-center items-center">
              <button
                className="font-extrabold text-xs"
                onClick={(_, id = 1) => handleDelete(uuid)}
              >
                <img
                  src="/assets/dashboard/delete-icon.svg"
                  className={`w-6 h-6 transform scale-75 mx-auto ${
                    finishedRenderingStyle && 'opacity-50'
                  }`}
                />
                Delete
              </button>
            </div>
            <div className="flex justify-center items-center">
              <button
                className="font-extrabold text-xs"
                onClick={(_, id = 1) => handleRename(uuid)}
              >
                <img
                  src="/assets/dashboard/rename-icon.svg"
                  className={`w-6 h-6 transform scale-125 mx-auto ${
                    finishedRenderingStyle && 'opacity-50'
                  }`}
                />
                Rename
              </button>
            </div>
            <div className="flex justify-center items-center">
              <button
                className="font-extrabold text-xs"
                onClick={(_, id = 1) => handleDuplicate(uuid)}
              >
                <img
                  src="/assets/dashboard/duplicate-icon.svg"
                  className={`w-6 h-6 mx-auto ${
                    finishedRenderingStyle && 'opacity-50'
                  }`}
                />
                Duplicate
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* Video Title */}
      <div className="group-hover:hidden text-main absolute bottom-6 shadow-sm max-w-[75%] whitespace-nowrap h-6 bg-white uppercase text-xs text-font-1 pt-1 pb-2 px-5 rounded-tr-md rounded-br-md font-bold tracking-wider truncate">
        {finishedRenderingStyle ? <span>rendering finished</span> : title}
      </div>
    </div>
  );
};

export default VideoElement;
