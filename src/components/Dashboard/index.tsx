import { CloudUploadIcon } from '@heroicons/react/outline';
import { useState } from 'react';
import DashboardWrapper from '~/components/ui/DashboardWrapper';
import DashboardModal from './DashboardModal';

function Dashboard() {
  const [showModal, setShowModal] = useState(false);
  const [fileDropEnter, setFileDropEnter] = useState(false);
  const [userFile, setUserFile] = useState();

  // Create a state to hold a file
  const triggerUpload = () => {
    // Create a file upload button
    const fileUploadButton = document.createElement('input');
    fileUploadButton.setAttribute('type', 'file');
    fileUploadButton.setAttribute('accept', 'video/*');
    fileUploadButton.addEventListener('change', (event: any) => {
      const file = event.target?.files;
      console.log(file[0]);
      if (file.length == 0) {
        return;
      }
      uploadFile(file[0]);
    });
    fileUploadButton.click();
  };

  function uploadFile(file) {
    setUserFile(file);
    setShowModal(true);
  }

  return (
    <>
      <DashboardWrapper pageTitle="New Project">
        {showModal && (
          <DashboardModal
            setShowModal={setShowModal}
            file={userFile ?? new File([], 'empty.mp4')}
          />
        )}
        <div>
          {/* FILE DROPZONE */}

          <section className="max-w-5xl h-[620px] rounded-xl shadow-md mx-auto bg-white mt-6 relative p-4">
            {/* In this div there will be the actual dropzone */}
            <div
              onClick={triggerUpload}
              onDragEnter={() => setFileDropEnter(true)}
              onDragLeave={() => setFileDropEnter(false)}
              onDragOver={e => {
                e.preventDefault();
              }}
              onDrop={event => {
                event.preventDefault();
                uploadFile(event.dataTransfer.files[0]);
              }}
              className="absolute inset-4 z-20"
            ></div>

            {/* Dropzone styling */}
            <div
              className={`w-full h-full border-dashed border-4 border-gray-200 rounded-lg transition-colors ${fileDropEnter &&
                'bg-gray-100'}`}
            >
              <div className="max-w-3xl mx-auto h-full flex justify-center items-center">
                <div className="flex flex-col items-center">
                  <div className="uppercase text-sm font-bold text-font-icon">
                    {fileDropEnter ? 'drop here' : 'upload your video here'}
                  </div>
                  <CloudUploadIcon className="w-20 h-20 mt-1 text-font-icon" />
                </div>
              </div>
            </div>
          </section>
        </div>
      </DashboardWrapper>
    </>
  );
}

export default Dashboard;
