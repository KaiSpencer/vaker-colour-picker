import { Dispatch, SetStateAction } from 'react';

export default interface IStepProps {
  setUserVideoOptions: Dispatch<
    SetStateAction<{ language: string; keywords: string; file: File }>
  >;
  setIndexStep: Dispatch<SetStateAction<number>>;
}

export interface ISummary {
  uuid: number;
  title: string;
  thumbnailUrl: string;
  description: string;
}
