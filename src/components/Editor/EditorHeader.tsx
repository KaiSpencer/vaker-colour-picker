import React, { Fragment } from 'react';
import NextLink from 'next/link';
import { Disclosure, Popover, Transition } from '@headlessui/react';
import { useRouter } from 'next/router';
import { NAVIGATION } from '../ui/DashboardHeader/constants';
import { ChevronDownIcon, MenuIcon, XIcon } from '@heroicons/react/solid';

interface NavItemProps {
  label: string;
  href: string;
  isDemoEditor: boolean;
}

const NavItem = ({ label, href, isDemoEditor }: NavItemProps) => {
  const labelClasses =
    'uppercase py-4 font-extrabold focus:outline-none focus:ring hover:bg-bg-3 cursor-pointer focus:ring-blue-200';
  console.log('is demo', isDemoEditor);

  if (isDemoEditor) {
    return <a className={labelClasses}>{label}</a>;
  }
  return (
    <NextLink href={href}>
      <a className={labelClasses}>{label}</a>
    </NextLink>
  );
};

interface IPopoverNavProps {
  isDemoEditor: boolean;
}
const PopoverNav: React.FC<IPopoverNavProps> = ({ isDemoEditor }) => {
  return (
    <Popover className="relative z-50">
      {({ open }) => (
        <>
          <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-800 rounded-md hover:text-white hover:bg-[#425B7D] focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
            {open ? (
              <XIcon className="block w-6 h-6" aria-hidden="true" />
            ) : (
              <MenuIcon className="block w-6 h-6" aria-hidden="true" />
            )}
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute mt-3 w-48 transform -translate-x-full md:-translate-x-3/4 left-1/2 ring-1 ring-black ring-opacity-5 bg-white rounded-lg shadow-lg">
              <div className="overflow-hidden">
                <nav className="space-y-4 flex flex-col font-extrabold py-6 text-center">
                  {NAVIGATION.map((item) => (
                    <NavItem
                      key={item.href}
                      href={item.href}
                      label={item.label}
                      isDemoEditor={isDemoEditor}
                    />
                  ))}
                </nav>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};

interface PopoverExportProps {
  setIsSRTModalOpen: (arg0: boolean) => void;
  setIsTXTModalOpen: (arg0: boolean) => void;
  setIsExportModalOpen: (arg0: boolean) => void;
  exportActionsInactive: boolean;
}

const PopoverExport = ({
  setIsSRTModalOpen,
  setIsTXTModalOpen,
  setIsExportModalOpen,
  exportActionsInactive,
}: PopoverExportProps) => {
  return (
    <Popover className="relative z-50">
      {({ open }) => (
        <>
          <Popover.Button className="space-x-1 bg-white pl-4 pr-2 py-2 md:pr-4 md:pl-6 shadow-editor rounded-lg focus:outline-none hover:bg-gray-100 text-font-1 uppercase font-extrabold">
            <div className="flex items-center justify-center h-6">
              <img
                className="relative bottom-0.5 mr-2"
                width="25px"
                height="25px"
                alt="export icon"
                aria-hidden="true"
                src="/assets/editor/export.svg"
              />
              <div className="hidden md:block">exporting</div>
              <ChevronDownIcon
                className="h-4 w-4 hidden md:block"
                aria-hidden="true"
              />
            </div>
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute mt-3 w-[160px] md:w-full transform -translate-x-1/2 left-1/2 ring-1 ring-black ring-opacity-5 bg-white rounded-lg shadow-lg">
              <div className="flex flex-col font-extrabold py-6 text-center">
                <button
                  onClick={() =>
                    exportActionsInactive || setIsSRTModalOpen(true)
                  }
                  className="py-4 font-extrabold focus:outline-none focus:ring hover:bg-bg-3 cursor-pointer focus:ring-blue-200"
                >
                  .SRT file
                </button>
                <button
                  onClick={() =>
                    exportActionsInactive || setIsTXTModalOpen(true)
                  }
                  className="py-4 font-extrabold focus:outline-none focus:ring hover:bg-bg-3 cursor-pointer focus:ring-blue-200"
                >
                  .txt file
                </button>
                <button
                  onClick={() =>
                    exportActionsInactive || setIsExportModalOpen(true)
                  }
                  className="py-4 font-extrabold focus:outline-none focus:ring hover:bg-bg-3 cursor-pointer focus:ring-blue-200"
                >
                  - Export Video -
                </button>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};

interface EditorHeaderProps {
  projectName?: string;
  setIsSRTModalOpen?: (arg0: boolean) => void;
  setIsTXTModalOpen?: (arg0: boolean) => void;
  setIsExportModalOpen?: (arg0: boolean) => void;
  exportPage?: boolean;
  isDemoEditor?: boolean;
}

const EditorHeader = ({
  projectName,
  setIsSRTModalOpen,
  setIsTXTModalOpen,
  setIsExportModalOpen,
  exportPage,
  isDemoEditor = false,
}: EditorHeaderProps) => {
  return (
    <header className="absolute top-0 z-30 w-full bg-bg-3 bg-opacity-70 overflow-visible backdrop-filter backdrop-blur-lg">
      <div className="sm:px-8 lg:px-12 max-w-[1900px] mx-auto overflow-visible py-6">
        <div className="grid grid-cols-3 items-center">
          <div>
            {isDemoEditor ? (
              <img
                className="z-50 cursor-pointer w-28"
                src="/assets/LogoVaker.png"
                alt="Logo"
              />
            ) : (
              <NextLink href="/">
                <img
                  className="z-50 cursor-pointer w-28"
                  src="/assets/LogoVaker.png"
                  alt="Logo"
                />
              </NextLink>
            )}
          </div>

          <div className="text-lg text-center md:text-xl font-extrabold place-self-center max-w-md truncate">
            {projectName}
            {exportPage && <div className="uppercase text-sm">-Export-</div>}
          </div>

          <div className="flex items-center justify-end space-x-6">
            {!exportPage &&
              setIsSRTModalOpen &&
              setIsTXTModalOpen &&
              setIsExportModalOpen && (
                <PopoverExport
                  setIsTXTModalOpen={setIsTXTModalOpen}
                  setIsSRTModalOpen={setIsSRTModalOpen}
                  setIsExportModalOpen={setIsExportModalOpen}
                  exportActionsInactive={isDemoEditor}
                />
              )}
            <PopoverNav isDemoEditor={isDemoEditor} />
          </div>
        </div>
      </div>
    </header>
  );
};

export default EditorHeader;
