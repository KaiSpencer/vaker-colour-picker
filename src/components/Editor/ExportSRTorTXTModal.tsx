import React, { useState } from 'react';
import EditorSwitch from '../ui/Form/EditorSwitch';
import Modal from '../ui/Modal';

interface Props {
	isModalOpen: boolean;
	type: 'SRT' | 'TXT';
	setIsModalOpen: (state: boolean) => void;
	onDownloadFile: (state: boolean) => void;
}

function ExportSRTorTXTModal({
	type,
	isModalOpen,
	setIsModalOpen,
	onDownloadFile,
}: Props) {
	const [withTimestaps, setWithTimestamps] = useState(true);

	const handleContinue = () => {
		console.log(type);
		onDownloadFile(withTimestaps);
		setIsModalOpen(false);
	};

	return (
		<Modal state={isModalOpen} setState={setIsModalOpen}>
			<div className="text-center py-6 pr-10 pl-8 space-y-6">
				<div className="font-extrabold ml-2 space-x-3">
					<EditorSwitch
						isActive={withTimestaps}
						onChange={() => setWithTimestamps((prev) => !prev)}
					/>

					<span className="ml-2">{`Download .${type} with timestamps`}</span>
				</div>

				<button
					onClick={handleContinue}
					className="uppercase text-font-1 rounded bg-white shadow hover:bg-gray-100 py-2 px-6"
				>
					continue
				</button>
			</div>
		</Modal>
	);
}

export default ExportSRTorTXTModal;
