import { XIcon } from '@heroicons/react/solid';
import React, { useRef, useState, useEffect } from 'react';
import ReactPlayer from 'react-player';
import { Button } from '../ui/Button';
import Checkbox from '../ui/Form/Checkbox';
import RadioButton from '../ui/Form/Radiobutton';
import Modal from '../ui/Modal';
import { ReactReduxContext, useSelector } from 'react-redux';
import { RootState } from '~/redux/rootStore';
import { getProjectDetails } from '~/utils/projectFetchHelper';

interface Props {
	isModalOpen: boolean;
	setIsModalOpen: (state: boolean) => void;
	onStartExportVideo: (quality: string, notifyWhenFinishes: boolean) => void;
}

function ExportVideoModal({
	isModalOpen,
	setIsModalOpen,
	onStartExportVideo,
}: Props) {
	//TODO add a value to the user object specifying their subscriber status
	const user = useSelector(({ user }: RootState) => user);
	const project = useSelector(({ project }: RootState) => project);
	const qualityOptions = ['480p', '720p', '1080p', '4k'];
	const reactPlayerRef: React.LegacyRef<ReactPlayer> | undefined = useRef(null);
	const [maxQuality, setMaxQuality] = useState(2);
	const [quality, setQuality] = useState(0);
	const [notifyWhenFinishes, setNotifyWhenFinishes] = useState(false);
	let videoHeight: number = 0;

	useEffect(() => {
		// Use video height + subscriber tier to calculate the max quality
		if (project.selectedProject) {
			getProjectDetails(user, ['ffmpegMetadata'])
				.then(res => {
					const elem = res.data.find(
						(e: any) => e.uuid === project.selectedProject,
					);
					videoHeight = elem.ffmpegMetadata.streams[0].height;
					if (videoHeight >= 2160 && user.subscriberStatus > 1)
						setMaxQuality(3);
					else if (videoHeight >= 1080) setMaxQuality(2);
					else if (videoHeight >= 720) setMaxQuality(1);
					else setMaxQuality(0);
				})
				.catch(err => {
					console.error('error getting project details: ', err);
				});
		}
	}, []);

	const submitFile = () => {
		onStartExportVideo(qualityOptions[quality], notifyWhenFinishes);
	};

	return (
		<Modal state={isModalOpen} setState={setIsModalOpen} full>
			<div>
				<div className="flex justify-end p-2 text-font-icon hover:text-font-1">
					<button onClick={() => setIsModalOpen(false)}>
						<XIcon className="w-4 h-4 float-right" />
					</button>
				</div>
				<div className="pb-10 pt-4 min-h-full w-full inline-flex justify-center items-center">
					<div className="font-extrabold">
						<div className="text-center">
							<div>Quality</div>
							<div>
								<RadioButton
									id="quality-480"
									label="480p"
									setValue={setQuality}
									actualRadioGroupValue={quality}
									value="480p"
									enabled={true}
								/>
							</div>
							<div
								style={{
									display: maxQuality >= 1 ? 'block' : 'none',
								}}
							>
								<RadioButton
									id="quality-720"
									label="720p (HD)"
									setValue={setQuality}
									actualRadioGroupValue={quality}
									value="720p"
									enabled={true}
								/>
							</div>
							<div
								style={{
									display: maxQuality >= 2 ? 'block' : 'none',
								}}
							>
								<RadioButton
									id="quality-1080"
									label="1080p (HD)"
									setValue={setQuality}
									actualRadioGroupValue={quality}
									value="1080p"
									enabled={true}
								/>
							</div>
							<div
								style={{
									display: maxQuality >= 3 ? 'block' : 'none',
								}}
							>
								<RadioButton
									id="quality-4k"
									label="4K"
									setValue={setQuality}
									actualRadioGroupValue={quality}
									value="4k"
									enabled={false}
								/>
							</div>
						</div>
						<div className="text-center mt-6">
							<Checkbox
								value={notifyWhenFinishes}
								handleClick={() => setNotifyWhenFinishes(prev => !prev)}
								id="notify-checkbox"
								label="Notify when finished via email"
							/>
						</div>

						{/* Video Preview */}
						<div className="max-w-xl px-2 sm:px-0 mt-8">
							<div className="relative">
								<ReactPlayer
									onDuration={() => {
										reactPlayerRef.current?.seekTo(Math.random());
									}}
									ref={reactPlayerRef}
									width="100%"
									style={{ borderRadius: '5px', overflow: 'hidden' }}
									height="100%"
									url="/assets/test.mp4"
								/>
								<div className="absolute transform -translate-x-1/2 -translate-y-1/2  top-1/2 left-1/2">
									<img
										src="/assets/editor/play-button.svg"
										height="60px"
										width="60px"
										className="rounded-full"
									/>
								</div>
							</div>
						</div>

						<div className="w-52 mt-8 mx-auto self-end">
							<Button className="uppercase" onClick={submitFile}>
								start export video
							</Button>
						</div>
					</div>
				</div>
			</div>
		</Modal>
	);
}

export default ExportVideoModal;
