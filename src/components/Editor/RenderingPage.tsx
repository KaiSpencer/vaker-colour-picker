import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import EditorHeader from '~/components/Editor/EditorHeader';
import VideoPlayer from '~/components/Editor/VideoPlayer';
import DestructiveActionModal from '~/components/ui/DestructiveActionModal';
import Checkbox from '~/components/ui/Form/Checkbox';
import ProgressBar from '~/components/ui/ProgressBar';
import { RootState } from '~/redux/rootStore';
import { downloadBlob } from 'src/utils/api/getBlob';
import PATHS from '~/constants/paths';

function RenderingPage() {
  const router = useRouter();

  const [emailNotification, setEmailNotification] = useState<boolean>(false);
  const [progress, setProgress] = useState<number>(0);
  const [showCancelModal, setShowCancelModal] = useState<boolean>(false);
  const [videoUrl, setVideoUrl] = useState<string>('');

  const intervalRef = useRef<NodeJS.Timeout | null>(null);

  const hasFinishedRendering = progress >= 10;
  // Get project data from store
  const projectData = useSelector(({ project }: RootState) => project);
  const user = useSelector(({ user }: RootState) => user);
  const token = user.jwtToken;
  const UUID = projectData.selectedProject;

  // This emulates increasing of the progress bar
  useEffect(() => {
    if (progress <= 100)
      intervalRef.current = setTimeout(
        () => setProgress((prev) => prev + 1),
        100,
      );
    return () => {
      intervalRef.current && clearTimeout(intervalRef.current);
    };
  }, [progress]);

  const handleEmailNotification = () => {
    //TODO Handle email notification communication logic with backend
    setEmailNotification((prev) => !prev);
  };

  const handleCancelExport = () => {
    //TODO handle user that cancels video export
    router.push(PATHS.DASHBOARD.EDITOR);
  };

  const handleEditClick = () => {
    router.push(`${PATHS.DASHBOARD.EDITOR}/${UUID}`);
  };

  const beginStream = async () => {
    const response = await downloadBlob(UUID, token);
    const blobData = response.data;

    //Stream the video
    const blobURL = URL.createObjectURL(blobData);
    setVideoUrl(blobURL);
  };

  const handleDownloadVideo = async () => {
    const response = await downloadBlob(UUID, token);
    const blobData = response.data;

    //Create download link
    const blobURL = URL.createObjectURL(blobData);
    const link = document.createElement('a');
    link.href = blobURL;
    link.download = `${UUID}.mp4`;
    link.click();
    URL.revokeObjectURL(blobURL);
  };

  const handleCancelRendering = () => {
    //TODO
  };

  return (
    <>
      <EditorHeader
        exportPage={!hasFinishedRendering}
        projectName="projectName"
      />
      <DestructiveActionModal
        onAccept={handleCancelRendering}
        state={showCancelModal}
        setState={setShowCancelModal}
        title="Cancel Rendering"
        actionButtonName="Cancel Rendering"
        description="Are you sure you want to cancel the rendering?"
      />
      <main className="max-w-2xl mx-auto px-4">
        <div className="pt-36 pb-24">
          {!hasFinishedRendering ? (
            <div className="space-y-8">
              <div className="sm:px-12 lg:px-16 py-2 flex justify-center items-center shadow-md rounded-md">
                <div className="flex-shrink-0">
                  <img
                    src="/assets/editor/ai-icon.svg"
                    height="75px"
                    width="75px"
                  />
                </div>
                <div className="rounded-md">
                  <h2 className="text-font-1 font-extrabold">
                    Great! Your rendering has started!
                  </h2>
                  <p className="text-font-2">
                    You can close this tab if you like. Once ready, you can find
                    it under "my projects".
                  </p>
                </div>
              </div>
              <div className="text-center">
                <Checkbox
                  labelClassName="font-extrabold"
                  handleClick={handleEmailNotification}
                  label="Notify me when finished via email"
                  value={emailNotification}
                  id="email-notification-checkbox"
                />
              </div>
              <div>
                <div className="h-96 bg-black w-full flex items-center px-12 rounded-md">
                  <ProgressBar
                    exportPage
                    progress={progress}
                    handleCancel={() => setShowCancelModal(true)}
                  />
                </div>
              </div>
            </div>
          ) : (
            <>
              <div className="flex flex-col items-center">
                <button
                  onClick={handleDownloadVideo}
                  className="cursor-pointer inline-block p-4 shadow rounded-md hover:bg-gray-100"
                >
                  <img
                    src="/assets/editor/download.svg"
                    alt="download icon"
                    aria-hidden
                    className="w-14 h-14"
                  />
                </button>
                <div className="uppercase font-extrabold text-font-icon mt-1">
                  download video here
                </div>
              </div>
              <div className="mt-12" onLoad={beginStream}>
                <VideoPlayer videoUrl={videoUrl} />
              </div>
              <div className="flex justify-center mt-16">
                <button
                  className="cursor-pointer uppercase shadow rounded-md hover:bg-gray-100 font-extrabold px-12 py-2"
                  onClick={handleEditClick}
                >
                  Edit
                </button>
              </div>
            </>
          )}
        </div>
      </main>
    </>
  );
}

export default RenderingPage;
