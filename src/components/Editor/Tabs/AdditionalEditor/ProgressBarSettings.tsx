import React, { FC, useEffect, useState } from 'react';
import Popup from 'reactjs-popup';
import { AlphaPicker } from 'react-color';
import { Slider } from 'antd';
import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/solid';
import { IEditorProgressBarStyles } from '~/redux/editor/types';
import VideoProgressBar from '~/components/Editor/VideoPlayer/VideoProgressBar';
import EditorSwitch from '~/components/ui/Form/EditorSwitch';
import progressbar_styles from '~/constants/styles/progressbar';
import { colorToString, stringToColor, rgba2hex } from '~/utils/colorToString';
import { TwitterPicker } from '../SelectAdvancedSettings/AdvancedSettings/TwitterPicker';

interface IProgressBarSettingsProps {
  style: IEditorProgressBarStyles;
  onChange: Function;
  headline?: String;
}

const ProgressBarSettings: FC<IProgressBarSettingsProps> = ({
  style,
  headline,
  onChange: handleChange,
}) => {
  const [menuOpen, setMenuOpen] = useState(false);

  const handleInput = (event: any) => {
    const { name, value } = event.target;

    handleChange(name, value);
  };

  const handleColorChange = (color, name) => {
    handleChange(name, colorToString(color));
  };

  const handleColorAlphaChange = (color, name) => {
    handleChange(name, colorToString(color));
  };

  const changeTemplate = (dval) => {
    let val = style.template + dval;
    if (val < 0) {
      val = progressbar_styles.length + val; //if -1 then it's going to go to the last one
    } else if (val > progressbar_styles.length - 1) {
      val = 0;
    }

    for (const property in progressbar_styles[val]) {
      handleChange(property, progressbar_styles[val][property]);
    }

    handleChange('template', val);
  };

  useEffect(() => {
    if (!style?.active) {
      setMenuOpen(false);
    }
  }, [style?.active]);

  return (
    <div>
      <div className="mt-5 sm:w-[440px] mx-auto">
        <div className="text-center grid grid-cols-12">
          <div className="col-span-2 justify-self-end">
            <EditorSwitch
              isActive={style?.active}
              onChange={() => handleChange('active', !style?.active)}
              name="active"
            />
          </div>
          <div className="text-font-2 font-extrabold text-base select-none flex-1 col-span-8">
            {headline}
          </div>
          <div className="col-span-2" />
        </div>

        <div className="pt-5">
          <div className="grid grid-cols-12">
            <ChevronLeftIcon
              className="h-5 w-5 justify-self-center place-self-center cursor-pointer ease-in-out transform  hover:scale-150 
						transition duration-75 transform active:-translate-x-2"
              onClick={() => changeTemplate(-1)}
            />

            <div className="col-span-10 w-full bg-white rounded-xl mx-auto px-6 py-4 shadow-editor">
              <VideoProgressBar
                percent={45}
                backgroundColor={style.backgroundColor}
                progressColor={style.progressColor}
                borderRadius={style.borderRounded}
              />
            </div>

            <ChevronRightIcon
              className="h-5 w-5 justify-self-center place-self-center cursor-pointer ease-in-out transform  hover:scale-150 
						transition duration-75 transform active:translate-x-2"
              onClick={() => changeTemplate(1)}
            />
          </div>

          <div
            className="text-xs text-font-2 hover:text-font-1 justify-center mt-2 cursor-pointer select-none flex items-center mb-5"
            onClick={() => setMenuOpen((prev) => !prev)}
          >
            <div>Advanced Settings</div>
            {menuOpen ? (
              <ChevronDownIcon className="inline h-4 w-4" />
            ) : (
              <ChevronRightIcon className="inline h-4 w-4" />
            )}
          </div>
          {menuOpen && (
            <div className="mt-2">
              <div className="flex space-x-2">
                <div className="flex-1">
                  <TwitterPicker
                    selectedColor={style.progressColor || '#ffffff'}
                    handleChange={(name, value) => handleChange(name, value)}
                    pickerName={'progressColor'}
                  />
                </div>
                <div className="flex-1">
                  <TwitterPicker
                    selectedColor={style.backgroundColor}
                    handleChange={(name, value) => handleChange(name, value)}
                    pickerName={'backgroundColor'}
                  />
                </div>

                <div className="rounded-lg border border-main px-2 flex-1">
                  <Slider
                    className="w-full h-full"
                    min={0}
                    value={style.borderRounded}
                    max={50}
                    onChange={(value: number) =>
                      handleInput({ target: { name: 'borderRounded', value } })
                    }
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ProgressBarSettings;
