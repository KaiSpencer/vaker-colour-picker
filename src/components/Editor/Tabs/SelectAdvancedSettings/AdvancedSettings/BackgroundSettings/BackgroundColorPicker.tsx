import React from 'react';
import { TwitterPicker } from '../TwitterPicker';

interface IColorPickerProps {
  handleChange: (name: string, value: string) => void;
  configuration: any;
}

const BackgroundColorPicker: React.FC<IColorPickerProps> = ({
  handleChange,
  configuration,
}) => {
  return (
    <div className="flex-1">
      <TwitterPicker
        selectedColor={configuration?.backgroundColor || '#43597D'}
        handleChange={handleChange}
        pickerName={'backgroundColor'}
      />
    </div>
  );
};

export default BackgroundColorPicker;
