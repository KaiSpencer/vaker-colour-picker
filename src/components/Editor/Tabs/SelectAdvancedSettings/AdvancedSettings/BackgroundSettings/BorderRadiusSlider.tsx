import { Slider } from 'antd';
import React from 'react';

interface IBorderRadiusSliderProps {
  borderRadiusOpacity: any;
  configuration: any;
  createSliderHandler: Function;
}

const BorderRadiusSlider: React.FC<IBorderRadiusSliderProps> = ({
  borderRadiusOpacity,
  configuration,
  createSliderHandler,
}) => {
  return (
    <div
      className="bg-white border flex-1 border-[#323438] p-1 rounded-lg flex"
      style={{ opacity: borderRadiusOpacity }}
    >
      <Slider
        onChange={createSliderHandler('borderRounded')}
        value={configuration?.borderRounded}
        min={0}
        max={25}
        className="w-full h-full relative bottom-1"
      />
    </div>
  );
};

export default BorderRadiusSlider;
