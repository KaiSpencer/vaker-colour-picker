import React from 'react';
import EditorSwitch from '~/components/ui/Form/EditorSwitch';

interface IToggleBackgroundProps {
  configuration: any;
  handleChange: Function;
}

const ToggleBackground: React.FC<IToggleBackgroundProps> = ({
  configuration,
  handleChange,
}) => {
  return (
    <div className="place-self-center">
      <EditorSwitch
        isActive={configuration?.backgroundActive}
        onChange={() =>
          handleChange('backgroundActive', !configuration.backgroundActive)
        }
        name="backgroundActive"
      />
    </div>
  );
};

export default ToggleBackground;
