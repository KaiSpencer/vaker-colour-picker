import React from 'react';
import { TwitterPicker } from '../TwitterPicker';
import BorderRadiusSlider from './BorderRadiusSlider';
import ToggleBackground from './ToggleBackground';

interface IBackgroundSettingsProps {
  configuration: any;
  handleChange: (name: string, value: string) => void;
  handleInput: Function;
  borderRadiusOpacity: any;
  createSliderHandler: Function;
}

const BackgroundSettings: React.FC<IBackgroundSettingsProps> = ({
  configuration,
  handleChange,
  handleInput,
  borderRadiusOpacity,
  createSliderHandler,
}) => {
  return (
    <div className="mt-10 max-w-lg mx-auto flex space-x-2 px-1 h-8">
      <ToggleBackground
        configuration={configuration}
        handleChange={handleChange}
      />
      <div className="pr-2 font-bold text-gray-500 place-self-center">
        Background:
      </div>
      <div>
        <TwitterPicker
          selectedColor={configuration.backgroundColor}
          handleChange={handleChange}
          pickerName={'backgroundColor'}
        />
      </div>
      <BorderRadiusSlider
        borderRadiusOpacity={borderRadiusOpacity}
        configuration={configuration}
        createSliderHandler={createSliderHandler}
      />
    </div>
  );
};

export default BackgroundSettings;
