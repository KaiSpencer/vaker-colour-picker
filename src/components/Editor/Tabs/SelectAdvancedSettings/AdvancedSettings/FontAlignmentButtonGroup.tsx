import React, { MouseEventHandler } from 'react';
import {
  AiOutlineAlignCenter,
  AiOutlineAlignLeft,
  AiOutlineAlignRight,
} from 'react-icons/ai';

interface IFontAlignmentButtonGroupProps {
  configuration: any;
  handleInput: MouseEventHandler<HTMLButtonElement>;
  handleChildButtonInput: MouseEventHandler<SVGAElement>;
}

const FontAlignmentButtonGroup: React.FC<IFontAlignmentButtonGroupProps> = ({
  configuration,
  handleInput,
  handleChildButtonInput,
}) => {
  return (
    <div className="grid grid-cols-3 border border-[#323438] rounded-lg text-lg">
      <button
        name="textAlign"
        value="left"
        onClick={handleInput}
        className={`${
          configuration?.textAlign === 'left'
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center border-r rounded-l-lg border-[#323438] focus:outline-none`}
      >
        <AiOutlineAlignLeft onClick={handleChildButtonInput} />
      </button>
      <button
        name="textAlign"
        value="center"
        onClick={handleInput}
        className={`${
          configuration?.textAlign === 'center'
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center border-r border-[#323438] focus:outline-none`}
      >
        <AiOutlineAlignCenter onClick={handleChildButtonInput} />
      </button>
      <button
        name="textAlign"
        value="right"
        onClick={handleInput}
        className={`${
          configuration?.textAlign === 'right'
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center rounded-r-lg focus:outline-none`}
      >
        <AiOutlineAlignRight onClick={handleChildButtonInput} />
      </button>
    </div>
  );
};

export default FontAlignmentButtonGroup;
