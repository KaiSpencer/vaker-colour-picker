import React, { useCallback } from 'react';

interface IFontCaseButtonGroupProps {
  configuration: any;
  handleChange: (name: string, value: string) => void;
}

const FontCaseButtonGroup: React.FC<IFontCaseButtonGroupProps> = ({
  configuration,
  handleChange,
}) => {
  const onClick = useCallback(
    ({ target }) => {
      const { name, value } = target;

      handleChange(name, value);
    },
    [handleChange],
  );

  return (
    <div className="grid grid-cols-3 border border-[#323438] rounded-lg text-lg">
      <button
        name="textTransform"
        value="none"
        onClick={onClick}
        className={`${
          configuration?.textTransform === 'none'
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center border-r rounded-l-lg border-[#323438] focus:outline-none`}
      >
        -
      </button>
      <button
        name="textTransform"
        value="uppercase"
        onClick={onClick}
        className={`${
          configuration?.textTransform === 'uppercase'
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center border-r hover:bg-gray-100 border-[#323438] focus:outline-none`}
      >
        AB
      </button>
      <button
        name="textTransform"
        value="lowercase"
        onClick={onClick}
        className={`${
          configuration?.textTransform === 'lowercase'
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center rounded-r-lg  focus:outline-none`}
      >
        ab
      </button>
    </div>
  );
};

export default FontCaseButtonGroup;
