import React, { MouseEventHandler } from 'react';
import { BsTypeBold, BsTypeItalic, BsTypeUnderline } from 'react-icons/bs';

interface IFontDecorationButtonGroupProps {
  configuration: any;
  handleBooleanInput: MouseEventHandler<HTMLButtonElement>;
  handleChildButtonBooleanInput: MouseEventHandler<SVGAElement>;
}

const FontDecorationButtonGroup: React.FC<IFontDecorationButtonGroupProps> = ({
  configuration,
  handleBooleanInput,
  handleChildButtonBooleanInput,
}) => {
  return (
    <div className="grid grid-cols-3 border border-[#323438] rounded-lg text-lg">
      <button
        name="bold"
        value="false"
        onClick={handleBooleanInput}
        className={`${
          configuration?.bold === true ? 'bg-gray-200' : 'hover:bg-gray-100'
        } flex justify-center items-center border-r rounded-l-lg border-[#323438] focus:outline-none`}
      >
        <BsTypeBold onClick={handleChildButtonBooleanInput} />
      </button>
      <button
        name="italic"
        value="false"
        onClick={handleBooleanInput}
        className={`${
          configuration?.italic === true ? 'bg-gray-200' : 'hover:bg-gray-100'
        } flex justify-center items-center border-r border-[#323438] focus:outline-none`}
      >
        <BsTypeItalic onClick={handleChildButtonBooleanInput} />
      </button>
      <button
        name="underline"
        value="false"
        onClick={handleBooleanInput}
        className={`${
          configuration?.underline === true
            ? 'bg-gray-200'
            : 'hover:bg-gray-100'
        } flex justify-center items-center rounded-r-lg border-[#323438] focus:outline-none`}
      >
        <BsTypeUnderline onClick={handleChildButtonBooleanInput} />
      </button>
    </div>
  );
};

export default FontDecorationButtonGroup;
