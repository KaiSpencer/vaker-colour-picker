import { IFontFamily } from './types';

export const FONT_FAMILIES: IFontFamily[] = [
  {
    label: 'Arial',
    fontFamily: 'arial',
  },
  {
    label: 'Roboto',
    fontFamily: 'roboto',
  },
  {
    label: 'Avenir',
    fontFamily: 'avenir',
  },
  {
    label: 'Helvetica',
    fontFamily: 'helvetica',
  },
  {
    label: 'Raleway',
    fontFamily: 'raleway',
  },
  {
    label: 'Open Sans',
    fontFamily: 'opensans',
  },
  {
    label: 'Bona Novay',
    fontFamily: 'bonanova',
  },
  {
    label: 'Yomogi',
    fontFamily: 'yomogi',
  },
  {
    label: 'Time Burner',
    fontFamily: 'timeburner',
  },
  {
    label: 'Quicksand',
    fontFamily: 'quicksand',
  },
];
