import React, { ChangeEventHandler } from 'react';
import { FONT_FAMILIES } from './constants';

interface IFontDropdownProps {
  value: any;
  handleInput: ChangeEventHandler<HTMLSelectElement>;
}

const FontDropdown: React.FC<IFontDropdownProps> = ({ value, handleInput }) => {
  return (
    <select
      value={value}
      className="bg-white border border-[#323438] px-1 h-8 py-0 rounded-lg"
      name="font"
      onChange={handleInput}
    >
      {FONT_FAMILIES.map(({ label, fontFamily }) => (
        <option key={label} value={fontFamily} style={{ fontFamily }}>
          {label}
        </option>
      ))}
    </select>
  );
};

export default FontDropdown;
