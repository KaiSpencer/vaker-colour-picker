export interface IFontFamily {
  label: string;
  fontFamily: string;
  value?: string;
}
