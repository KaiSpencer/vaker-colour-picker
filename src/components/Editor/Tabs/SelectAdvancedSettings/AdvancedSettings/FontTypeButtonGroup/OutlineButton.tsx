import React, { MouseEventHandler } from 'react';

interface IOutlineButtonProps {
  handleInput: MouseEventHandler<HTMLButtonElement>;
  configuration: any;
}

const OutlineButton: React.FC<IOutlineButtonProps> = ({
  handleInput,
  configuration,
}) => {
  return (
    <button
      onClick={handleInput}
      name="fontType"
      value="outline"
      style={{
        textShadow:
          '0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black',
      }}
      className={`${
        configuration?.fontType === 'outline'
          ? 'bg-gray-200'
          : 'bg-white hover:bg-gray-50'
      } focus:outline-none text-outline py-4 text-lg font-extrabold h-full shadow-editor text-center text-white rounded-md cursor-pointer`}
    >
      Outline
    </button>
  );
};

export default OutlineButton;
