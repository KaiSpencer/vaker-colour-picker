import React, { MouseEventHandler } from 'react';

interface IShadowButtonProps {
  handleInput: MouseEventHandler<HTMLButtonElement>;
  configuration: any;
}

const ShadowButton: React.FC<IShadowButtonProps> = ({
  handleInput,
  configuration,
}) => {
  return (
    <button
      onClick={handleInput}
      name="fontType"
      value="shadow"
      style={{ textShadow: '2px 2px 5px black' }}
      className={`${
        configuration?.fontType === 'shadow'
          ? 'bg-gray-200'
          : 'bg-white hover:bg-gray-50'
      } focus:outline-none py-4 text-lg font-extrabold shadow-editor h-full text-center text-white rounded-md cursor-pointer`}
    >
      Shadow
    </button>
  );
};

export default ShadowButton;
