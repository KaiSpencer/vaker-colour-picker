import React, { MouseEventHandler } from 'react';

interface ISplitBoxButtonProps {
  handleChildButtonInput: MouseEventHandler<HTMLSpanElement>;
  handleInput: MouseEventHandler<HTMLButtonElement>;
  configuration: any;
}

const SplitBoxButton: React.FC<ISplitBoxButtonProps> = ({
  handleChildButtonInput,
  handleInput,
  configuration,
}) => {
  return (
    <button
      onClick={handleInput}
      name="fontType"
      value="splitBox"
      className={`${
        configuration?.fontType === 'splitBox'
          ? 'bg-gray-200'
          : 'bg-white hover:bg-gray-50'
      } focus:outline-none text-lg shadow-editor py-1 text-center text-white rounded-md cursor-pointer`}
    >
      <span className="bg-black" onClick={handleChildButtonInput}>
        Split
        <br /> box
      </span>
    </button>
  );
};

export default SplitBoxButton;
