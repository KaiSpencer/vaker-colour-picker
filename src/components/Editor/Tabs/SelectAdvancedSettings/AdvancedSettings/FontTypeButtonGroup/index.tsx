import React, { MouseEventHandler } from 'react';
import OutlineButton from './OutlineButton';
import ShadowButton from './ShadowButton';
import SplitBoxButton from './SplitBoxButton';

interface IFontTypeButtonGroupProps {
  handleInput: MouseEventHandler<HTMLButtonElement>;
  configuration: any;
  handleChildButtonInput: MouseEventHandler<HTMLSpanElement>;
}

const FontTypeButtonGroup: React.FC<IFontTypeButtonGroupProps> = ({
  handleChildButtonInput,
  handleInput,
  configuration,
}) => {
  return (
    <div className="mx-auto sm:w-[440px] grid grid-cols-3 gap-8 mt-8 items-center justify-center">
      <OutlineButton configuration={configuration} handleInput={handleInput} />
      <SplitBoxButton
        configuration={configuration}
        handleChildButtonInput={handleChildButtonInput}
        handleInput={handleInput}
      />
      <ShadowButton configuration={configuration} handleInput={handleInput} />
    </div>
  );
};

export default FontTypeButtonGroup;
