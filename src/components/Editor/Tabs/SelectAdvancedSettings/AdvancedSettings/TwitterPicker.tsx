import React, { useState } from 'react';
import { TwitterPicker as LibTwitterPicker } from 'react-color';

interface ITwitterPickerProps {
  selectedColor: string;
  handleChange: (name: string, value: string) => void;
  pickerName: string;
}
export const TwitterPicker = ({
  selectedColor,
  handleChange,
  pickerName,
}: ITwitterPickerProps) => {
  const [pickerOpen, setPickerOpen] = useState(false);

  const PICKER_COLORS = [
    '#FFFFFF',
    '#000000',
    '#D9E3F0',
    '#F47373',
    '#697689',
    '#37D67A',
    '#2CCCE4',
    '#555555',
    '#dce775',
    '#ff8a65',
    '#ba68c8',
  ];

  return (
    <>
      <div
        style={{
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        }}
        onClick={() => setPickerOpen(!pickerOpen)}
      >
        <div
          style={{
            width: '36px',
            height: '100%',
            minHeight: '30px',
            borderRadius: '2px',
            backgroundColor: selectedColor,
          }}
        />
      </div>
      {pickerOpen && (
        <div style={{ position: 'absolute' }}>
          <LibTwitterPicker
            onChange={(colour) => {
              handleChange(pickerName, colour.hex);
            }}
            colors={PICKER_COLORS}
          />
        </div>
      )}
    </>
  );
};
