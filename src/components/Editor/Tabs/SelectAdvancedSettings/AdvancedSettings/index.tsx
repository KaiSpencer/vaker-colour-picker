import { Slider } from 'antd';
import React from 'react';
import BackgroundSettings from './BackgroundSettings';
import FontAlignmentButtonGroup from './FontAlignmentButtonGroup';
import FontCaseButtonGroup from './FontCaseButtonGroup';
import FontDecorationButtonGroup from './FontDecorationButtonGroup';
import FontDropdown from './FontDropdown';
import FontTypeButtonGroup from './FontTypeButtonGroup';
import { TwitterPicker } from './TwitterPicker';

interface IAdvancedSettingsProps {
  configuration: any;
  handleInput: any;
  borderRadiusOpacity: any;
  handleChange: (name: string, value: string | number | boolean) => void;
}

const AdvancedSettings: React.FC<IAdvancedSettingsProps> = ({
  borderRadiusOpacity,
  configuration,
  handleInput,
  handleChange,
}) => {
  const handleChildButtonInput = (event: any) => {
    var parentElement = event.target.parentElement;
    var i = 0; //retries

    while (parentElement.tagName != 'BUTTON' && i != 10) {
      parentElement = parentElement.parentElement;
      i++;
    }

    const { name, value } = parentElement;

    handleChange(name, value);
  };

  const handleBooleanInput = (event: any) => {
    const { name, value } = event.target;
    var isTrueSet = value === 'true';
    event.target.value = (!isTrueSet).toString();

    handleChange(name, !isTrueSet);
  };

  const handleChildButtonBooleanInput = (event: any) => {
    var parentElement = event.target.parentElement;
    var i = 0; //retries

    while (parentElement.tagName != 'BUTTON' && i != 10) {
      parentElement = parentElement.parentElement;
      i++;
    }
    const { name, value } = parentElement;
    var isTrueSet = value === 'true';
    parentElement.value = (!isTrueSet).toString();

    handleChange(name, !isTrueSet);
  };

  const createSliderHandler = (name: string) => {
    return (value: number) => {
      handleChange(name, value);
    };
  };

  return (
    <>
      <div className="mx-auto sm:w-[440px] space-y-3">
        <div className="mx-auto grid grid-flow-row grid-cols-3 gap-x-3 px-3 gap-y-3">
          <FontDropdown value={configuration?.font} handleInput={handleInput} />
          <div className="bg-white border border-[#323438] h-8 p-1 rounded-lg flex items-center">
            <div className="pr-2 place-self-center text-base">Size:</div>
            <Slider
              onChange={createSliderHandler('fontSize')}
              value={configuration?.fontSize}
              step={2}
              min={8}
              max={36}
              className="w-full h-full relative top-1.5"
            />
          </div>
          <TwitterPicker
            selectedColor={configuration?.fontColor || '#ffffff'}
            handleChange={handleChange}
            pickerName={'fontColor'}
          />
        </div>
        <div className="mx-auto grid grid-flow-row grid-cols-3 gap-x-3 px-3 gap-y-3 h-8">
          <FontDecorationButtonGroup
            configuration={configuration}
            handleBooleanInput={handleBooleanInput}
            handleChildButtonBooleanInput={handleChildButtonBooleanInput}
          />
          <FontAlignmentButtonGroup
            handleInput={handleInput}
            handleChildButtonInput={handleChildButtonInput}
            configuration={configuration}
          />
          <FontCaseButtonGroup
            handleChange={handleChange}
            configuration={configuration}
          />
        </div>
      </div>
      <BackgroundSettings
        configuration={configuration}
        handleChange={handleChange}
        handleInput={handleInput}
        borderRadiusOpacity={borderRadiusOpacity}
        createSliderHandler={createSliderHandler}
      />
      <FontTypeButtonGroup
        configuration={configuration}
        handleChildButtonInput={handleChildButtonInput}
        handleInput={handleInput}
      />
    </>
  );
};

export default AdvancedSettings;
