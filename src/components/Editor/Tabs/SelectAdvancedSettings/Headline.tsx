import React from 'react';
import EditorSwitch from '~/components/ui/Form/EditorSwitch';

interface IHeadlineProps {
  configuration: any;
  headline: string;
  handleChange: Function;
}

const Headline: React.FC<IHeadlineProps> = ({
  configuration,
  handleChange,
  headline,
}) => {
  return (
    <div className="mx-auto sm:sm:w-[440px]">
      <div className="text-center grid grid-cols-12">
        <div className="col-span-2 justify-self-end">
          <EditorSwitch
            isActive={configuration?.active}
            onChange={() => handleChange('active', !configuration?.active)}
          />
        </div>
        <div className="col-span-8">
          <div className="text-font-2 font-extrabold text-base text-center select-none flex-1">
            {headline}
          </div>
        </div>
        <div className="col-span-2" />
      </div>
    </div>
  );
};

export default Headline;
