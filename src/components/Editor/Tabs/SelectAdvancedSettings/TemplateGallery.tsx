import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/solid';
import React from 'react';

interface ITemplateProps {
  changeTemplate: Function;
  styles: any;
  contentEditable: any;
  onContentChange: any;
  configuration: any;
  menuOpen: boolean;
  setMenuOpen: any;
}

const Template: React.FC<ITemplateProps> = ({
  changeTemplate,
  configuration,
  contentEditable,
  menuOpen,
  onContentChange,
  setMenuOpen,
  styles,
}) => {
  return (
    <div className="mx-auto sm:w-[440px]">
      <div className="grid grid-cols-12">
        <ChevronLeftIcon
          className="h-5 w-5 justify-self-center place-self-center cursor-pointer ease-in-out transform  hover:scale-150 
      transition duration-75 transform active:-translate-x-2"
          onClick={() => changeTemplate(-1)}
        />

        <div className="col-span-10 select-none">
          <div
            style={{
              background:
                'repeating-linear-gradient(-45deg, rgba(0, 0, 0, 0.005), rgba(0, 0, 0, 0.005) 10px, rgba(255, 255, 255) 10px, rgba(255, 255, 255) 55px)',
            }}
            className="rounded-xl mx-auto px-8 py-4 shadow-editor truncate flex justify-center"
          >
            <div
              style={styles}
              className="bg-gray-700 p-3 text-white px-6 text-center focus:outline-none relative"
              contentEditable={contentEditable}
              onBlur={event =>
                onContentChange?.(event.currentTarget.textContent!)
              }
            >
              {configuration.name}
            </div>
          </div>
        </div>

        <ChevronRightIcon
          className="h-5 w-5 justify-self-center place-self-center cursor-pointer ease-in-out transform  hover:scale-150 
      transition duration-75 transform active:translate-x-2
      "
          onClick={() => changeTemplate(1)}
        />
      </div>
      <div
        className="text-xs text-font-2 hover:text-font-1 justify-center mt-2 cursor-pointer select-none flex items-center mb-5"
        onClick={() => setMenuOpen(prev => !prev)}
      >
        <div>Advanced Settings</div>
        {menuOpen ? (
          <ChevronDownIcon className="inline h-4 w-4" />
        ) : (
          <ChevronRightIcon className="inline h-4 w-4" />
        )}
      </div>
    </div>
  );
};

export default Template;
