import React, { FC, useEffect, useState, useCallback } from 'react';
import { IEditorTextStyles } from '~/redux/editor/types';
import getCssPropertiesFromStyleConfiguration from '~/components/Editor/helper/getCssPropertiesFromStyleConfiguration';
import subtitle_styles from '~/constants/styles/subtitles';
import AdvancedSettings from './AdvancedSettings';
import Headline from './Headline';
import TemplateGallery from './TemplateGallery';

interface ISelectAdvancedSettingsProps {
  configuration: IEditorTextStyles;
  onChange: (fieldName: string, value: string | number | boolean) => void;
  headline: string;
  previewText: string;
  contentEditable: boolean;
  onContentChange?: (value: string) => void;
}

const SelectAdvancedSettings: FC<ISelectAdvancedSettingsProps> = ({
  configuration,
  headline,
  onChange: handleChange,
  contentEditable,
  onContentChange,
}) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [borderRadiusOpacity, setBorderRadiusOpacity] = useState(1);
  const [fontTypeActive, setFontTypeActive] = useState('');

  const handleInput = (event: any) => {
    let { name, value } = event.target;

    //if the value is "splitBox" rounding option will be grayed out
    if ((value == 'splitBox' || !value) && fontTypeActive != 'splitBox') {
      setBorderRadiusOpacity(0.2);
      setFontTypeActive('splitBox');
    } else if (value != fontTypeActive) {
      switch (value) {
        case 'outline':
          setBorderRadiusOpacity(1);
          break;

        case 'shadow':
          setBorderRadiusOpacity(1);
          break;

        case 'shadow':
          break;

        default:
          value = '';
          break;
      }
    }

    setFontTypeActive(value);
    handleChange(name, value);
  };

  useEffect(() => {
    if (!configuration?.active) {
      setMenuOpen(false);
    }
  }, [configuration?.active]);

  const changeTemplate = (dval) => {
    let val = configuration.template + dval;
    if (val < 0) {
      val = subtitle_styles.length + val; //if -1 then it's going to go to the last one
    } else if (val > subtitle_styles.length - 1) {
      val = 0;
    }

    for (const property in subtitle_styles[val]) {
      handleChange(property, subtitle_styles[val][property]);
    }

    handleChange('template', val);
  };

  const styles = getCssPropertiesFromStyleConfiguration(configuration);
  styles.boxShadow = 'none'; //removes huge difference between outline and splitbox
  styles.lineHeight = 'normal'; //removes huge difference between outline and splitbox

  return (
    <div className="mt-5">
      <Headline
        configuration={configuration}
        handleChange={handleChange}
        headline={headline}
      />
      <div className="pt-5">
        <TemplateGallery
          changeTemplate={changeTemplate}
          configuration={configuration}
          contentEditable={contentEditable}
          menuOpen={menuOpen}
          onContentChange={onContentChange}
          setMenuOpen={setMenuOpen}
          styles={styles}
        />

        {menuOpen && (
          <AdvancedSettings
            configuration={configuration}
            borderRadiusOpacity={borderRadiusOpacity}
            handleInput={handleInput}
            handleChange={handleChange}
          />
        )}
      </div>
    </div>
  );
};

export default SelectAdvancedSettings;
