import React, { CSSProperties, FC, useEffect, useState } from 'react';
import { TwitterPicker, AlphaPicker } from 'react-color';
import { Switch } from '@headlessui/react';
import {
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/solid';
import { IEditorKaraokeStyles, IEditorTextStyles } from '~/redux/editor/types';
import getCssPropertiesFromStyleConfiguration from '~/components/Editor/helper/getCssPropertiesFromStyleConfiguration';
import EditorSwitch from '~/components/ui/Form/EditorSwitch';
import { colorToString, stringToColor } from '~/utils/colorToString';

interface IKaraokeSettingsProps {
  karaokeConfiguration: IEditorKaraokeStyles;
  subtitleConfiguration: IEditorTextStyles;
  onChange: Function;
}

const KaraokeSettings: FC<IKaraokeSettingsProps> = ({
  karaokeConfiguration,
  subtitleConfiguration,
  onChange: handleChange,
}) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [enabled, setEnabled] = useState(false);

  const handleColorChange = (color, name) => {
    handleChange(name, colorToString(color));
  };

  const handleColorAlphaChange = (color, name) => {
    handleChange(name, colorToString(color));
  };

  const baseStyles = getCssPropertiesFromStyleConfiguration(
    subtitleConfiguration,
  );
  const karaokeStyles: CSSProperties = {
    color: karaokeConfiguration.fontColor,
  };

  useEffect(() => {
    if (!karaokeConfiguration?.active) {
      setMenuOpen(false);
    }
  }, [karaokeConfiguration?.active]);

  return (
    <div className="mt-5 sm:w-[440px] mx-auto">
      <div className="text-center grid grid-cols-12">
        <div className="col-span-2 justify-self-end">
          <EditorSwitch
            isActive={karaokeConfiguration?.active}
            onChange={() =>
              handleChange('active', !karaokeConfiguration?.active)
            }
            name="active"
          />
        </div>
        <div className="text-font-2 font-extrabold text-base select-none flex-1 col-span-8">
          Karaoke
        </div>
        <div className="col-span-2" />
      </div>
      <div className="pt-5">
        <div className="grid grid-cols-12">
          <ChevronLeftIcon
            className="h-5 w-5 justify-self-center place-self-center cursor-pointer ease-in-out transform  hover:scale-150 
						transition duration-75 transform active:-translate-x-2"
          />

          <div
            style={{
              background:
                'repeating-linear-gradient(-45deg, rgba(0, 0, 0, 0.005), rgba(0, 0, 0, 0.005) 10px, rgba(255, 255, 255) 10px, rgba(255, 255, 255) 55px)',
            }}
            className="col-span-10 w-full bg-white rounded-xl mx-auto px-8 py-4 shadow-editor truncate flex justify-center"
          >
            <span
              style={baseStyles}
              className="bg-gray-700 text-white text-center block"
            >
              <span style={karaokeStyles}>This</span> is Karaoke
            </span>
          </div>

          <ChevronRightIcon
            className="h-5 w-5 justify-self-center place-self-center cursor-pointer ease-in-out transform  hover:scale-150 
						transition duration-75 transform active:-translate-x-2"
          />
        </div>
        <div
          className="text-xs text-font-2 hover:text-font-1 justify-center mt-2 cursor-pointer select-none flex items-center mb-5"
          onClick={() => setMenuOpen((prev) => !prev)}
        >
          <div>Advanced Settings</div>
          {menuOpen ? (
            <ChevronDownIcon className="inline h-4 w-4" />
          ) : (
            <ChevronRightIcon className="inline h-4 w-4" />
          )}
        </div>

        {menuOpen && (
          <>
            <div className="justify-center mt-2 select-none flex flex-col items-center mb-5 space-y-4">
              <TwitterPicker
                onChange={(color) => handleColorChange(color, 'fontColor')}
                color={karaokeConfiguration?.fontColor || '#ffffff'}
              ></TwitterPicker>
              <AlphaPicker
                onChange={(color) => handleColorAlphaChange(color, 'fontColor')}
                color={
                  stringToColor(karaokeConfiguration?.fontColor) || '#ffffff'
                }
              ></AlphaPicker>
            </div>

            <div className="mt-10 flex-gap 20 justify-center max-w-lg center mx-auto flex space-x-2 px-1 h-8">
              <Switch
                checked={enabled}
                onChange={(e) => {
                  setEnabled;
                  handleChange('effectType', !karaokeConfiguration.effectType);
                }}
                name="effectType"
                className={`${
                  karaokeConfiguration.effectType
                    ? 'bg-gray-200'
                    : 'bg-gray-200'
                } relative inline-flex items-center rounded-full m-0 w-44 py-1 transition-colors focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500`}
              >
                <span
                  className={`${
                    karaokeConfiguration.effectType
                      ? 'translate-x-[74px]'
                      : 'translate-x-1'
                  } py-2 text-base w-24 transform bg-white rounded-full transition-transform`}
                >
                  {karaokeConfiguration.effectType ? 'One Word' : 'All Words'}
                </span>
              </Switch>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default KaraokeSettings;
