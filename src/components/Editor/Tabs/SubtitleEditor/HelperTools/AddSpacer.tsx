import React, { FC, useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FaPlus } from 'react-icons/fa';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
} from '~/redux/editor/types';
import { RootState } from '~/redux/rootStore';
import { setSubtitles } from '~/redux/editor/editor.actions';
import { useSubtitleEditor } from '../useSubtitleEditor';

interface IAddSpacerProps {}

const AddSpacer: React.FC<IAddSpacerProps> = () => {
  const dispatch = useDispatch();

  const subtitles = useSelector(
    ({ editor }: RootState) => editor?.subtitles || [],
  );
  const { selectedWord, setSelectedWord } = useSubtitleEditor();

  const addDivider = useCallback(() => {
    const getIndex = (): number => {
      const index = subtitles.findIndex(({ id }) => id === selectedWord?.id);

      if (index === 0) {
        return 1;
      }

      return index;
    };

    const index = getIndex();

    if (!index) {
      console.warn('Could not find selected word!', {
        index,
        subtitles,
        selectedWord,
      });

      return;
    }

    subtitles.splice(index, 0, { type: TranslationWordTypeEnum.DIVIDER });
    dispatch(setSubtitles(subtitles));

    setSelectedWord(null);
  }, [subtitles, setSubtitles, selectedWord, setSelectedWord]);

  const disabled = useMemo(
    () => !selectedWord && subtitles.length > 0,
    [selectedWord, subtitles],
  );

  return (
    <div className="grid grid-cols-8 items-center">
      <label className="col-span-3 text-font-2 font-extrabold block col">
        Add a new divider
      </label>
      <div className="flex justify-self-start col-span-2 items-center space-x-2">
        <div className="text-font-1 self-start h-10 w-[4px] bg-font-1 rounded-full" />
      </div>
      <div className="justify-self-end col-span-2">
        <button
          className={`block self-end p-2 text-font-icon bg-white rounded shadow-editor ${
            disabled
              ? `disabled:bg-gray-100 disabled:text-gray-400`
              : 'hover:bg-gray-100 hover:text-gray-400 focus:outline-none'
          }`}
          onClick={addDivider}
          disabled={disabled}
        >
          <FaPlus />
        </button>
      </div>
    </div>
  );
};

export default AddSpacer;
