import React, { useEffect, useMemo, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '~/redux/rootStore';
import { setSubtitles } from '~/redux/editor/editor.actions';
import { FaAngleLeft, FaAngleRight, FaCheck } from 'react-icons/fa';
import { useDebouncedCallback } from 'use-debounce';
import collect from 'collect.js';
import { ITranslationWord } from '~/redux/editor/types';
import { DirectionEnum } from '../HtmlEditor/types.d';
import { useSubtitleEditor } from '../useSubtitleEditor';

interface IReplaceToolsProps {}

const ReplaceTools: React.FC<IReplaceToolsProps> = () => {
  const dispatch = useDispatch();

  const { replaceWord: replaceWordContext } = useSubtitleEditor();

  const subtitles = useSelector(
    ({ editor }: RootState) => editor?.subtitles || [],
  );

  const [replaceWord, setReplaceWord] = useState<string>('');
  const [countOfWords, setCountOfWords] = useState<number>(0);
  const [replace, setReplace] = useState<string>('');

  const hasMoreThanOneMatchedWord = useMemo(
    () => countOfWords > 1,
    [countOfWords],
  );
  const canReplaceWord = useMemo(
    () => countOfWords > 0 && !!replace && !!replaceWord,
    [countOfWords, replace, replaceWord],
  );

  const updateReplaceWord = useDebouncedCallback(
    (replaceWord, countOfWords) => {
      if (replaceWordContext.word !== replaceWord) {
        replaceWordContext.setWord(replaceWord);
      }

      if (replaceWordContext.count !== countOfWords) {
        replaceWordContext.setCount(countOfWords);
      }

      if (countOfWords > 0 && replaceWordContext.focusedWordIndex !== 0) {
        replaceWordContext.setFocusedWordIndex(0);
      }
    },
    500,
    { trailing: true },
  );

  const regex = useCallback(
    (value: string) => new RegExp(`(\\b${value}\\b)`, 'giu'),
    [],
  );

  useEffect(() => {
    if (replaceWord !== replaceWordContext.word) {
      setReplaceWord(replaceWordContext.word);
    }
  }, [replaceWord, replaceWordContext.word, setReplaceWord]);

  useEffect(() => {
    updateReplaceWord(replaceWord, countOfWords);
  }, [replaceWord, countOfWords]);

  const handleTypedWord = useCallback(
    (text) => {
      setReplaceWord(text);

      if (text.length === 0) {
        setCountOfWords(0);

        return;
      }

      const { length: numberOfMatches } = subtitles.filter(
        ({ Word: word }) => !!word && regex(text).test(word),
      );

      setCountOfWords(numberOfMatches);
    },
    [regex, subtitles, setReplaceWord, setCountOfWords],
  );

  useEffect(() => {
    handleTypedWord(replaceWord);
  }, [handleTypedWord, replaceWord]);

  const focusOnNextWord = useCallback(
    (direction: DirectionEnum) => {
      const getDelta = (): number => {
        switch (direction) {
          case DirectionEnum.BEFORE:
            return -1;

          case DirectionEnum.AFTER:
            return 1;

          case DirectionEnum.NONE:
          default:
            return 0;
        }
      };

      const delta = getDelta();

      replaceWordContext.setFocusedWordIndex((focusedWordIndex) => {
        const number = (focusedWordIndex ?? 0) + delta;

        return ((number % countOfWords) + countOfWords) % countOfWords;
      });
    },
    [replaceWordContext.setFocusedWordIndex, countOfWords],
  );

  const replaceAll = useCallback(
    (event: any) => {
      event.preventDefault();

      if (replace === '') {
        return;
      }

      const replacedWordList = subtitles.map((subtitle) => ({
        ...subtitle,
        Word: (subtitle.Word ?? '')
          .toString()
          .replace(regex(replaceWord), replace),
      }));

      setReplaceWord('');
      setReplace('');
      // setCountOfWords(0);

      dispatch(setSubtitles(replacedWordList));
    },
    [
      regex,
      replace,
      subtitles,
      setReplaceWord,
      setReplace,
      setCountOfWords,
      setSubtitles,
      dispatch,
    ],
  );

  const replaceFocusedWord = useCallback(() => {
    if (replace == '') {
      return;
    }

    const focusedWord = collect(subtitles)
      .filter(({ Word: word }) => regex(replaceWord).test(word ?? ''))
      .skip(replaceWordContext.focusedWordIndex)
      .first();

    dispatch(
      setSubtitles(
        subtitles.map((subtitle) => {
          if (subtitle.id === focusedWord.id) {
            return { ...subtitle, Word: replace };
          }

          return subtitle;
        }),
      ),
    );

    if ((replaceWordContext.focusedWordIndex ?? 0) > 0) {
      focusOnNextWord(DirectionEnum.BEFORE);
    }
  }, [
    subtitles,
    regex,
    replace,
    replaceWord,
    replaceWordContext.focusedWordIndex,
    focusOnNextWord,
    dispatch,
    setSubtitles,
  ]);

  return (
    <div className="grid grid-cols-4 items-center">
      <div className="col-span-1">
        <label
          htmlFor="replace-this-word"
          className="text-font-2 mb-4 font-extrabold block"
        >
          Replace word:
        </label>
        <label htmlFor="with-word" className="text-font-2 font-extrabold block">
          with word:
        </label>
      </div>
      <div className="col-span-2 space-y-2 px-4">
        <input
          placeholder="Example"
          id="replace-this-word"
          className="px-1 text-center py-1 w-full bg-white rounded-full shadow-editor focus:outline-none border border-gray-100"
          onChange={({ target }) => handleTypedWord(target.value)}
          value={replaceWord}
        />
        <input
          placeholder="Example"
          id="with-word"
          className="px-1 py-1 text-center w-full bg-white rounded-full shadow-editor focus:outline-none border border-gray-100"
          onChange={(event: any) => setReplace(event?.target.value)}
          value={replace}
        />
      </div>
      <div className="col-span-1 justify-self-center">
        <div className="flex">
          <button
            className="block self-end p-2 text-font-icon bg-transparent rounded hover:bg-gray-100 hover:text-gray-400 focus:outline-none"
            onClick={() => focusOnNextWord(DirectionEnum.BEFORE)}
            hidden={!hasMoreThanOneMatchedWord}
          >
            <FaAngleLeft size={16} />
          </button>
          <button
            className="ml-0.5 mr-0.5 block self-end p-2 text-font-icon bg-white rounded hover:bg-gray-100 hover:text-gray-400 shadow-editor focus:outline-none"
            onClick={() => replaceFocusedWord()}
            hidden={!canReplaceWord}
          >
            <FaCheck size={16} />
          </button>
          <button
            className="block self-end p-2 text-font-icon bg-transparent rounded hover:bg-gray-100 hover:text-gray-400 focus:outline-none"
            onClick={() => focusOnNextWord(DirectionEnum.AFTER)}
            hidden={!hasMoreThanOneMatchedWord}
          >
            <FaAngleRight size={16} />
          </button>
        </div>
      </div>
      <div className="col-span-2"></div>
      <div className="flex items-center justify-end mt-2 col-span-2 text-right">
        <button
          className="ml-0.5 mr-0.5 block self-end p-2 text-font-icon bg-white rounded hover:bg-gray-100 hover:text-gray-400 shadow-editor focus:outline-none"
          onClick={replaceAll}
          hidden={!canReplaceWord}
        >
          Replace All {countOfWords}
        </button>
      </div>
    </div>
  );
};

export default ReplaceTools;
