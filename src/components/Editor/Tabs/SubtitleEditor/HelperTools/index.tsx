import React from 'react';
import { ITranslationWord } from '~/redux/editor/types';
import AddSpacer from './AddSpacer';
import ReplaceTools from './ReplaceTools';

interface IHelperToolsProps {}

const HelperTools: React.FC<IHelperToolsProps> = () => {
  return (
    <div className="space-y-6 mt-6">
      <ReplaceTools />
      <AddSpacer />
    </div>
  );
};

export default HelperTools;
