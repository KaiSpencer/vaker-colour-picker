import React, { FC, useMemo } from 'react';
import { ISentence } from './types.d';
import { useVideoEditor } from '~/hooks/useVideoEditor';

interface Props {
  sentence: ISentence;
  className?: string;
  hidden?: boolean;
}

const ActiveHighlightedSentence: FC<Props> = ({
  sentence,
  children,
  ...props
}) => {
  const { playedSeconds } = useVideoEditor();

  const playedNanoSeconds = useMemo(
    () => playedSeconds * 1000 * 1000 * 10,
    [playedSeconds],
  );

  const { offset = 0, duration = 0 } = sentence;

  const sentenceIsCurrentSubtitle = useMemo((): boolean => {
    return playedNanoSeconds > offset && playedNanoSeconds < offset + duration;
  }, [offset, duration, playedNanoSeconds]);

  return (
    <span
      {...props}
      className={`${props.className || ''} ${
        sentenceIsCurrentSubtitle ? 'bg-blue-100' : ''
      }`}
    >
      {children && typeof children === 'function'
        ? (children(sentenceIsCurrentSubtitle) as (
            sentenceIsCurrentSubtitle: boolean,
          ) => React.ReactNode)
        : children}
    </span>
  );
};

export default ActiveHighlightedSentence;
