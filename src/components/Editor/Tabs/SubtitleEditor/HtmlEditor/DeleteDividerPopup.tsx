import React, { FC } from 'react';
import Popup from 'reactjs-popup';
import { XCircleIcon } from '@heroicons/react/solid';
import { Button } from '~/components/ui/Button';
import 'reactjs-popup/dist/index.css';

interface Props {
  dividerId: string | number | null;
  onConfirm: (dividerId: string | number) => void;
  onClose: () => void;
}

const DeleteDividerPopup: FC<Props> = ({ dividerId, onConfirm, onClose }) => {
  return (
    <Popup open={!!dividerId} modal closeOnEscape onClose={() => onClose()}>
      {(close) => (
        <div className="p-4">
          <button
            className="absolute top-0 right-0 -mt-4 -mr-4"
            onClick={close}
          >
            <XCircleIcon className="h-8 w-8 inline bg-white rounded-full" />
          </button>
          <div className="text-center text-2xl pb-4">
            Are you sure you want to remove this divider?
          </div>
          <div className="flex w-2/4 mx-auto">
            <Button
              variant="primary"
              className="rounded-r-none"
              onClick={close}
            >
              No
            </Button>
            <Button
              variant="danger"
              className="rounded-l-none"
              onClick={(...args) => {
                dividerId && onConfirm(dividerId);

                close(...args);
              }}
            >
              Yes, remove the divider!
            </Button>
          </div>
        </div>
      )}
    </Popup>
  );
};

export default DeleteDividerPopup;
