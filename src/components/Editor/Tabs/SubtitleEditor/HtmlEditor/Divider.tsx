import React, { FC, useCallback } from 'react';
import Tooltip, { tooltipParentClass } from '~/components/ui/Tooltip';
import { useSubtitleEditor } from '../useSubtitleEditor';

interface Props {
  className?: string;
  onMouseDown?: () => void | React.MouseEvent;
  onMouseUp?: () => void | React.MouseEvent;
  style?: any;
}

const Divider: FC<Props> = ({ ...props }) => {
  const { shownDividerPopup } = useSubtitleEditor();

  return (
    <div className={`inline ${shownDividerPopup ? '' : tooltipParentClass}`}>
      {!shownDividerPopup && (
        <Tooltip className="mt-8 -ml-20">Drag & drop the divider!</Tooltip>
      )}
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 6.8 31.3"
        height="1.5rem"
        {...props}
        className={`${props.className || ''} inline mx-1 cursor-pointer`}
      >
        <path d="M2.2 3v26.2c0 .7.5 1.2 1.2 1.2s1.2-.5 1.2-1.2V3c0-.7-.5-1.2-1.2-1.2S2.2 2.4 2.2 3z" />
      </svg>
    </div>
  );
};

export default Divider;
