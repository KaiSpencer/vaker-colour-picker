import React, { FC, useCallback, useRef } from 'react';
import { useDrag, useDrop, DropTargetMonitor } from 'react-dnd';
import { XYCoord } from 'dnd-core';
import { TranslationWordTypeEnum } from '~/redux/editor/types';
import { DirectionEnum } from './types.d';

interface Props {
  className?: string;
  id: string | number;
  index: number;
  type: TranslationWordTypeEnum;
  onHover: (
    dragId: string | number,
    hoverId: string | number,
    direction: DirectionEnum,
  ) => void;
  onDragEnd: () => void;
  onDrop: (
    dragId: string | number,
    hoverId: string | number,
    direction: DirectionEnum,
  ) => void;
}

interface IDragItem {
  index: number;
  id: string;
  type: string;
}

const Draggable: FC<Props> = ({
  children,
  id,
  index,
  type,
  onHover,
  onDragEnd,
  onDrop,
  ...props
}) => {
  const ref = useRef<HTMLDivElement>(null);

  const getDirection = useCallback(
    (monitor: DropTargetMonitor): DirectionEnum => {
      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      if (!hoverBoundingRect) {
        console.warn(
          'Could not compute direction! No boundingClientRect found.',
        );

        return DirectionEnum.NONE;
      }

      // Get horizontal middle
      const hoverMiddleX =
        (hoverBoundingRect.right - hoverBoundingRect.left) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset() as XYCoord;

      // Get pixels to the left
      const hoverClientX = clientOffset.x - hoverBoundingRect.left;

      if (hoverClientX < hoverMiddleX) {
        return DirectionEnum.BEFORE;
      }

      return DirectionEnum.AFTER;
    },
    [],
  );

  const [{ handlerId }, drop] = useDrop({
    accept: TranslationWordTypeEnum.DIVIDER,
    collect: (monitor: DropTargetMonitor) => ({
      handlerId: monitor.getHandlerId(),
    }),
    hover: (item: IDragItem, monitor: DropTargetMonitor) => {
      const { id: dragId } = item;
      const hoverId = id;

      onHover(
        dragId,
        hoverId,
        dragId === hoverId ? DirectionEnum.NONE : getDirection(monitor),
      );
    },
    drop: (item: IDragItem, monitor: DropTargetMonitor) => {
      const { id: dragId } = item;
      const hoverId = id;

      onDrop(
        dragId,
        hoverId,
        dragId === hoverId ? DirectionEnum.NONE : getDirection(monitor),
      );
    },
  });

  const [{ isDragging, fill }, drag, dragPreview] = useDrag({
    canDrag: () => type === TranslationWordTypeEnum.DIVIDER,
    collect: (monitor: any) => {
      const isDragging = monitor.isDragging();

      return {
        isDragging,
        fill: isDragging ? 'red' : 'blue',
      };
    },
    end: onDragEnd,
    item: () => ({ id, index }),
    type,
  });

  drag(drop(ref));

  return (
    <div
      {...props}
      ref={isDragging ? dragPreview : ref}
      style={{ fill }}
      data-handler-id={handlerId}
      className={`${props.className || ''} inline`}
    >
      {children}
    </div>
  );
};

export default Draggable;
