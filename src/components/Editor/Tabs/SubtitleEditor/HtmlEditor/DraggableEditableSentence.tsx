import React, { FC, useMemo } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { TranslationWordTypeEnum } from '~/redux/editor/types';
import { ISentence, DirectionEnum } from './types.d';
import collect from 'collect.js';
import ActiveHighlightedSentence from './ActiveHighlightedSentence';
import EditableSentence from './EditableSentence';
import Draggable from './Draggable';
import Divider from './Divider';

interface Props {
  index: number;
  dropCursorId: string | number | null;
  sentence: ISentence;
  setSentence: (sentence: ISentence) => void;
  removeDivider: (sentence: ISentence, direction: DirectionEnum) => void;
  numberOfSentences: number;
  isDragging: boolean;
  onHover: (
    dragId: string | number,
    hoverId: string | number,
    direction: DirectionEnum,
  ) => void;
  onDragStart: () => void;
  onDragEnd: () => void;
  onDrop: (
    dragId: string | number,
    hoverId: string | number,
    direction: DirectionEnum,
  ) => void;
}

const DraggableEditableSentence: FC<Props> = ({
  index,
  dropCursorId,
  sentence,
  setSentence,
  removeDivider,
  numberOfSentences,
  isDragging,
  onHover,
  onDragStart,
  onDragEnd,
  onDrop,
}) => {
  const draggableProps = {
    index,
    onHover,
    onDragEnd,
    onDrop,
  };

  const Cursor = ({ id }) =>
    id === dropCursorId ? (
      <span>
        <Divider
          style={{
            position: 'absolute',
            marginLeft: '-0.25rem',
            fill: 'black',
          }}
        />
      </span>
    ) : null;

  const padding = useMemo(
    () => (sentence.originalStartId === 0 ? 'pr-1' : 'px-1'),
    [sentence.originalStartId],
  );

  if (!!sentence.divider) {
    const { id } = sentence.divider;

    return (
      <Draggable
        {...draggableProps}
        key={`draggable-divider-${sentence.divider.id}`}
        id={id || -1}
        type={sentence.type}
      >
        <Divider onMouseDown={onDragStart} onMouseUp={onDragEnd} />
      </Draggable>
    );
  }

  return (
    <ActiveHighlightedSentence sentence={sentence}>
      {!isDragging && (
        <EditableSentence
          key={`editable_sentence-${index}-${numberOfSentences}`}
          sentence={sentence}
          setSentence={setSentence}
          removeDivider={removeDivider}
        />
      )}
      <span className={`${padding}`} hidden={!isDragging}>
        {sentence.words &&
          sentence.words.map(({ id, type, Word: text }, index) => {
            const leftBuffer = index === 0 ? '' : ' ';
            const rightBuffer =
              index === (sentence.words?.length || 0) - 1 ? '' : ' ';

            return (
              <Draggable
                {...draggableProps}
                key={`draggable-word-${id}`}
                id={id || -1}
                type={type || TranslationWordTypeEnum.WORD}
              >
                <Cursor id={id} />
                <span>
                  {leftBuffer}
                  {text}
                  {rightBuffer}
                </span>
              </Draggable>
            );
          })}
      </span>
    </ActiveHighlightedSentence>
  );
};

export default DraggableEditableSentence;
