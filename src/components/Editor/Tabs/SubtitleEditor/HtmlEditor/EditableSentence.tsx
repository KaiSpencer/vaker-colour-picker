import React, {
  FC,
  useMemo,
  useEffect,
  useState,
  useRef,
  useCallback,
} from 'react';
import { useSelector } from 'react-redux';
import { useDebouncedCallback } from 'use-debounce';
import reactStringReplace from 'react-string-replace';
import collect from 'collect.js';
import { DirectionEnum, ISentence } from './types.d';
import { ITranslationWord } from '~/redux/editor/types';
import { RootState } from '~/redux/rootStore';
import { getSelectedWordFromSentence } from './utils/getSelectedWord';
import { useSubtitleEditor } from '../useSubtitleEditor';

interface Props {
  sentence: ISentence;
  setSentence: (sentence: ISentence) => void;
  removeDivider: (sentence: ISentence, direction: DirectionEnum) => void;
}

const EditableSentence: FC<Props> = ({
  sentence,
  setSentence,
  removeDivider,
}) => {
  const editTextRef = useRef<HTMLParagraphElement>(null);

  const [text, setText] = useState<string>(sentence.text || '');
  const [previousText, setPreviousText] = useState<string>(text);

  const subtitles = useSelector(
    ({ editor }: RootState) => editor?.subtitles || [],
  );

  const { setSelectedWord, replaceWord: replaceWordContext } =
    useSubtitleEditor();
  const {
    word: replaceWord,
    count: replaceWordCount,
    focusedWordIndex: replaceWordFocusedWordIndex,
  } = replaceWordContext;

  const updateSentence = useCallback(
    useDebouncedCallback(setSentence, 1000, { trailing: true }),
    [setSentence],
  );

  useEffect(() => {
    if (editTextRef.current !== document.activeElement) {
      setText(sentence.text || '');
    }
  }, [
    sentence.text,
    editTextRef.current,
    typeof document !== 'undefined' && document.activeElement,
  ]);

  const onKeyUp = useCallback(
    ({ keyCode, target }) => {
      const currentInnerText = editTextRef.current?.innerText.trim();

      if (!!currentInnerText && currentInnerText !== previousText) {
        updateSentence({ ...sentence, text: currentInnerText });
        setPreviousText(currentInnerText);

        return;
      }

      switch (keyCode) {
        // keyCode: backspace
        case 8:
          removeDivider(sentence, DirectionEnum.BEFORE);
          break;

        // keyCode: delete
        case 46:
          removeDivider(sentence, DirectionEnum.AFTER);
          break;

        default:
          break;
      }
    },
    [
      sentence,
      editTextRef.current?.innerText,
      updateSentence,
      previousText,
      setPreviousText,
      removeDivider,
    ],
  );

  const onMouseUp = useCallback(
    ({ target }) => {
      const wordAtLocation = getSelectedWordFromSentence(target, sentence);

      setSelectedWord(wordAtLocation);
    },
    [sentence, getSelectedWordFromSentence, setSelectedWord],
  );

  const padding = useMemo(
    () => (sentence.originalStartId === 0 ? 'pr-1' : 'px-1'),
    [sentence.originalStartId],
  );

  const regex = useMemo(
    () => new RegExp(`(\\b${replaceWord}\\b)`, 'giu'),
    [replaceWord],
  );

  const startingCountForFocusedWords = useMemo(
    () =>
      collect(subtitles)
        .takeUntil(({ id }) => id === sentence.originalStartId)
        .filter(({ Word: word }) => (word ?? '').match(regex))
        .count(),
    [regex, subtitles, sentence.originalStartId],
  );

  const stylisedText = useMemo(() => {
    const isFocusedWord = (index) =>
      index + startingCountForFocusedWords === replaceWordFocusedWordIndex;

    let counter = 0;

    return reactStringReplace(text, regex, (match, index) => {
      const className = isFocusedWord(counter) ? 'bg-blue-200' : 'bg-gray-300';

      counter = counter + 1;

      return (
        <span key={index} className={className}>
          {match}
        </span>
      );
    });
  }, [text, regex, startingCountForFocusedWords, replaceWordFocusedWordIndex]);

  return (
    <p
      id={`editable-sentence-${sentence.originalStartId}-${sentence.originalEndId}`}
      className={`focus:outline-none inline ${padding}`}
      contentEditable
      onKeyUp={onKeyUp}
      onMouseUp={onMouseUp}
      ref={editTextRef}
      suppressContentEditableWarning
      style={{
        border: 'none',
        background: 'transparent',
        borderBottom: '1px solid #fff',
        outline: 'none',
      }}
    >
      {stylisedText}
    </p>
  );
};

export default EditableSentence;
