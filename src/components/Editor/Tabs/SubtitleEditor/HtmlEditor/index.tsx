import React, { FC, useState, useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useDebouncedCallback } from 'use-debounce';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import collect from 'collect.js';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
} from '~/redux/editor/types';
import useHasFocus from '~/hooks/useHasFocus';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { useSubtitleEditor } from '../useSubtitleEditor';
import { ISentence, DirectionEnum } from './types.d';
import { RootState } from '~/redux/rootStore';
import { setSubtitles } from '~/redux/editor/editor.actions';
import DraggableEditableSentence from './DraggableEditableSentence';
import convertSubtitleWordsToSentences from './utils/convertSubtitleWordsToSentences';
import convertSubtitleSentenceToWords from './utils/convertSubtitleSentenceToWords';
import getSelectionCharacterOffsetWithinElement from './utils/getSelectionCharacterOffsetWithinElement';
import { getSelectedWordFromSentences } from './utils/getSelectedWord';
import DeleteDividerPopup from './DeleteDividerPopup';
import Draggable from './Draggable';

interface Props {}

const HtmlEditor: FC<Props> = () => {
  const dispatch = useDispatch();

  const [isDragging, setIsDragging] = useState<boolean>(false);
  const [deleteDividerId, setDeleteDividerId] = useState<
    string | number | null
  >(null);
  const [dropCursorId, setDropCursorId] = useState<string | number | null>(
    null,
  );

  const subtitles = useSelector(
    ({ editor }: RootState) => editor?.subtitles || [],
  );

  const { selectedWord, setSelectedWord, setShownDividerPopup, replaceWord } =
    useSubtitleEditor();
  const { setPlayedSeconds, setPlaying } = useVideoEditor();

  const sentences = useMemo<ISentence[]>(
    () => convertSubtitleWordsToSentences(subtitles),
    [subtitles],
  );

  const setSentence = useCallback(
    (sentence): void => {
      dispatch(
        setSubtitles(convertSubtitleSentenceToWords(subtitles, sentence)),
      );
    },
    [subtitles, setSubtitles, convertSubtitleSentenceToWords, dispatch],
  );

  const getIdToInsertWordAt = useCallback(
    (
      dragId: string | number,
      hoverId: string | number,
      direction: DirectionEnum,
    ): string | number | null => {
      switch (direction) {
        case DirectionEnum.AFTER:
          return collect(subtitles)
            .skipUntil(({ id }) => id === hoverId)
            .get(1)?.id;

        case DirectionEnum.BEFORE:
          const previousWordId = collect(subtitles)
            .takeUntil(({ id }) => id === hoverId)
            .last()?.id;

          if (previousWordId === dragId) {
            return null;
          } else {
            return hoverId;
          }

        default:
        case DirectionEnum.NONE:
          return null;
      }
    },
    [subtitles],
  );

  const getWordAndNeighbours = useCallback(
    (wordId: string | number): ITranslationWord[] =>
      collect(subtitles)
        .slice(subtitles.findIndex(({ id }) => id === wordId) - 1, 3)
        .toArray(),
    [subtitles],
  );

  const moveDivider = useCallback(
    (
      dragId: string | number,
      hoverId: string | number,
      direction: DirectionEnum,
    ): void => {
      const insertWordAtId =
        dropCursorId || getIdToInsertWordAt(dragId, hoverId, direction);

      if (!insertWordAtId) {
        console.warn('Cannot moveDivider to location!', {
          dropCursorId,
          dragId,
          hoverId,
          direction,
        });

        return;
      }

      const dragDivider = subtitles.find(({ id }) => id === dragId);

      if (!dragDivider) {
        console.warn('Could not find drag word!', {
          dropCursorId,
          dragId,
          hoverId,
          direction,
        });

        return;
      }

      const [beforeWord, insertAtWord] = getWordAndNeighbours(insertWordAtId);
      if (
        beforeWord.type === TranslationWordTypeEnum.DIVIDER ||
        insertAtWord.type === TranslationWordTypeEnum.DIVIDER
      ) {
        return;
      }

      dispatch(
        setSubtitles(
          collect(subtitles)
            .where('id', '!==', dragId)
            .tap((collection) => {
              const insertWordAtIndex = (
                collection.toArray() as ITranslationWord[]
              ).findIndex(({ id }) => id === insertWordAtId);

              collection.splice(insertWordAtIndex, 0, [dragDivider]);
            })
            .pipe((collection) => {
              const dragDividerIndex = collection
                .toArray()
                .findIndex(({ id }) => id === dragDivider.id);

              const leftHandSide = collection
                .slice(0, dragDividerIndex)
                .reverse();
              const rightHandSide = collection.slice(dragDividerIndex + 1);

              // If we're at the beginning, or the end, of all Subtitles then do nothing
              if (leftHandSide.isEmpty() || rightHandSide.isEmpty()) {
                return collection;
              }

              // If the Offset's are different then do nothing
              const leftHandSideFirstOffset = leftHandSide.first().Offset;
              if (leftHandSideFirstOffset !== rightHandSide.first().Offset) {
                return collection;
              }

              // Find the most appropriate next offset
              const { Offset: leftHandSideNewOffset } = leftHandSide.firstWhere(
                'Offset',
                '!==',
                leftHandSideFirstOffset,
              ) || { Offset: 0 };

              // Return a collection with the left-handside subtitles' Offsets updated
              return collection.map((word, index) => {
                if (
                  index < dragDividerIndex &&
                  word.Type !== TranslationWordTypeEnum.DIVIDER &&
                  word.Offset === leftHandSide.first().Offset
                ) {
                  return { ...word, Offset: leftHandSideNewOffset };
                }

                return word;
              });
            })
            .toArray(),
        ),
      );
    },
    [subtitles, setSubtitles, dropCursorId, getIdToInsertWordAt, dispatch],
  );

  const showRemoveDividerPopup = useCallback(
    (
      { originalStartId, originalEndId }: ISentence,
      direction: DirectionEnum,
    ): void => {
      const wordId =
        direction === DirectionEnum.AFTER ? originalEndId : originalStartId;

      const reverse = direction === DirectionEnum.BEFORE;
      const divider = collect(reverse ? [...subtitles].reverse() : subtitles)
        .skipUntil((word) => word.id === wordId)
        .firstWhere('type', TranslationWordTypeEnum.DIVIDER);

      if (!divider) {
        console.warn('Could not find divider to remove!', {
          originalStartId,
          originalEndId,
          direction,
        });

        return;
      }

      setDeleteDividerId(divider.id);
    },
    [subtitles],
  );

  const removeDivider = useCallback(
    (dividerId: string | number): void => {
      const dividerIndex = subtitles.findIndex(({ id }) => id === dividerId);

      if (!dividerIndex) {
        console.warn('Could not find divider to remove!', { dividerId });

        return;
      }

      subtitles.splice(dividerIndex, 1);
      dispatch(setSubtitles(subtitles));
    },
    [subtitles, setSubtitles, dispatch],
  );

  const onHover = useCallback(
    (
      dragId: string | number,
      hoverId: string | number,
      direction: DirectionEnum,
    ) => {
      const insertWordAtId = getIdToInsertWordAt(dragId, hoverId, direction);
      if (!insertWordAtId) {
        setDropCursorId(null);

        return;
      }

      const [beforeWord, insertAtWord] = getWordAndNeighbours(insertWordAtId);
      if (
        beforeWord.type === TranslationWordTypeEnum.DIVIDER ||
        insertAtWord.type === TranslationWordTypeEnum.DIVIDER
      ) {
        setDropCursorId(null);

        return;
      }

      setDropCursorId(insertWordAtId);
    },
    [getIdToInsertWordAt, getIdToInsertWordAt],
  );
  const onDragStart = useCallback(() => {
    setIsDragging(true);

    setShownDividerPopup(true);
  }, [setIsDragging, setShownDividerPopup]);
  const onDragEnd = useCallback(() => {
    setIsDragging(false);

    setDropCursorId(null);
  }, []);

  const [hasFocusProps] = useHasFocus({
    onFocusGained: useCallback(
      (target: HTMLElement) => {
        const selectedWord = getSelectedWordFromSentences(target, sentences);

        if (!selectedWord?.Offset) {
          return;
        }

        setPlaying(false);

        setPlayedSeconds(
          parseFloat(selectedWord.Offset?.toString() ?? '0') / 10000000 - 1.5,
        );
      },
      [sentences, setPlaying, setPlayedSeconds],
    ),
    onFocusLost: useDebouncedCallback(
      useCallback(
        (target: HTMLElement) => setSelectedWord(null),
        [selectedWord, setSelectedWord],
      ),
      1000,
      {
        trailing: true,
      },
    ),
  });

  return (
    <span className="text-base leading-7" {...hasFocusProps}>
      <DeleteDividerPopup
        dividerId={deleteDividerId}
        onConfirm={(dividerId) => removeDivider(dividerId)}
        onClose={() => setDeleteDividerId(null)}
      />
      <DndProvider backend={HTML5Backend}>
        {sentences.map((sentence, index) => (
          <DraggableEditableSentence
            key={index}
            index={index}
            dropCursorId={dropCursorId}
            sentence={sentence}
            setSentence={setSentence}
            removeDivider={showRemoveDividerPopup}
            numberOfSentences={sentences.length}
            isDragging={isDragging}
            onHover={onHover}
            onDragStart={onDragStart}
            onDragEnd={onDragEnd}
            onDrop={moveDivider}
          />
        ))}
      </DndProvider>
    </span>
  );
};

export default HtmlEditor;
