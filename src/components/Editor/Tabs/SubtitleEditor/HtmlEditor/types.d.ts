import {
  ITranslationWord,
  TranslationWordTypeEnum,
} from '~/redux/editor/types';

export enum DirectionEnum {
  BEFORE,
  AFTER,
  NONE,
}

export interface ISentence {
  text?: string;
  originalStartId: string | number;
  originalEndId: string | number;
  words?: ITranslationWord[];
  divider?: ITranslationWord;
  type: TranslationWordTypeEnum;
  offset?: number;
  duration?: number;
}
