import collect from 'collect.js';
import { ISentence } from '../types.d';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
} from '~/redux/editor/types';

const convertSubtitleSentenceToWords = (
  words: ITranslationWord[],
  { text, originalStartId, originalEndId }: ISentence,
): ITranslationWord[] => {
  {
    if (!text || originalStartId === null || originalEndId === null) {
      console.warn('Cannot update sentence, missing parameters!', {
        text,
        originalStartId,
        originalEndId,
      });

      return words;
    }

    const startIndex = words.findIndex(({ id }) => id === originalStartId);
    const endIndex = words.findIndex(({ id }) => id === originalEndId);

    const subsectionOfWords = words.slice(startIndex, endIndex + 1);
    const subsectionOfWordsQueue: Array<ITranslationWord | null> = [
      ...subsectionOfWords,
    ];

    const newWordsAsStrings = text.split(' ');

    words.splice(
      startIndex,
      endIndex - startIndex + 1,
      ...newWordsAsStrings.map((word) => {
        // Attempt to find the previous word's Offset & Duration
        const originalWordIndex = subsectionOfWordsQueue.findIndex(
          (obj) => obj?.Word?.toLowerCase() === word.toLowerCase(),
        );
        const originalWord =
          originalWordIndex > -1
            ? subsectionOfWordsQueue[originalWordIndex]
            : null;

        // Remove the original word from the queue,
        // -> we can only use that word's Offset & Duration once`
        if (!!originalWord) {
          subsectionOfWordsQueue[originalWordIndex] = null;
        }

        return {
          ...originalWord,
          Word: word,
          type: TranslationWordTypeEnum.WORD,
        };
      }),
    );

    const firstWord = subsectionOfWords[0];
    const lastWord = subsectionOfWords[subsectionOfWords.length - 1];

    // Ensure the "first word" respects the Offset/Duration at the start of the sentence,
    // -> if they don't already have their own Offset/Duration
    words[startIndex] = {
      Offset: firstWord.Offset,
      Duration: firstWord.Duration,
      ...words[startIndex],
    };

    // Ensure the "last word" respects the Offset/Duration at the end of the sentence,
    // -> if they don't already have their own Offset/Duration
    const endIndexAfterSplice = startIndex + newWordsAsStrings.length - 1;
    words[endIndexAfterSplice] = {
      Offset: lastWord.Offset,
      Duration: lastWord.Duration,
      ...words[endIndexAfterSplice],
    };

    // Compute the Offset for the remaining new words
    for (let i = startIndex; i < endIndexAfterSplice + 1; i++) {
      const nextWord = words[i + 1];
      const previousWord = words[i - 1];

      words[i] = { Duration: 0, ...words[i] };

      const word = words[i];

      if (!word.Offset) {
        const computeNewOffset = () => {
          if (!nextWord.Offset) {
            return previousWord.Offset;
          }

          return Math.round(
            (parseFloat(previousWord.Offset?.toString() ?? '0') +
              parseFloat(nextWord.Offset?.toString() ?? '0')) /
              2,
          );
        };

        words[i] = { ...word, Offset: computeNewOffset() };
      }
    }

    return words;
  }
};

export default convertSubtitleSentenceToWords;
