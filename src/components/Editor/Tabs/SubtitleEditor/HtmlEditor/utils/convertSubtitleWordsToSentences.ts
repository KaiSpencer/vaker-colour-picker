import collect from 'collect.js';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
} from '~/redux/editor/types';
import { ISentence } from '../types.d';

const convertSubtitleWordsToSentences = (
  words: ITranslationWord[],
): ISentence[] => {
  const collection = collect(words);
  const sentences: ISentence[] = [];

  while (collection.isNotEmpty()) {
    const sentence = collection.takeUntil(
      ({ type }) => type === TranslationWordTypeEnum.DIVIDER,
    );

    if (sentence.isNotEmpty()) {
      collection.splice(0, sentence.count());

      const minOffset = sentence.min('Offset');
      const lastWordInSentence = sentence.last();
      const duration =
        lastWordInSentence.Offset + lastWordInSentence.Duration - minOffset;

      sentences.push({
        text: sentence.pluck('Word').join(' '),
        originalStartId: sentence.first().id,
        originalEndId: lastWordInSentence.id,
        words: sentence.toArray(),
        type: TranslationWordTypeEnum.WORD,
        offset: minOffset,
        duration,
      });
    }

    if (collection.isNotEmpty()) {
      const divider = collection.first();
      collection.splice(0, 1);

      sentences.push({
        originalStartId: divider.id || '',
        originalEndId: divider.id || '',
        divider,
        type: TranslationWordTypeEnum.DIVIDER,
      });
    }
  }

  return sentences;
};

export default convertSubtitleWordsToSentences;
