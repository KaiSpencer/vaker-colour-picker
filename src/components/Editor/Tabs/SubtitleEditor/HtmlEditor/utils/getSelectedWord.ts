import collect from 'collect.js';
import { ITranslationWord } from '~/redux/editor/types';
import { ISentence } from '../types.d';
import getSelectionCharacterOffsetWithinElement from './getSelectionCharacterOffsetWithinElement';

const getSelectedSentence = (
  target: HTMLSpanElement,
  sentences: ISentence[],
): ISentence | null => {
  const [_, originalStartId] =
    target.id.match(/editable-sentence-(\d+)-(\d+)/) || [];

  if (!originalStartId) {
    console.warn('Invalid focus target! Not an EditableSentence!', {
      target,
    });

    return null;
  }

  return collect(sentences).firstWhere(
    'originalStartId',
    parseInt(originalStartId),
  ) as ISentence;
};

const getCaretLocation = (target: HTMLElement): number => {
  const { start: caretLocation } =
    getSelectionCharacterOffsetWithinElement(target);

  const nextCharacter = target.innerText.substring(
    caretLocation,
    caretLocation + 1,
  );

  if (!/^[a-z0-9]+$/i.test(nextCharacter)) {
    return caretLocation + 1;
  }

  return caretLocation;
};

export const getSelectedWordFromSentence = (
  target: HTMLElement,
  sentence: ISentence,
): ITranslationWord | null => {
  const { innerText } = target;

  const caretLocation = getCaretLocation(target);

  const arrayOfWords = innerText.split(' ');
  const wordAtLocation: ITranslationWord | null = collect(arrayOfWords)
    .pipe((collection) =>
      collect(
        collection.reduce(
          ({ collection, sumLengthOfWords }, text) => {
            const sum = sumLengthOfWords + text.length + 1;

            return {
              collection: [
                ...collection,
                { text, index: collection.length, sumLengthOfWords: sum },
              ],
              sumLengthOfWords: sum,
            };
          },
          { collection: [], sumLengthOfWords: 0 },
        ),
      ),
    )
    .pipe(({ items }) => collect(items.collection))
    .skipUntil(({ sumLengthOfWords }) => sumLengthOfWords > caretLocation)
    .pipe((collection) => {
      const first = collection.first();

      return collect(sentence.words || []).pipe(
        (words) => words.get(first?.index) || words.last(),
      );
    });

  return wordAtLocation;
};

export const getSelectedWordFromSentences = (
  target: HTMLElement,
  sentences: ISentence[],
): ITranslationWord | null => {
  const sentence = getSelectedSentence(target, sentences);

  if (!sentence) {
    return null;
  }

  return getSelectedWordFromSentence(target, sentence);
};
