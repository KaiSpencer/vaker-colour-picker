// https://stackoverflow.com/questions/4811822/get-a-ranges-start-and-end-offsets-relative-to-its-parent-container/4812022#4812022
const getSelectionCharacterOffsetWithinElement = (element) => {
  const doc = element.ownerDocument || element.document;
  const win = doc.defaultView || doc.parentWindow;

  if (typeof win.getSelection !== 'undefined') {
    if (win.getSelection().rangeCount > 0) {
      const range = win.getSelection().getRangeAt(0);
      const preCaretRange = range.cloneRange();

      preCaretRange.selectNodeContents(element);
      preCaretRange.setEnd(range.startContainer, range.startOffset);

      const start = preCaretRange.toString().length;
      preCaretRange.setEnd(range.endContainer, range.endOffset);
      const end = preCaretRange.toString().length;

      return { start, end };
    }
  } else {
    const { selection } = doc;

    if (selection.type !== 'Control') {
      const textRange = selection.createRange();
      const preCaretTextRange = doc.body.createTextRange();

      preCaretTextRange.moveToElementText(element);
      preCaretTextRange.setEndPoint('EndToStart', textRange);

      const start = preCaretTextRange.text.length;
      preCaretTextRange.setEndPoint('EndToEnd', textRange);
      const end = preCaretTextRange.text.length;

      return { start, end };
    }
  }

  return { start: 0, end: 0 };
};

export default getSelectionCharacterOffsetWithinElement;
