import React from 'react';
import EditorSwitch from '~/components/ui/Form/EditorSwitch';

interface IToggleHelperToolsProps {
  helperTools: boolean;
  setHelperTools: Function;
}

const ToggleHelperTools: React.FC<IToggleHelperToolsProps> = ({
  helperTools,
  setHelperTools,
}) => {
  return (
    <div className="flex items-center">
      <EditorSwitch
        isActive={helperTools}
        onChange={() => setHelperTools(prev => !prev)}
        name="toogleHelperTools"
      />
      <label
        className="text-font-2 font-extrabold pl-4"
        htmlFor="toogleHelperTools"
      >
        Helper Tools
      </label>
    </div>
  );
};

export default ToggleHelperTools;
