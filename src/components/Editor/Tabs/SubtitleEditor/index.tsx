import React, { FC, useState } from 'react';
import { ITranslationWord } from '~/redux/editor/types';
import SubtitleEditorProvider from './useSubtitleEditor';
import ToggleHelperTools from './ToggleHelperTools';
import HtmlEditor from './HtmlEditor';
import HelperTools from './HelperTools';

interface ISubtitleEditorProps {}

const SubtitleEditor: FC<ISubtitleEditorProps> = () => {
  const [helperTools, setHelperTools] = useState(false);

  return (
    <SubtitleEditorProvider>
      <div
        className="mt-5 min-h-96 ht:grid ht:grid-cols-2 gap-4 overflow-visible"
        id="helper-cursor-change"
      >
        <div
          className="w-full h-full shadow-editor rounded-xl p-3"
          style={{
            overflowY: 'auto',
            overflowX: 'clip',
            resize: 'both',
            maxWidth: '100%',
            minHeight: '25rem',
          }}
        >
          <HtmlEditor />
        </div>
        <div className="mt-0 2xl:mt-0">
          <div className="helper-tools">
            <ToggleHelperTools
              helperTools={helperTools}
              setHelperTools={setHelperTools}
            />
            {helperTools && <HelperTools />}
          </div>
        </div>
      </div>
    </SubtitleEditorProvider>
  );
};

export default SubtitleEditor;
