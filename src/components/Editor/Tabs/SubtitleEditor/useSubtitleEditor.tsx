import React, { FC, createContext, useContext, useState } from 'react';
import { ITranslationWord } from '~/redux/editor/types';
import { setSubtitles } from '~/redux/editor/editor.actions';

interface ISubtitleEditorContext {
  selectedWord: ITranslationWord | null;
  setSelectedWord: React.Dispatch<
    React.SetStateAction<ITranslationWord | null>
  >;
  shownDividerPopup: boolean;
  setShownDividerPopup: React.Dispatch<React.SetStateAction<boolean>>;
  replaceWord: {
    word: string;
    setWord: React.Dispatch<React.SetStateAction<string>>;
    count: number;
    setCount: React.Dispatch<React.SetStateAction<number>>;
    focusedWordIndex: number | null;
    setFocusedWordIndex: React.Dispatch<React.SetStateAction<number | null>>;
  };
}

export const SubtitleEditorContext = createContext<ISubtitleEditorContext>(
  {} as ISubtitleEditorContext,
);
export const useSubtitleEditor = () => useContext(SubtitleEditorContext);

interface ISubtitleEditorProvider {}

const SubtitleEditorProvider: FC<ISubtitleEditorProvider> = ({ children }) => {
  const [selectedWord, setSelectedWord] = useState<ITranslationWord | null>(
    null,
  );

  const [shownDividerPopup, setShownDividerPopup] = useState<boolean>(false);

  const [replaceWord, setReplaceWord] = useState<string>('');
  const [countOfWords, setCountOfWords] = useState<number>(0);
  const [focusedWordIndex, setFocusedWordIndex] = useState<number | null>(null);

  return (
    <SubtitleEditorContext.Provider
      value={{
        selectedWord,
        setSelectedWord,
        shownDividerPopup,
        setShownDividerPopup,
        replaceWord: {
          word: replaceWord,
          setWord: setReplaceWord,
          count: countOfWords,
          setCount: setCountOfWords,
          focusedWordIndex,
          setFocusedWordIndex,
        },
      }}
    >
      {children}
    </SubtitleEditorContext.Provider>
  );
};

export default SubtitleEditorProvider;
