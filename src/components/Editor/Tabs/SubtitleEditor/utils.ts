import collect from 'collect.js';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
  ITranslationMatch,
} from '~/redux/editor/types';
import { DirectionEnum } from './HtmlEditor/types.d';

//get word length or 0 if type is spacer
export function getWordLength(item: ITranslationWord): number {
  if (item.type == TranslationWordTypeEnum.DIVIDER) return 0;
  else if (item.Word?.length) return item.Word.length;
  return 0;
}

//Split an ITranslationWord at a particular offset and return an array of both
//Or just one if input is invalid
export function splitWord(
  item: ITranslationWord,
  offset: number,
): ITranslationWord[] {
  const returnItems: ITranslationWord[] = [];
  returnItems.push(item);
  if (item.type == TranslationWordTypeEnum.DIVIDER) return returnItems;

  //Shut up typescript this isn't even possible
  const len = item.Word ? item.Word.length : 0;
  if (offset == 0 || offset >= len) {
    return returnItems;
  }

  const secondWord: ITranslationWord = {
    //Again, ts, not possible....
    id: item.id ? item.id : 0 + 1,
    Word: item.Word?.toString().substring(offset, item.Word.toString().length),
    Offset: item.Offset,
    Duration: item.Duration,
  };
  item.Word = item.Word?.substring(0, offset);

  returnItems.push(secondWord);
  return returnItems;
}

//Convert node's index + focusOffset provided by DOM to total offSet number
//Calculates spaces automatically
export function convertOffset(
  nodeIndex: number,
  focusOffset: number,
  list: ITranslationWord[],
): number {
  let index = 0;
  let currWord = 0;
  let totalOffset = 0;

  for (currWord; currWord < list.length && index < nodeIndex; currWord++) {
    if (list[currWord].type == TranslationWordTypeEnum.DIVIDER) {
      index += 1;
    } else {
      let str: string = list[currWord].Word ?? '';
      totalOffset += str.length;
    }
  }

  if (currWord == list.length) {
    console.warn(
      'Could not find enough spacers to satisfy nodeIndex in convertOffset()',
    );
    return -1;
  }

  totalOffset += currWord - 1;
  return totalOffset + focusOffset;
}

export function move(
  arr: ITranslationWord[],
  oldIndex: number,
  newIndex: number,
): ITranslationWord[] {
  arr = arr.slice();
  if (
    oldIndex < 0 ||
    newIndex < 0 ||
    newIndex >= arr.length ||
    oldIndex > arr.length
  ) {
    console.warn('invalid input in move() function');
    console.log('oldIndex: ', oldIndex, 'newIndex: ', newIndex);
    return arr;
  }

  const copy = arr[oldIndex];
  arr.splice(oldIndex, 1);
  arr.splice(newIndex, 0, copy);

  return arr;
}

//Takes in offset, return id
export function convertOffsetToId(
  originalOffset: number,
  dividerCount: number,
  list: ITranslationWord[],
) {
  // console.log('debug convert (internal) originalOffset', originalOffset);
  // console.log('debug convert (internal) dividerCount', dividerCount);

  let total = 0;
  let returnVal = -1;
  if (dividerCount == 1) dividerCount += 1;
  let maxCount = 0;

  for (var i = 0; i < list.length; i++) {
    if (total < originalOffset) {
      total += getWordLength(list[i]);
      total += 1;
      maxCount = Math.max(maxCount, total);
    } else if (total - dividerCount <= originalOffset) {
      //causes problems when operator is switched to ==
      console.log(
        'original: ',
        originalOffset,
        'new: ',
        total - dividerCount,
        'total: ',
        total,
      );
      returnVal = list[i].id ?? returnVal;
      break;
    }
  }

  // console.log('debug convert (internal) maxCount: ', maxCount);

  return returnVal;
}

//Returns a list with the specified spacer index deleted
//Or unaltered version if list doesn't have that many spacers
export function deleteSpacer(
  spacerIndex: number,
  list: ITranslationWord[],
): ITranslationWord[] {
  if (spacerIndex < 0) {
    console.warn(
      'Invalid entry for spacerIndex in deleteSpacer. Returning empty array: ',
      spacerIndex,
    );
    return [];
  }
  list = list.slice(); //Shouldn't matter but just in case

  console.log('starting point: ', list);

  let block: ITranslationWord[] = [];
  let currSpacer = 0;

  list.map((elem) => {
    if (elem.type != TranslationWordTypeEnum.DIVIDER) block.push(elem);
    else if (elem.type == TranslationWordTypeEnum.DIVIDER) {
      if (currSpacer != spacerIndex) block.push(elem);
      else console.log('removed spacer at index: ', elem.id);
      currSpacer += 1;
    }
  });

  return block;
}

//Returns list using words provided in string array
//Leaves spacers alone.... I think...
export function changeWords(
  words: Array<string>,
  list: ITranslationWord[],
): ITranslationWord[] {
  const elements: ITranslationWord[] = [];
  let spacerOffset = 0;

  words.forEach((value: string, index: number) => {
    if (list[index + spacerOffset]?.type === TranslationWordTypeEnum.DIVIDER) {
      elements.push({
        id: elements.length,
        type: TranslationWordTypeEnum.DIVIDER,
      });

      spacerOffset++;
    }

    elements.push({
      ...list[index + spacerOffset],
      id: elements.length,
      Word: value,
    });
  });

  return elements;
}

export function getMissingDividerIndices(
  nodeList: NodeListOf<ChildNode & Node>,
): number[] {
  const toDelete: number[] = [];
  let currDivider = 0;

  for (let x = 0; x < nodeList.length - 1; x += 1) {
    if (isTextNode(nodeList[x]) == true) {
      if (isTextNode(nodeList[x + 1]) == true) toDelete.push(currDivider);
      currDivider += 1;
    }
  }

  console.log('should return: ', toDelete);
  return toDelete;
}

//Internal helper functions can reduce memory complexity
export function removeInvalidDividers(
  list: ITranslationWord[],
): ITranslationWord[] {
  list = list.slice();
  const deleteTargets: number[] = [];
  let spacerCount: number = 0;

  //Both while loops desperately need testing
  while (list[0].type == TranslationWordTypeEnum.DIVIDER) list.splice(0, 1);

  while (list[list.length - 1].type == TranslationWordTypeEnum.DIVIDER) {
    list.splice(list.length - 1, 1);
  }

  list.map((elem: ITranslationWord, index: number) => {
    if (elem.type == TranslationWordTypeEnum.DIVIDER) {
      spacerCount += 1;
      if (list[index + 1].type == TranslationWordTypeEnum.DIVIDER)
        deleteTargets.push(spacerCount);
    }
    if ((index = list.length - 1)) return;
  });

  deleteTargets.reverse(); //Prevent creating moving targets by working backwards
  deleteTargets.map((elem: number) => {
    list = deleteSpacer(elem, list);
  });

  return list;
}

export function handleSubtitleChange(
  list: ITranslationWord[],
  wordsGrammatical: ITranslationWord[],
): ITranslationWord[] {
  // Loop the list and return list of indexes of dividers
  let withoutSpacers: any[] = [];
  list.map((el) => {
    if (el.type != TranslationWordTypeEnum.DIVIDER) {
      withoutSpacers.push(el);
    }
  });

  let helper = 20;
  let indexes: number[] = [];

  //punctuation smart dividers placement check for
  const punctuation = ['.', ',', '?', '!'];
  const range = 5;
  //looping the list and checking where the punctuation is
  //if it matches criteria, moving the divider there
  for (let i = 0; i < withoutSpacers.length; i++) {
    for (let x = -range; x <= range; x++) {
      if (
        i === helper &&
        punctuation.includes(
          withoutSpacers[i + x].Word.charAt(
            withoutSpacers[i + x].Word.length - 1,
          ),
        )
      ) {
        indexes.push(i + x);
        helper += 20 + x;
        break;
      }
    }

    if (
      i === helper &&
      punctuation.includes(
        withoutSpacers[i].Word?.charAt(withoutSpacers[i].Word?.length - 1),
      ) == false
    ) {
      indexes.push(i);
      helper += 20;
    }
  }

  let helperIndex = 0;
  const finishedList: ITranslationWord[] = [];
  for (let index = 0; index < wordsGrammatical.length; index++) {
    const element = wordsGrammatical[index];

    finishedList.push({
      id: finishedList.length,
      type: TranslationWordTypeEnum.WORD,
      ...element,
    });

    if (index > 0 && index === indexes[helperIndex]) {
      finishedList.push({
        id: finishedList.length,
        type: TranslationWordTypeEnum.DIVIDER,
      });
      helperIndex += 1;
    }
  }

  return finishedList;
}

function isTextNode(node: Node): boolean {
  return node.nodeType === 3;
}
