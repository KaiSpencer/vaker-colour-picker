import React from 'react';

interface ITabsProps {
  configuration: any[];
  tabIndex: number;
  setTabIndex: Function;
}

const Tabs: React.FC<ITabsProps> = ({
  configuration,
  tabIndex,
  setTabIndex,
}) => {
  return (
    <div className="lg:col-span-8 mt-12 lg:mt-0">
      <div className="flex space-x-3 mx-4 p-2 bg-white shadow-editor rounded-md">
        {configuration.map((tab, i) => (
          <button
            onClick={() => setTabIndex(i)}
            className={`flex py-2 space-x-2 rounded flex-1 w-24 justify-center items-center uppercase font-bold focus:outline-none  
              ${
                tabIndex === i
                  ? 'text-white bg-main'
                  : 'text-main hover:bg-gray-100'
              }`}
          >
            <img
              src={tab.icon}
              alt="panel icon"
              width="25px"
              height="25px"
              className={`${tabIndex === i &&
                'filter saturate-0 brightness-[3]'}`}
            />
            <span>{tab.title}</span>
          </button>
        ))}
      </div>
      {configuration.map(({ view }, i) => (
        <div
          className={`relative px-4 min-h-[520px] max-h-[520px] pt-8 overflow-y-scroll scrollbar-thin 
        scrollbar-thumb-main scrollbar-track-bg3 ${
          tabIndex !== i ? 'hidden' : ''
        }`}
        >
          {view}
        </div>
      ))}
    </div>
  );
};

export default Tabs;
