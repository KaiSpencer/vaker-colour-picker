import React, { useMemo } from 'react';
import { ISubtitleTrack } from '~/components/Editor/types';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { useTimelineContext } from '../../hooks/TimelineContext';

interface ISubtitleTrackProps {
  track: ISubtitleTrack;
  index: number;
}

const SubtitleTrack: React.FC<ISubtitleTrackProps> = ({ track, index }) => {
  const { timelineWidth } = useTimelineContext();

  const { duration } = useVideoEditor();

  const spanStyle = { margin: '0 2px' };
  const spanClassName =
    'w-[2px] h-8 absolute top-[50%] rounded-full transform -translate-y-4 bg-[#333539] transition duration-200';

  const x = useMemo(
    () => (track.start * timelineWidth) / duration,
    [track.start, timelineWidth, duration],
  );
  const width = useMemo(
    () => (track.duration * timelineWidth) / duration,
    [track.duration, timelineWidth, duration],
  );

  if (isNaN(x) || width === Infinity) {
    return <div hidden />;
  }

  return (
    <div
      data-i={index}
      data-x={x}
      data-width={width}
      data-text={track.text}
      className="track h-[100%] p-2 absolute flex items-start justify-center rounded-lg box-border border-2 border-[#e8f1f6] bg-[#e8f1f6] z-auto"
      style={{
        width,
        transform: `translate(${x}px, 0)`,
      }}
    >
      <span style={spanStyle} className={`${spanClassName} left-0`} />
      <span style={spanStyle} className={`${spanClassName} right-0`} />
      <p className="max-w-full max-h-full text-xs leading-4 break-words overflow-clip overflow-hidden">
        {track.text}
      </p>
    </div>
  );
};

export default SubtitleTrack;
