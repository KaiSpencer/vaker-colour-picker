import { GRAB_DIRECTION, IShape } from '../types';
import { getShapes } from './getShapes';

export const generateOldTrackElement = target => {
  if (target.dataset?.initialLeft === undefined) {
    const backgroundElement = target.cloneNode(true);
    backgroundElement.style.zIndex = 0;
    backgroundElement.setAttribute('data-text', '');
    const backgroundElementText = backgroundElement.querySelector('p');
    backgroundElementText.innerText = '';
    backgroundElement.style.opacity = 0.6;
    backgroundElement.id = 'drag-background-element';
    target.parentElement.appendChild(backgroundElement);
  }
};

export const adjustSubtitleTrackField = ({
  event,
  leftScrollSpace,
  positionRef,
  x,
  w,
  width,
  rectLeft,
  deltaLeft,
  deltaRight,
  left,
  actualCursorPosition,
  i,
  right,
}) => {
  let x_ = x,
    w_ = w,
    width_ = width,
    rectLeft_ = rectLeft,
    deltaLeft_ = deltaLeft,
    deltaRight_ = deltaRight,
    left_ = left,
    actualCursorPosition_ = actualCursorPosition,
    i_ = i,
    right_ = right;
  const target = event.target;

  const setInitialRight = () => {
    if (!target.dataset.initialRight) {
      target.dataset.initialRight = x_ + w_ || width_;
    }
  };

  const setInitialLeft = () => {
    if (!target.dataset.initialLeft) {
      target.dataset.initialLeft = x_ || rectLeft_;
    }
  };

  // imaginaryLeft is where the left_ side would normally be if we did not programatically alter the position
  const setImaginaryLeft = () => {
    if (target.dataset?.imaginaryLeft) {
      target.dataset.imaginaryLeft =
        Number(deltaLeft_) + Number(target.dataset.imaginaryLeft);
    }
  };

  // Distinguishing which side is currently "grabbed"
  const determineGrabbedSide = () => {
    if (deltaLeft_ !== 0) {
      target.dataset.resizeSide = GRAB_DIRECTION.LEFT;
      width_ = width_ - deltaLeft_;
      return GRAB_DIRECTION.LEFT;
    } else if (deltaRight_ !== 0) {
      target.dataset.resizeSide = GRAB_DIRECTION.RIGHT;
      width_ = width_ - deltaRight_;
      return GRAB_DIRECTION.RIGHT;
    } else {
      return null;
    }
  };

  // following code is used for the magnetic snap
  const distanceToCursorLeft = Math.abs(
    (target.dataset?.imaginaryLeft || left_) - actualCursorPosition_,
  );
  const distanceToCursorRight = Math.abs(
    left_ + width_ - actualCursorPosition_,
  );
  console.log('imaginaryLeft', target.dataset?.imaginaryLeft);
  console.log('distanceToCursorLeft', distanceToCursorLeft);
  console.log('distanceToCursorRight', distanceToCursorRight);
  console.log('actualCursorPosition', actualCursorPosition_);

  const snapRange = 20;

  // TODO write generic function to calculate and implement all magnetic points (Mika will do this)

  const shapeLeft: IShape = getShapes()[i_ - 1];
  const shapeRight: IShape = getShapes()[i_ + 1];
  const isOverlappingRight = shapeRight && shapeRight.x1 < left_ + width_;
  const isOverlappingLeft = shapeLeft && shapeLeft.x2 > left_;
  const distanceToShapeLeft = Math.abs(
    (target.dataset?.imaginaryLeft || left_) - shapeLeft?.x2,
  );
  const distanceToShapeRight = Math.abs(left_ + width_ - shapeRight?.x1);
  const distanceToBeginning = Math.abs(target.dataset?.imaginaryLeft || left_);
  const lengthOfTimeline =
    document.getElementById('timeline')?.clientWidth || 0;
  const distanceToEnd = Math.abs(left_ + width_ - lengthOfTimeline);
  const distanceToInitialRight =
    target.dataset?.initialRight &&
    Math.abs(target.dataset?.initialRight - (left_ + width_));
  const distanceToInitialLeft = Math.abs(
    (target.dataset?.imaginaryLeft || left_) - target.dataset.initialLeft,
  );
  // following code is used for anti-overlapping

  const handleTargetOverlap = (direction: GRAB_DIRECTION) => {
    if (direction === GRAB_DIRECTION.RIGHT && isOverlappingRight) {
      console.log(' -- right_ hit shapeRight');
      left_ = x_;
      width_ = shapeRight.x1 - x_;
    } else if (direction === GRAB_DIRECTION.LEFT && isOverlappingLeft) {
      console.log(' -- left_ hit shapeLeft');
      left_ = shapeLeft.x2;
      width_ = target.dataset.initialRight - left_;
    }
  };

  const handleMagneticCursorSnap = (direction: GRAB_DIRECTION) => {
    if (direction === GRAB_DIRECTION.RIGHT) {
      if (distanceToCursorRight < snapRange) {
        console.log(' ---- right_ hit magnetic cursor');
        width_ = Math.abs(left_ - actualCursorPosition_);
      }
    } else if (direction === GRAB_DIRECTION.LEFT) {
      if (distanceToCursorLeft < snapRange) {
        console.log(' ---- left_ hit magnetic cursor');
        if (!target.dataset.imaginaryLeft) {
          target.dataset.imaginaryLeft = left_;
        }
        width_ = target.dataset.initialRight - actualCursorPosition_;
      }
    }
  };

  const handleMagneticEnd = () => {
    if (distanceToEnd < snapRange) {
      console.log(' ---- right_ hit magnetic end');
      width_ = Math.abs(left_ - lengthOfTimeline);
    }
  };

  const handleMagneticStart = () => {
    if (distanceToBeginning < snapRange) {
      console.log(' ---- left_ hit magnetic beginning');
      if (!target.dataset.imaginaryLeft) {
        target.dataset.imaginaryLeft = left_;
      }
      width_ = target.dataset.initialRight;
    }
  };

  const handleMagneticInitialPosition = (direction: GRAB_DIRECTION) => {
    if (direction === GRAB_DIRECTION.RIGHT) {
      if (distanceToInitialRight < snapRange / 2) {
        console.log(' ---- right_ hit initial right');
        width_ = Math.abs(left_ - target.dataset.initialRight);
      }
    } else if (direction === GRAB_DIRECTION.LEFT) {
      if (distanceToInitialLeft < snapRange / 2) {
        console.log(' ---- left_ hit initialLeft');
        if (!target.dataset.imaginaryLeft) {
          target.dataset.imaginaryLeft = left_;
        }
        width_ = target.dataset.initialRight - target.dataset.initialLeft;
      }
    }
  };

  const handleMagneticNeighbourField = (direction: GRAB_DIRECTION) => {
    if (direction === GRAB_DIRECTION.RIGHT) {
      if (
        (shapeRight && isOverlappingRight) ||
        distanceToShapeRight < snapRange
      ) {
        console.log(' -- right_ hit shapeRight');
        left_ = x_;
        width_ = shapeRight.x1 - x_;
      }
    } else if (direction === GRAB_DIRECTION.LEFT) {
      if (distanceToShapeLeft < snapRange && shapeLeft) {
        console.log(' ---- left_ hit magnetic shapeLeft');
        if (!target.dataset.imaginaryLeft) {
          target.dataset.imaginaryLeft = left_;
        }
        width_ = target.dataset.initialRight - shapeLeft.x2;
      }
    }
  };

  const handleLeftSideCleanup = () => {
    const minimumWidth = 30;
    right_ = target.dataset.initialRight;
    left_ = right_ - width_;
    if (width < minimumWidth) {
      left_ = right_ - minimumWidth;
      width_ = minimumWidth;
    }
    if (shapeLeft && shapeLeft.x2 > left_) {
      left_ = shapeLeft.x2;
      width_ = right_ - left_;
    }
  };

  const assureMinimumWidth = (direction: GRAB_DIRECTION) => {
    if (direction === GRAB_DIRECTION.LEFT) {
      const minimumWidth = 30;
      right_ = target.dataset.initialRight;
      left_ = right_ - width_;
      if (width_ < minimumWidth) {
        left_ = right_ - minimumWidth;
        width_ = minimumWidth;
      }
      if (shapeLeft && shapeLeft.x2 > left_) {
        left_ = shapeLeft.x2;
        width_ = right_ - left_;
      }
    }
  };

  const setInitialWidth = () => {
    if (!target.dataset.initialWidth) {
      target.dataset.initialWidth = Number(target.style.width.slice(0, -2));
    }
  };

  const getData = () => {
    return {
      left_,
      width_,
      right_,
    };
  };

  return {
    assureMinimumWidth,
    getData,
    setImaginaryLeft,
    setInitialLeft,
    setInitialRight,
    setInitialWidth,
    handleLeftSideCleanup,
    handleMagneticCursorSnap,
    handleMagneticEnd,
    handleMagneticInitialPosition,
    handleMagneticNeighbourField,
    handleMagneticStart,
    handleTargetOverlap,
    determineGrabbedSide,
  };
};
