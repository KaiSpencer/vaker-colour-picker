import { IShape } from '../types';

export const getShapes = (): IShape[] => {
  const timeline = document.getElementById('timeline')! as HTMLDivElement;
  const shapes = new Array<{ i: number; x1: number; x2: number }>();
  for (let i = 0; i < timeline.children.length; i++) {
    let divShape = timeline.children[i] as HTMLDivElement;
    let x1 = parseFloat(divShape.dataset.x || '');
    let w = parseFloat(divShape.dataset.width || '');
    let x2 = x1 + w;
    shapes.push({ i, x1, x2 });
  }
  return shapes;
};
