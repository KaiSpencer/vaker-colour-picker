import React, { FC, useEffect, useContext, useRef } from 'react';
import interact from 'interactjs';
import { ISubtitleTrack } from '~/components/Editor/types';
import { TimelineContext } from '../../hooks/TimelineContext';
import {
  adjustSubtitleTrackField,
  generateOldTrackElement,
} from './helper/adjustSubtitleTrackField';
import { getShapes } from './helper/getShapes';
import SubtitleTrack from './SubtitleTrack';
import { GRAB_DIRECTION, IShape } from './types';

interface ISubtitleTrackProps {
  tracks: ISubtitleTrack[];
  updateTracks: (updateTrack: ISubtitleTrack[]) => void;
  leftScrollSpace: {
    current: number;
  };
  className: string;
}

const SubtitleTrackEditor: FC<ISubtitleTrackProps> = ({
  tracks,
  updateTracks,
  leftScrollSpace,
  ...props
}) => {
  const { timelineWidth, position } = useContext(TimelineContext);
  const positionRef = useRef(0);

  useEffect(() => {
    console.debug('POSITION EFFECT', position);
    positionRef.current = position;
    console.debug('POSITION REF', positionRef.current);
  }, [position]);

  useEffect(() => {
    console.debug('REINITILILIZE');
    initializeInteractJs();
  }, [tracks]);

  const initializeInteractJs = () => {
    interact('.track')
      .resizable({
        edges: { left: true, right: true, top: false, bottom: false },
        inertia: true,
        modifiers: [
          interact.modifiers.restrictEdges({ outer: 'parent' }),
          interact.modifiers.restrictSize({ min: { width: 30, height: 50 } }),
        ],
        listeners: {
          end: (val) => {
            updateTracks(val);
            delete val.target.dataset.initialWidth;
            delete val.target.dataset.initialLeft;
            delete val.target.dataset.initialRight;
            delete val.target.dataset.imaginaryLeft;
            const backgroundElement = document.getElementById(
              'drag-background-element',
            );
            if (backgroundElement) {
              backgroundElement.parentElement?.removeChild(backgroundElement);
            }
            // delete val.target.dataset.magneticStartWidth
          },
          move: handleResizeMove,
        },
      })
      .draggable({
        inertia: true,
        startAxis: 'x',
        lockAxis: 'x',
        modifiers: [
          interact.modifiers.restrictRect({
            restriction: 'parent',
            endOnly: false,
          }),
        ],
        listeners: {
          end: (val) => {
            updateTracks(val);
            delete val.target.dataset.initialWidth;
          },
          move: handleDragMove,
        },
      });
  };

  const handleResizeMove = (event: any) => {
    // initialization of variables in the scope of moving
    const target = event.target;
    generateOldTrackElement(target);

    const i: number = parseFloat(target.dataset.i) || 0;
    let x: number = parseFloat(target.dataset.x) || 0;
    const w: number = parseFloat(target.dataset.width) || 0;
    const deltaLeft: number = event.deltaRect.left;
    const deltaRight: number = event.deltaRect.right;
    console.debug('leftScrollSpace', leftScrollSpace.current);
    const actualCursorPosition = positionRef.current + leftScrollSpace.current;

    let width: number = event.rect.width;
    const rectRight: number = event.rect.right;
    const rectLeft: number = event.rect.left;

    let left = x + deltaLeft;
    let right =
      rectRight + deltaRight - Math.abs(rectRight + deltaRight - left - width);

    const Adjuster = adjustSubtitleTrackField({
      event,
      leftScrollSpace,
      positionRef,
      actualCursorPosition,
      deltaLeft,
      deltaRight,
      i,
      left,
      rectLeft,
      right,
      w,
      width,
      x,
    });

    Adjuster.setInitialRight();
    Adjuster.setInitialLeft();
    Adjuster.setImaginaryLeft();

    const grabbedSide = Adjuster.determineGrabbedSide();

    if (!grabbedSide) return;

    Adjuster.handleTargetOverlap(grabbedSide);
    Adjuster.handleMagneticCursorSnap(grabbedSide);
    Adjuster.handleMagneticInitialPosition(grabbedSide);
    Adjuster.handleMagneticNeighbourField(grabbedSide);

    if (grabbedSide === GRAB_DIRECTION.RIGHT) {
      Adjuster.handleMagneticEnd();
    } else if (grabbedSide === GRAB_DIRECTION.LEFT) {
      Adjuster.handleMagneticStart();
      Adjuster.handleLeftSideCleanup();
    }

    Adjuster.assureMinimumWidth(grabbedSide);

    Adjuster.setInitialWidth();

    const {
      left_: newLeft,
      width_: newWidth,
      right_: newRight,
    } = Adjuster.getData();

    console.debug('On handleResizeMove!', {
      newLeft,
      newWidth,
      newRight,
      left,
      width,
      right,
    });

    target.style.transform = `translateX(${newLeft}px)`;
    target.style.width = newWidth + 'px';
    target.dataset.width = newWidth;
    target.dataset.x = newLeft;
  };

  const handleDragMove = (event: any) => {
    const target = event.target;
    event.prevent;
    const i = parseFloat(target.dataset.i) || 0;
    let x = (parseFloat(target.dataset.x) || 0) + event.dx;
    const w = parseFloat(target.dataset.width) || 0;
    const x2 = x + w;

    if (x < 0) return 0;
    if (x2 > timelineWidth) return timelineWidth;

    const shapeLeft: IShape = getShapes()[i - 1];
    const shapeRight: IShape = getShapes()[i + 1];

    if (shapeLeft && shapeLeft.x2 >= x) {
      x -= event.dx;
    } else if (shapeRight && shapeRight.x1 <= x2) {
      x -= event.dx;
    }

    if (!target.dataset.initialWidth) {
      target.dataset.initialWidth = Number(target.style.width.slice(0, -2));
    }

    target.style.transform = `translateX(${x}px)`;
    target.dataset.x = x;
  };

  return (
    <div {...props} id="timeline">
      {tracks.map((track, index) => (
        <SubtitleTrack key={index} track={track} index={index} />
      ))}
    </div>
  );
};

export default SubtitleTrackEditor;
