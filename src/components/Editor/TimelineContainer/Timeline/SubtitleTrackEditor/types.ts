export interface IShape {
  i: number;
  x1: number;
  x2: number;
}

export enum GRAB_DIRECTION {
  LEFT = 'left',
  RIGHT = 'right',
}
