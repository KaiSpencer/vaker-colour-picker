/**
 * @author Gerardo Perez <gperezmanjarrez@gmail.com>
 * This file contains left and right divs to move the cursor to initial and end position
 */
import React, { createRef } from 'react';

interface TimelineClickableZoneProps {}

const TimelineClickableZone: React.FC<TimelineClickableZoneProps> = () => {
  const zone = createRef<HTMLDivElement>();

  const leftClasses = 'absolute inset-y-0 w-4 -left-4';
  const rightClasses = 'absolute inset-y-0 w-4 -right-4';

  return (
    <>
      <div className={leftClasses} />
      <div
        ref={zone}
        id="timeline-clickable"
        className="absolute inset-0 -top-10 bottom-3"
      />
      <div className={rightClasses} />
    </>
  );
};

export default TimelineClickableZone;
