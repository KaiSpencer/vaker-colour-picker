import React from 'react';

interface IArrowBottomProps {
  hover: boolean;
}

const ArrowBottom = ({ hover }: IArrowBottomProps) => {
  return (
    <div
      className="absolute top-2 bg-main z-20"
      style={{
        width: '.55rem',
        height: '.55rem',
        transform: 'translate(-.3rem, 14.4rem)',
        clipPath: 'polygon(50% 0%, 0% 100%, 100% 100%)',
      }}
    />
  );
};

export default ArrowBottom;
