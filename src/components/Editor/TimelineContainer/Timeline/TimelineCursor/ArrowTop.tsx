import React from 'react';

interface IArrowTopProps {
  hover: boolean;
}

const ArrowTop = ({ hover }: IArrowTopProps) => {
  return (
    <div
      className="absolute top-2 bg-main z-20"
      style={{
        width: '.55rem',
        height: '.55rem',
        transform: 'translate(-.3rem, -1rem)',
        background: hover ? 'red' : '',
        clipPath: 'polygon(50% 100%, 0 0, 100% 0)',
      }}
    />
  );
};

export default ArrowTop;
