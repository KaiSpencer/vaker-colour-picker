import { useContext } from 'react';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { TimelineContext } from '../../hooks/TimelineContext';

interface ITimestampProps {
  hover: boolean;
  cursorDragDifference: number;
}

const Timestamp = (props: ITimestampProps) => {
  const timelineContext = useContext(TimelineContext);
  const { position, leftScrollSpace, timelineWidth } = timelineContext;
  const { duration, playing } = useVideoEditor();

  const getTimestampString = () => {
    const minutes = String(
      Math.floor(
        ((position + leftScrollSpace.current + props.cursorDragDifference) *
          duration) /
          timelineWidth /
          60,
      ),
    ).padStart(2, '0');

    const decimalSeconds = String(
      (((position + leftScrollSpace.current + props.cursorDragDifference) *
        duration) /
        timelineWidth) %
        60,
    );

    const [seconds, milliSeconds] = decimalSeconds.split('.');

    if (![minutes, seconds, milliSeconds].every(val => val)) {
      return null;
    }

    // MilliSeconds is not used currently
    return `${minutes}.:${seconds.padStart(2, '0')}`;
  };

  return (
    <span
      style={{
        opacity: props.hover || playing ? 0.75 : 0,
      }}
      className="absolute bottom-0 ml-1 text-xs text-[#929292] font-semibold transition duration-75 z-50"
    >
      {getTimestampString()}
    </span>
  );
};

export default Timestamp;
