import interact from 'interactjs';
import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { TimelineContext } from '../../hooks/TimelineContext';
import ArrowBottom from './ArrowBottom';
import ArrowTop from './ArrowTop';
import Timestamp from './Timestamp';

interface TimelineCursorProps {
  wrapper: {
    current: any;
  };
  leftScrollSpace: {
    current: number;
  };
}

const TimelineCursor: React.FC<TimelineCursorProps> = (
  props: TimelineCursorProps,
) => {
  const { duration, setPlayedSeconds } = useVideoEditor();
  const { position, timelineWidth } = useContext(TimelineContext);
  const [hover, setHover] = useState<boolean>(false);
  const [cursorDragDifference, setCursorDragDifference] = useState(0);

  const handleMove = (event: any) => {
    const target = event.target;
    const parsedX = parseFloat(target.dataset.x);
    let x = (parsedX || 0) + event.dx;
    setCursorDragDifference(x - position);
    target.style.transform = `translate(calc(-50% + ${x}px), -1rem)`;
    target.dataset.x = x;
  };

  const handleEnd = ({ target, dx }) => {
    setCursorDragDifference(0);

    const x = (parseFloat(target.dataset.x) || 0) + dx;
    target.style.transform = `translate(calc(-50% + ${x}px), -1rem)`;
    target.dataset.x = x;

    setPlayedSeconds(
      ((x + props.leftScrollSpace.current) * duration) / timelineWidth,
    );
  };

  useEffect(() => {
    interact('#timeline-cursor').draggable({
      inertia: false,
      startAxis: 'x',
      lockAxis: 'x',
      modifiers: [
        interact.modifiers.restrictRect({
          restriction: 'parent',
          endOnly: false,
        }),
      ],
      listeners: {
        move: handleMove,
        end: handleEnd,
      },
    });
  }, [position]);

  return (
    // cursor bar
    <div
      data-x={position ? position : 0}
      id="timeline-cursor"
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      className="absolute -top-2 bottom-0 border-l border-font1 z-10"
      style={{
        width: '1px',
        zIndex: 100,
        //This probably causes the weird behavior when position is NaN
        //But it doesn't explain how it *becomes* NaN
        transform: `translate(calc(-20% + ${position}px), -1rem)`,
        height: '15rem',
      }}
    >
      <div
        className="absolute -top-2 -left-2 z-20"
        style={{
          width: '1rem',
          height: '2rem',
        }}
      ></div>
      <ArrowTop hover={hover} />
      <ArrowBottom hover={hover} />
      <Timestamp hover={hover} cursorDragDifference={cursorDragDifference} />
    </div>
  );
};

export default TimelineCursor;
