import React, { useContext, useEffect, useState } from 'react';
import { TimelineContext } from '../hooks/TimelineContext';

const TimelineEnds: React.FC = () => {
  const { zoom, scrolling } = useContext(TimelineContext);
  const [opacity, setOpacity] = useState<number>(0);

  useEffect(() => {
    if (zoom !== 1 && scrolling) {
      setOpacity(1);
    } else {
      setOpacity(0);
    }
  }, [zoom, scrolling]);

  return (
    <>
      <div
        style={{ opacity }}
        className="absolute inset-y-0 -left-2 w-32 bg-gradient-to-r from-white to-transparent z-50"
      ></div>
      <div
        style={{ opacity }}
        className="absolute inset-y-0 -right-2 w-32 bg-gradient-to-l from-white to-transparent z-50"
      ></div>
    </>
  );
};

export default TimelineEnds;
