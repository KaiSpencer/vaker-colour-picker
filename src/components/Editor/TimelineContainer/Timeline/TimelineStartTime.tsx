import React, { FC } from 'react';
import { useTimelineContext } from '~/components/Editor/TimelineContainer/hooks/TimelineContext';

const TimelineStartTime: FC = () => {
  const { minutes = NaN, seconds = NaN } =
    useTimelineContext()?.zoomStartTime ?? {};

  return (
    <span className="pl-1 border-l border-font2">
      {`${String(minutes).padStart(2, '0')}.:${String(seconds).padStart(
        2,
        '0',
      )}`}
    </span>
  );
};

export default TimelineStartTime;
