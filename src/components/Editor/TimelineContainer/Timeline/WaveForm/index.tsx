/**
 * @author Gerardo Perez <gperezmanjarrez@gmail.com>
 * This file contains the audio waveform
 */
import React, { FC, createRef, useContext, useEffect, useMemo } from 'react';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { TimelineContext } from '../../hooks/TimelineContext';

interface IWaveFormProps {
  width: number;
  className: string;
  style: {};
}

const WaveForm: FC<IWaveFormProps> = ({ width: wrapperWidth, ...props }) => {
  const { audioChannelData, duration } = useVideoEditor();
  const { timelineWidth, zoomStartTime, zoomEndTime } =
    useContext(TimelineContext);

  const canvasRef = createRef<HTMLCanvasElement>();

  const numberOfSamples = Math.round(timelineWidth / 10);

  const rawDataFromAudioBuffer = useMemo(
    () => audioChannelData,
    [audioChannelData],
  );

  const bufferData = useMemo(() => {
    if (!rawDataFromAudioBuffer) {
      return [];
    }

    const blockSize = Math.floor(
      rawDataFromAudioBuffer.length / numberOfSamples,
    );

    // Normalize data based on highest peek
    // TODO: Only supporting mono right now
    const filteredData: number[] = [];
    for (let i = 0; i < numberOfSamples; i++) {
      const blockStart = blockSize * i;

      let sum = 0;
      for (let j = 0; j < blockSize; j++) {
        sum = sum + Math.abs(rawDataFromAudioBuffer[blockStart + j]);
      }

      filteredData.push(sum / blockSize);
    }

    return filteredData;
  }, [numberOfSamples, rawDataFromAudioBuffer]);

  const bufferDataTrimmedAndNormalised = useMemo(() => {
    // Trim the data to just the currently visible part of the timeline
    const zoomStartTimeSeconds =
      zoomStartTime.minutes * 60 + zoomStartTime.seconds;
    const zoomEndTimeSeconds = zoomEndTime.minutes * 60 + zoomEndTime.seconds;

    const startIndex = Math.round(
      (zoomStartTimeSeconds / duration) * bufferData.length,
    );
    const endIndex = Math.round(
      (zoomEndTimeSeconds / duration) * bufferData.length,
    );

    const multiplier = Math.pow(Math.max(...bufferData), -1);

    return bufferData
      .slice(startIndex, endIndex + 1)
      .map((n) => n * multiplier);
  }, [bufferData, zoomStartTime, zoomEndTime]);

  useEffect(() => {
    if (!canvasRef || !bufferDataTrimmedAndNormalised) {
      return;
    }

    const canvas = canvasRef.current!;
    const dpr = window.devicePixelRatio || 1;
    const padding = 0;

    const equalizerOffset = -9;

    canvas.width = canvas.offsetWidth * dpr;
    canvas.height = (canvas.offsetHeight + padding) * dpr;

    const ctx = canvas.getContext('2d')!;
    ctx.scale(dpr, dpr);
    ctx.translate(0, canvas.offsetHeight);

    ctx.fillStyle = '#e8f1f6';
    ctx.strokeStyle = '#e8f1f6';
    ctx.beginPath();
    ctx.moveTo(0, 0);

    const width = wrapperWidth / bufferDataTrimmedAndNormalised.length;
    for (let i = 1; i < bufferDataTrimmedAndNormalised.length; i++) {
      const x1 = width * (i - 1);
      const y1 = -Math.abs(bufferDataTrimmedAndNormalised[i - 1]) * 50;
      const x2 = width * i;
      const y2 = -Math.abs(bufferDataTrimmedAndNormalised[i]) * 50;

      ctx.bezierCurveTo(
        x1 + (x2 - x1) / 2,
        y1 - equalizerOffset,
        x1 + (x2 - x1) / 2,
        y2 - equalizerOffset,
        x2,
        y2 - equalizerOffset,
      );
    }

    ctx.bezierCurveTo(wrapperWidth, 0, wrapperWidth, 0, wrapperWidth, 0);

    ctx.fill();
    ctx.stroke();
    ctx.beginPath();

    // Draw box as reference for user to see bottom of audio visualization
    ctx.rect(0, 0, wrapperWidth, -2);
    ctx.fill();
    ctx.stroke();
  }, [
    canvasRef,
    bufferData,
    bufferDataTrimmedAndNormalised,
    wrapperWidth,
    typeof window === 'undefined' || window.devicePixelRatio,
  ]);

  return (
    <div {...props}>
      <canvas ref={canvasRef} id="waveform" className="w-[100%] h-[100%]" />
    </div>
  );
};

export default WaveForm;
