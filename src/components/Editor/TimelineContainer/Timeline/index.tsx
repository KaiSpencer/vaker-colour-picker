import React, { useMemo, useEffect, useState, useCallback } from 'react';
import { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NodeList } from 'subtitle';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { useTimelineContext } from '../hooks/TimelineContext';
import { updateSubtitleWithVTTNode } from '~/redux/editor/editor.actions';
import { selectSubtitlesWithVTTNodeList } from '~/redux/editor/editor.selectors';
import generateSubtitleTracks from './SubtitleTrackEditor/helper/generateSubtitleTracks';
import SubtitleTrackEditor from './SubtitleTrackEditor';
import WaveForm from './WaveForm';
import { TIMELINE_CLICKABLE_ZONES } from '../constants';
import { getIndexOfTrack } from '../helper/getIndexOfTrack';
import { getModifiedTrack } from '../helper/getModifiedTrack';
import { roundSubtitleTrack } from '../helper/roundSubtitleTrack';
import { roundToSecondDigit } from '../helper/roundToSecondDigit';
import TimelineClickableZone from './TimelineClickableZone';
import TimelineCursor from './TimelineCursor';
import TimelineEndTime from './TimelineEndTime';
import TimelineStartTime from './TimelineStartTime';

export const Timeline = () => {
  const dispatch = useDispatch();

  const { duration, setPlayedSeconds } = useVideoEditor();

  const subtitlesWithVTTNodeList: NodeList = useSelector(
    selectSubtitlesWithVTTNodeList,
  );
  const subtitleTracks = useMemo(
    () => generateSubtitleTracks(subtitlesWithVTTNodeList),
    [subtitlesWithVTTNodeList],
  );

  const timelineContext = useTimelineContext();
  const {
    leftScrollSpace,
    timelineWidth,
    timelineStart,
    timelineEnd,
    setScrolling,
    timelineRef,
    wrapper,
    cursorIsVisible,
  } = timelineContext;

  const [wrapperWidth, setWrapperWidth] = useState<number | null>(null);
  useEffect(() => {
    if (!!wrapper.current?.clientWidth) {
      setWrapperWidth(Number(wrapper.current.clientWidth));
    }
  }, [wrapper]);

  const [wrapperScroll, setWrapperScroll] = useState<number | null>(null);
  const onWrapperScroll = useCallback(
    ({ target }) => {
      setScrolling(true);

      setWrapperScroll(target.scrollLeft);
    },
    [setWrapperScroll, setScrolling],
  );

  const handleUpdateTracks = (updatedTrack: any): void => {
    const modifiedTrack = getModifiedTrack(updatedTrack, subtitleTracks);
    if (!modifiedTrack) return;

    const indexOfModifiedTrack = getIndexOfTrack(modifiedTrack, subtitleTracks);

    const oneSecondWidth =
      updatedTrack.target?.dataset?.initialWidth / modifiedTrack.duration;
    const newWidth = updatedTrack?.target.dataset?.width;
    const oldWidth = modifiedTrack.duration * oneSecondWidth;
    let newDuration = roundToSecondDigit(
      (newWidth * modifiedTrack.duration) / oldWidth,
    );
    let newStart = roundToSecondDigit(
      updatedTrack.target?.dataset?.x / oneSecondWidth,
    );

    const updatedTrackItem = {
      ...modifiedTrack,
      duration: newDuration,
      start: newStart,
    };

    const updatedSubtitleTracks = [...subtitleTracks];
    updatedSubtitleTracks[indexOfModifiedTrack] =
      roundSubtitleTrack(updatedTrackItem);

    const leftItem = updatedSubtitleTracks[indexOfModifiedTrack - 1];
    const rightItem = updatedSubtitleTracks[indexOfModifiedTrack + 1];
    const collidedLeft =
      leftItem && updatedTrackItem.start < leftItem.start + leftItem.duration;
    const collidedRight =
      rightItem &&
      roundToSecondDigit(updatedTrackItem.start) +
        roundToSecondDigit(updatedTrackItem.duration) >
        rightItem.start;

    if (collidedLeft) {
      console.debug('collided left');
      const deltaEndToStart =
        modifiedTrack.start - (leftItem.start + leftItem.duration);
      const start = modifiedTrack.start - deltaEndToStart;
      const duration =
        updatedTrack.target.dataset.initialWidth / oneSecondWidth +
        deltaEndToStart;

      updatedSubtitleTracks[indexOfModifiedTrack] = {
        ...updatedSubtitleTracks[indexOfModifiedTrack],
        start,
        duration,
      };
    }
    if (collidedRight) {
      console.debug('collided right', newStart, newDuration);
      const start = modifiedTrack.start;
      const duration =
        updatedSubtitleTracks[indexOfModifiedTrack + 1].start -
        modifiedTrack.start;

      updatedSubtitleTracks[indexOfModifiedTrack] = {
        ...modifiedTrack,
        start,
        duration,
      };
    }

    if (updatedTrackItem.duration <= 0) {
      const start = modifiedTrack.start;
      const duration =
        updatedSubtitleTracks[indexOfModifiedTrack + 1].start -
        modifiedTrack.start;

      updatedSubtitleTracks[indexOfModifiedTrack] = {
        ...modifiedTrack,
        start,
        duration,
      };
    }

    dispatch(updateSubtitleWithVTTNode(roundSubtitleTrack(updatedTrackItem)));
  };

  const onTimelineClick = useCallback(
    ({ target, pageX }) => {
      if (TIMELINE_CLICKABLE_ZONES.indexOf(target.id) > 0) {
        const wrapperX = Number(wrapper.current?.getClientRects()[0].x);
        const pos = pageX - wrapperX + timelineStart;
        const leftScrollSpace: number = wrapper.current?.scrollLeft || 0;
        const newPlayedSeconds =
          ((pos + leftScrollSpace) * duration) / timelineWidth;

        setPlayedSeconds(newPlayedSeconds);
      }
    },
    [wrapper, timelineStart, duration, timelineWidth, setPlayedSeconds],
  );

  return (
    <div
      className="w-full mt-8 relative bg-white overflow-y-visible"
      style={{
        padding: '0 4em',
        borderRadius: '40px', // TODO: See if converting to % makes difference for responsive design
        boxShadow: '0 0 8px 0 rgba(0, 0, 0, 0.1)',
      }}
    >
      <div className="relative" onClick={onTimelineClick}>
        <TimelineClickableZone />
        {/* Scrollbar */}
        <div
          ref={wrapper}
          onScroll={onWrapperScroll}
          id="scrollDiv"
          className="
                scrollbar-thin scrollbar-thumb-main scrollbar-track-bg3 scrollbar-thumb-rounded-full scrollbar-track-rounded-full
                static h-[160px] py-4 overflow-x-auto
                overflow-y-auto box-border pb-4"
        >
          {cursorIsVisible.current && (
            <TimelineCursor
              wrapper={wrapper}
              leftScrollSpace={leftScrollSpace}
            />
          )}
          <div
            ref={timelineRef}
            style={{ width: timelineWidth }}
            className="w-full h-[100%] relative overflow-x-hidden overflow-y-visible"
          >
            <SubtitleTrackEditor
              className="h-[50%] w-full relative"
              tracks={subtitleTracks}
              updateTracks={handleUpdateTracks}
              leftScrollSpace={leftScrollSpace}
            />
            <WaveForm
              className="h-[50%]"
              style={{
                width: wrapperWidth ?? timelineWidth,
                marginLeft: `${
                  (wrapperScroll || wrapper.current?.scrollLeft) ?? 0
                }px`,
              }}
              width={wrapperWidth ?? timelineWidth}
            />
          </div>
        </div>
      </div>
      <div className="relative my-2 pb-2 flex items-center justify-between font-semibold opacity-50 text-xs text-font2">
        <TimelineStartTime />
        <TimelineEndTime />
      </div>
    </div>
  );
};
