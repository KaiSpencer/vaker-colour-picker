/**
 * @author Gerardo Perez <gperezmanjarrez@gmail.com>
 * This component toggles the timeline
 */
import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import EditorSwitch from '~/components/ui/Form/EditorSwitch';
import { TimelineContext } from './hooks/TimelineContext';

const TimelineToggle: React.FC = () => {
  const { visible, setVisible } = useContext(TimelineContext);
  const [loading, setLoading] = useState(false);

  const toggleVisible = () => {
    if (!visible) setLoading(true);
    setVisible(v => !v);
  };

  useEffect(() => {
    if (loading) {
      setTimeout(() => {
        setLoading(false);
      }, 3000);
    }
  }, [loading]);

  return (
    <>
      <div>
        <div className="flex items-center">
          <EditorSwitch isActive={visible} onChange={toggleVisible} />
          <div className="text-font-2 font-extrabold pl-4">Timeline on/off</div>
        </div>
        {loading && (
          <div className="mt-4">
            <p className="text-xs text-[#929292] font-semibold">
              Adjust the timing of your subtitles.
            </p>
          </div>
        )}
      </div>
    </>
  );
};

export default TimelineToggle;
