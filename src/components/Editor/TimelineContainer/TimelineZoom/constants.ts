export const ZOOM_DEFAULTS = {
  initial: 1.0,
  step: 2.0,
  min: 1,
  max: 64.0,
};
