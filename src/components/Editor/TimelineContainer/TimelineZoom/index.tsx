import React, { forwardRef, useImperativeHandle, useContext } from 'react';
import { MinusIcon, PlusIcon } from '@heroicons/react/solid';
import { TimelineContext } from '../hooks/TimelineContext';
import { ZOOM_DEFAULTS } from './constants';
import { IZoomDefaults } from './types';

const TimelineZoom: React.ForwardRefRenderFunction<any> = (_props, ref) => {
  const { setZoom } = useContext(TimelineContext);

  const defaults: IZoomDefaults = ZOOM_DEFAULTS;

  useImperativeHandle(ref, () => ({
    incrementZoom,
    decrementZoom,
  }));

  const incrementZoom = () => {
    setZoom(z => (z >= defaults.max ? defaults.max : z * defaults.step));
  };

  const decrementZoom = () => {
    setZoom(z => (z <= defaults.min ? defaults.min : z / defaults.step));
  };

  return (
    <>
      <div className="inline-flex font-extrabold space-x-3 px-2 items-center justify-between bg-white rounded-full shadow select-none">
        <MinusIcon
          className="h-4 w-4 cursor-pointer"
          onClick={() => decrementZoom()}
        />
        <div
          className="cursor-pointer"
          onClick={() => setZoom(defaults.initial)}
        >
          Fit Timeline
        </div>
        <PlusIcon
          className="h-4 w-4 cursor-pointer"
          onClick={() => incrementZoom()}
        />
      </div>
    </>
  );
};

export default forwardRef(TimelineZoom);
