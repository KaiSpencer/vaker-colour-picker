export interface IZoomDefaults {
  initial: number;
  step: number;
  min: number;
  max: number;
}
