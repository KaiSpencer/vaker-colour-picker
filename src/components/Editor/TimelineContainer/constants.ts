export const TIMELINE_CLICKABLE_ZONES = [
  'wrapper',
  'timeline',
  'waveform',
  'timeline-clickable',
];

export const KEYS = {
  INCREMENT_ZOOM: '+',
  DECREMENT_ZOOM: '-',
  FORWARD: '>',
  BACKWARD: '<',
};

export const TIMEWARP_AMOUNT = 5;
