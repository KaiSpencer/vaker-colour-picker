import { ISubtitleTrack } from '~/components/Editor/types';

export const getIndexOfTrack = (
  track: ISubtitleTrack,
  allTracks: ISubtitleTrack[],
) => allTracks.map((item) => item.text).indexOf(track.text);
