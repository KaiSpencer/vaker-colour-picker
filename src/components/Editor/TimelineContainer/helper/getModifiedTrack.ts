import { ISubtitleTrack } from '~/components/Editor/types';
export const getModifiedTrack = (updatedTrack, allTracks: ISubtitleTrack[]) =>
  allTracks.find((track) => track.text === updatedTrack?.target?.innerText);
