import { roundToSecondDigit } from './roundToSecondDigit';

export const roundSubtitleTrack = (subtitleTrack) => ({
  ...subtitleTrack,
  start: roundToSecondDigit(subtitleTrack.start),
  duration: roundToSecondDigit(subtitleTrack.duration),
});
