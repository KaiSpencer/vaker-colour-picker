import { createContext, useContext, useMemo } from 'react';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { ITimelineContext } from '../types';

export const TimelineContext = createContext<ITimelineContext>(
  {} as ITimelineContext,
);

export const useTimelineContext = () => useContext(TimelineContext);

export function withTimelineContext(Component) {
  return function ThemeComponent(props) {
    return (
      <TimelineContext.Consumer>
        {(contexts) => <Component {...props} {...contexts} />}
      </TimelineContext.Consumer>
    );
  };
}

interface ITimelineProvider {
  value: ITimelineContext;
}

const TimelineProvider: React.FC<ITimelineProvider> = ({ value, children }) => {
  const { timelineStart, timelineEnd, timelineWidth } = value;
  const { current: leftScrollSpace } = value.leftScrollSpace;

  const { duration } = useVideoEditor();

  const zoomStartTime = useMemo<{ minutes: number; seconds: number }>(() => {
    const calculatedSeconds =
      ((timelineStart + leftScrollSpace) * duration) / timelineWidth;

    const minutes = Math.floor(calculatedSeconds / 60);
    const seconds = Math.floor(calculatedSeconds % 60);

    return { minutes, seconds };
  }, [leftScrollSpace, timelineStart, duration, timelineWidth]);

  const zoomEndTime = useMemo<{ minutes: number; seconds: number }>(() => {
    const calculatedSeconds =
      ((timelineEnd + leftScrollSpace) * duration) / timelineWidth;

    const minutes = Math.floor(calculatedSeconds / 60);
    const seconds = Math.floor(calculatedSeconds % 60);

    return { minutes, seconds };
  }, [leftScrollSpace, timelineEnd, duration, timelineWidth]);

  return (
    <TimelineContext.Provider value={{ ...value, zoomStartTime, zoomEndTime }}>
      {children}
    </TimelineContext.Provider>
  );
};

export default TimelineProvider;
