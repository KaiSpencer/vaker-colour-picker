import React, { createRef, useEffect, useRef, useState } from 'react';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import TimelineZoom from './TimelineZoom';
import TimelineToggle from './TimelineToggle';
import TimelineProvider, { TimelineContext } from './hooks/TimelineContext';
import { Timeline } from './Timeline';
import { KEYS, TIMEWARP_AMOUNT } from './constants';
import { ITimelineContext } from './types';
import { ISubtitleTrack } from '../types';

interface ITimelineProps {}

const TimelineContainer: React.FC<ITimelineProps> = () => {
  const { duration, playedSeconds, setPlayedSeconds } = useVideoEditor();
  const wrapper = createRef<HTMLDivElement>();
  const timeline = createRef<HTMLDivElement>();
  const zoomRef = useRef<React.ElementRef<typeof TimelineZoom>>(null);
  const leftScrollSpace = useRef<number>(0);

  const [zoom, setZoom] = useState<number>(4);
  const [visible, setVisible] = useState<boolean>(true);
  const [position, setPosition] = useState<number>(15);
  const [timelineWidth, setTimelineWidth] = useState<number>(0);
  const [timelineStart, setTimelineStart] = useState<number>(0);
  const [timelineEnd, setTimelineEnd] = useState<number>(0);
  const [scrolling, setScrolling] = useState<boolean>(false);
  const cursorIsVisible = useRef<boolean>(true);

  const contextValues: ITimelineContext = {
    zoom,
    setZoom,
    visible,
    setVisible,
    position,
    setPosition,
    scrolling,
    setScrolling,
    timelineWidth,
    timelineStart,
    timelineEnd,
    leftScrollSpace,
    wrapper,
    cursorIsVisible,
    timelineRef: timeline,
    zoomStartTime: { minutes: -1, seconds: -1 },
    zoomEndTime: { minutes: -1, seconds: -1 },
  };

  //Initial render only
  useEffect(() => {
    window.addEventListener('keydown', onKeyDown);
    updateTimelineBounds();
    return () => window.removeEventListener('keydown', onKeyDown);
  }, []);

  const onKeyDown = (e: KeyboardEvent) => {
    if (!zoomRef) return;

    const timelineIsFocused =
      document.getElementById('timeline') === document.activeElement;

    if (timelineIsFocused && e.metaKey && e.key === KEYS.INCREMENT_ZOOM) {
      e.preventDefault();
      zoomRef.current.incrementZoom();
    } else if (
      timelineIsFocused &&
      e.metaKey &&
      e.key === KEYS.DECREMENT_ZOOM
    ) {
      e.preventDefault();
      zoomRef.current.decrementZoom();
    } else if (e.key === KEYS.FORWARD) {
      e.preventDefault();
      setPlayedSeconds((s) => {
        if (s + TIMEWARP_AMOUNT >= duration) return duration;
        return s + TIMEWARP_AMOUNT;
      });
    } else if (e.key === KEYS.BACKWARD) {
      e.preventDefault();
      setPlayedSeconds((s) => {
        if (s - TIMEWARP_AMOUNT <= 0) return 0;
        return s - TIMEWARP_AMOUNT;
      });
    }
  };

  const updateTimelineWidth = () => {
    const wrapperWidth = Number(wrapper.current?.clientWidth);

    setTimelineWidth(wrapperWidth * zoom);
  };

  const updateTimelineBounds = () => {
    if (wrapper.current) wrapper.current.scrollLeft = position * zoom;
    const wrapperWidth = Number(wrapper.current?.clientWidth);
    const wrapperStart = Number(wrapper.current?.scrollLeft);
    const wrapperEnd = wrapperStart + wrapperWidth;
    setTimelineStart(wrapperStart);
    setTimelineEnd(wrapperEnd);
    updateCursorPosition();
  };

  const updateCursorPosition = () => {
    const leftScrollSpace: number = wrapper.current?.scrollLeft || 0;
    const newPosition =
      (playedSeconds * timelineWidth) / duration - leftScrollSpace;
    cursorIsVisible.current =
      newPosition <= Number(wrapper.current?.clientWidth) &&
      position >= timelineStart;

    setPosition(newPosition);
  };

  useEffect(() => {
    if (!wrapper || !timeline) {
      return;
    }

    window.removeEventListener('resize', updateTimelineWidth);
    window.addEventListener('resize', updateTimelineWidth);

    return () => {
      window.removeEventListener('resize', updateTimelineWidth);
    };
  }, [wrapper, timeline, zoom]);

  useEffect(() => updateTimelineWidth(), [zoom]);
  useEffect(() => updateCursorPosition(), [playedSeconds, timelineWidth]);

  useEffect(() => {
    if (wrapper.current) {
      const wrapperWidth = Number(wrapper.current?.clientWidth);
      const value =
        (playedSeconds * (wrapperWidth * zoom)) / duration - wrapperWidth / 2;
      wrapper.current.scrollLeft = value;
    }
  }, [timelineWidth]);

  useEffect(() => {
    updateCursorPosition();
    leftScrollSpace.current = Number(wrapper.current?.scrollLeft);
    setScrolling(false);
  }, [scrolling]);

  return (
    <TimelineProvider value={contextValues}>
      <div className="w-full mt-16">
        <div className="w-full flex items-start justify-between">
          <TimelineToggle />
          <TimelineZoom ref={zoomRef} />
        </div>
      </div>
      {visible && <Timeline />}
    </TimelineProvider>
  );
};

export default TimelineContainer;
