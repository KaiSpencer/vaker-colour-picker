import React from 'react';
import { ISubtitleTrack } from '../types';

export interface ITimelineContext {
  zoom: number;
  setZoom: React.Dispatch<React.SetStateAction<number>>;
  visible: boolean;
  setVisible: React.Dispatch<React.SetStateAction<boolean>>;
  position: number;
  setPosition: React.Dispatch<React.SetStateAction<number>>;
  scrolling: boolean;
  setScrolling: React.Dispatch<React.SetStateAction<boolean>>;
  timelineWidth: number;
  timelineStart: number;
  timelineEnd: number;
  leftScrollSpace: {
    current: number;
  };
  wrapper: {
    current: any;
  };
  cursorIsVisible: {
    current: boolean;
  };
  timelineRef: any;
  zoomStartTime: {
    minutes: number;
    seconds: number;
  };
  zoomEndTime: {
    minutes: number;
    seconds: number;
  };
}
