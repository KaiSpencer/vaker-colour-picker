import { Listbox, Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';
import React, { Fragment, FunctionComponent } from 'react';
import { videoFormatList } from '../constants';

interface props {
  handleVideoFormatChange: Function;
  croppingIndex: string;
}

const CroppingList: React.FC<props> = props => {
  const { handleVideoFormatChange, croppingIndex } = props;
  return (
    <div className="relative mb-12 text-center">
      <Listbox
        value={croppingIndex}
        onChange={() => {}}
        as="div"
        className="relative z-20 max-w-sm mx-auto"
      >
        {({ open }) => (
          <>
            <Listbox.Button className="hover:bg-gray-100 bg-white px-4 py-2 rounded-md text-font-1 uppercase font-extrabold w-48 tracking tracking-wider shadow-editor focus:outline-none mt-2">
              <div className="flex justify-between items-center h-6">
                <div className="w-6">
                  <svg height="28px" viewBox="0 0 112 68.25">
                    <g>
                      <rect
                        rx="10"
                        id="svg_1"
                        vectorEffect="non-scaling-stroke"
                        x="6"
                        y="6"
                        strokeWidth="3"
                        fillOpacity="0%"
                        stroke="#2B2B2B"
                        width="100"
                        height="56.25"
                      ></rect>
                    </g>
                  </svg>
                </div>
                <div className="pl-4">cropping</div>

                <ChevronDownIcon className="inline w-5 h-5" />
              </div>
            </Listbox.Button>
            <Transition
              show={open}
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options
                as="div"
                className="absolute z-10 mt-1 w-full bg-white shadow-editor p-4 rounded-lg  overflow-auto focus:outline-none"
              >
                {videoFormatList.map(videoFormat => (
                  <Listbox.Option
                    key={videoFormat.value}
                    value={videoFormat.value}
                    as="div"
                    className={`flex justify-between w-full h-12 font-extrabold px-4 py-2 cursor-pointer rounded-md ${
                      croppingIndex ?? 0 === videoFormat.value
                        ? 'bg-blue-100'
                        : 'hover:bg-gray-100 focus:bg-gray-100'
                    }`}
                  >
                    <div className="flex items-center w-full">
                      <div className="self-start w-12 flex items-center relative bottom-0.5">
                        {videoFormat.icon}
                      </div>
                      <div className="flex pl-4 items-center justify-between w-full flex-1">
                        <div>{videoFormat.label}</div>
                        <div className="text-gray-400">
                          {videoFormat.aspectRatio}
                        </div>
                      </div>
                    </div>
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </>
        )}
      </Listbox>
    </div>
  );
};
