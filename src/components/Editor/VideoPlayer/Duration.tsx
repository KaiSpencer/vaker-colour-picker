type Props = {
  seconds: number;
  className?: string;
};

const format = (seconds: number) => {
  const zeroPad = (num: Number) => String(num).padStart(2, '0');

  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = date.getUTCSeconds();
  const string = `${hh ? `${zeroPad(hh)}:` : ''}${zeroPad(mm)}:${zeroPad(ss)}`;
  console.log(string, hh, mm, ss);
  return string;
};

const Duration = ({ seconds, className }: Props) => (
  <time dateTime={`P${Math.round(seconds)}S`} className={className}>
    {format(seconds)}
  </time>
);

export default Duration;
