import React, { useState, useCallback } from 'react';
import { IEditorTextStyles } from '~/redux/editor/types';
import { useDispatch, useSelector } from 'react-redux';
import {
  setVideoHeadPosition,
  setVideoHeadSize,
} from '~/redux/editor/editor.actions';
import MoveableComponent from '~/components/ui/Moveable';
import getCssPropertiesFromStyleConfiguration from '~/components/Editor/helper/getCssPropertiesFromStyleConfiguration';
import { RootState } from '~/redux/rootStore';
interface IHeadlinePreviewProps {
  headline: string;
  style: IEditorTextStyles;
}

const HeadlinePreview: React.FC<IHeadlinePreviewProps> = ({
  headline,
  style,
}) => {
  const dispatch = useDispatch();

  const [target, setTarget] = React.useState<HTMLElement>();
  const [frame, setFrame] = React.useState({
    translate: [0, 0],
    rotate: 0,
  });
  const [zoom, setZoom] = React.useState(0);
  const [currentHeadPosition, setHeadPosition] = useState<{
    x: number;
    y: number;
  }>();
  const [currentHeadSize, setHeadSize] = useState<{
    width: number;
    height: number;
  }>();
  const size = useSelector(({ editor }: RootState) => editor?.container?.size);
  const height = size?.height ? size.height : 0;
  const left = target && size ? target?.offsetWidth / 2 - size.width / 2 : 0;
  const width = size?.width ? size.width + left : 0;

  const onDragged = useCallback((position) => {
    setHeadPosition(position);

    dispatch(setVideoHeadPosition(position));
  }, []);

  const onResize = useCallback((size) => {
    setHeadSize(size);

    dispatch(setVideoHeadSize(size));
  }, []);

  React.useEffect(() => {
    setTarget(document.querySelector<HTMLElement>('.target')!);
  }, []);

  function changeZoom() {
    if (zoom == 0) {
      setZoom(1);
    } else {
      setZoom(0);
    }
  }

  return (
    <div className="relative" onClick={() => changeZoom()}>
      <div
        className={`p-2 opacity-90 target`}
        style={getCssPropertiesFromStyleConfiguration(style)}
      >
        {headline}
      </div>
      <MoveableComponent
        target={target}
        frame={frame}
        zoom={zoom}
        onDragged={onDragged}
        onResize={onResize}
        snappable={size?.height ? true : false}
        bounds={{ left: left, top: 0, right: width, bottom: height }}
      />
    </div>
  );
};

export default HeadlinePreview;
