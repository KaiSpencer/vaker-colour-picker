import React, { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setVideoSubtitlePosition,
  setVideoSubtitleSize,
} from '~/redux/editor/editor.actions';
import MoveableComponent from '~/components/ui/Moveable';
import { RootState } from '~/redux/rootStore';
interface IVideoLogoProps {
  subtitleLineText:
    | string
    | (
        | string
        | React.ReactElement<any, string | React.JSXElementConstructor<any>>
      )[];
  wrapperStyle: object;
  style?: object;
}

const Subtitle: React.FC<IVideoLogoProps> = ({
  subtitleLineText,
  wrapperStyle,
  style,
}) => {
  const dispatch = useDispatch();

  const [target, setTarget] = React.useState<HTMLElement>();
  const [frame, setFrame] = React.useState({
    translate: [0, 0],
    rotate: 0,
  });
  const [zoom, setZoom] = React.useState(0);
  const [currentSubtitlePosition, setSubtitlePosition] = useState<{
    x: number;
    y: number;
  }>();
  const [currentLogoSize, setSubtitleSize] = useState();
  const size = useSelector(({ editor }: RootState) => editor?.container?.size);
  const botton = target ? target?.offsetHeight + 20 : 0;
  const height = size?.height ? size.height - botton : 0;
  const left = target && size ? target?.offsetWidth / 2 - size.width / 2 : 0;
  const width = size?.width ? size.width + left : 0;
  const onDragged = useCallback(position => {
    setSubtitlePosition(position);

    dispatch(setVideoSubtitlePosition(position));
  }, []);

  const onResize = useCallback(size => {
    setSubtitleSize(size);

    dispatch(setVideoSubtitleSize(size));
  }, []);

  React.useEffect(() => {
    setTarget(document.querySelector<HTMLElement>('.targetSubtitle')!);
  }, []);

  function changeZoom() {
    if (zoom == 0) {
      setZoom(1);
    } else {
      setZoom(0);
    }
  }
  return (
    <div className="relative" onClick={() => changeZoom()}>
      <div style={wrapperStyle} className="p-2 targetSubtitle">
        <span className={`opacity-90`} style={style}>
          {subtitleLineText}
        </span>
      </div>
      <MoveableComponent
        target={target}
        frame={frame}
        zoom={zoom}
        onDragged={onDragged}
        onResize={onResize}
        snappable={size?.height ? true : false}
        bounds={{ left: left, top: -height, right: width, bottom: botton }}
      />
    </div>
  );
};

export default Subtitle;
