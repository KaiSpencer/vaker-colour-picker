import React, { FC, useMemo, ReactElement } from 'react';
import { useSelector } from 'react-redux';
import { NodeCue, NodeList } from 'subtitle';
import { RootState } from '~/redux/rootStore';
import { selectSubtitlesWithVTTNodeList } from '~/redux/editor/editor.selectors';
import getCssPropertiesFromStyleConfiguration from '~/components/Editor/helper/getCssPropertiesFromStyleConfiguration';

interface ISubtitlePreviewProps {
  start: number;
}

const SubtitlePreview: FC<ISubtitlePreviewProps> = ({ start }) => {
  const subtitlesWithVTTNodeList = useSelector(selectSubtitlesWithVTTNodeList);
  const { subtitleStyle, karaokeStyle } = useSelector(
    ({ editor }: RootState) => {
      const { subtitle: subtitleStyle, karaoke: karaokeStyle } = editor?.styles;

      return { subtitleStyle, karaokeStyle };
    },
  );

  const getCurrentSubtitleLine = (
    subtitles: NodeList,
    startTime: number,
  ): NodeCue | null => {
    for (const node of subtitles) {
      if (node.type !== 'cue') continue;
      if (startTime < node.data.start || startTime > node.data.end) continue;
      return node;
    }
    return null;
  };

  const getKaraokeStyledText = (
    subtitleNode: NodeCue,
    startTime: number,
    fontColor: string,
    effectType: boolean,
  ): Array<string | ReactElement> => {
    const words: Array<string | ReactElement> =
      subtitleNode.data.text.split(' ');
    const amountWords = words.length;

    // How long is the current line already shown
    const currentSubtitleLinePlayTime = Math.max(
      startTime - subtitleNode.data.start,
      0,
    );

    // Calculate how long each word will be highlighted
    const subtitleDuration = subtitleNode.data.end - subtitleNode.data.start;
    const highlightDuration = subtitleDuration / amountWords;

    // Calculate the current highlighted index based on the playtime
    const highlightedIndex = Math.max(
      Math.floor(currentSubtitleLinePlayTime / highlightDuration) - 1,
      0,
    );

    if (effectType) {
      //if effect type is "every word", change all the words before the highlithed index, else just the highlighted index
      for (let i = 0; i <= highlightedIndex; i++) {
        words[i] = <span style={{ color: fontColor }}>{words[i]}</span>;
      }
    } else {
      words[highlightedIndex] = (
        <span style={{ color: fontColor }}>{words[highlightedIndex]}</span>
      );
    }

    // Add back whitespace
    return words.map((word) => <>{word} </>);
  };

  const { style, wrapperStyle } = useMemo(() => {
    const style = getCssPropertiesFromStyleConfiguration(subtitleStyle!);

    const wrapperStyle: any = {
      textAlign: style.textAlign,
      borderRadius: style.borderRadius,
    };

    delete style.textAlign;

    // If boxShadow is not none it means that "Split box" is off
    // When split box is off the bg must be in the wrapper
    if (style.boxShadow === 'none') {
      wrapperStyle.backgroundColor = style.backgroundColor;
      delete style.backgroundColor;
    }

    if (subtitleStyle?.fontType == 'splitBox') {
      delete style.borderRadius;
      style.backgroundColor = subtitleStyle.backgroundColor;
      style.boxShadow = '0 0 0 10px ' + subtitleStyle.backgroundColor;
      style.color = subtitleStyle.fontColor;
    }

    if (subtitleStyle?.fontType == 'outline') {
      style.textShadow =
        '0 0 3px black, 0 0 3px black, 0 0 3px black, 0 0 3px black';
    }

    if (subtitleStyle?.fontType == 'shadow') {
      style.textShadow = '2px 2px 3px rgba(0, 0, 0, 1)';
    }

    return { style, wrapperStyle };
  }, [subtitleStyle]);

  // Playing time is in seconds, cue time in milliseconds
  const startTimeMilliseconds = useMemo(() => start * 1000, [start]);

  const subtitleNode = useMemo<NodeCue | null>(
    () =>
      !!subtitlesWithVTTNodeList
        ? getCurrentSubtitleLine(
            subtitlesWithVTTNodeList as NodeList,
            startTimeMilliseconds,
          )
        : null,
    [subtitlesWithVTTNodeList, startTimeMilliseconds],
  );

  const subtitleLineText = useMemo(
    () =>
      !!subtitleNode && karaokeStyle?.active
        ? getKaraokeStyledText(
            subtitleNode,
            startTimeMilliseconds,
            karaokeStyle.fontColor,
            karaokeStyle.effectType,
          )
        : subtitleNode?.data.text,
    [karaokeStyle, subtitleNode, startTimeMilliseconds],
  );

  if (subtitleNode === null) {
    return null;
  }

  return (
    <div style={wrapperStyle}>
      <span className={`opacity-90`} style={style}>
        {subtitleLineText}
      </span>
    </div>
  );
};

export default SubtitlePreview;
