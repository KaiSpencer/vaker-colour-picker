import React, { useState, useCallback, useEffect } from 'react';
import ReactPlayer from 'react-player';
import { useDispatch } from 'react-redux';
import {
  setVideoPosition,
  setContainerSize,
} from '~/redux/editor/editor.actions';
interface IVideoPreviewProps {
  ref: any;
  ref2: any; //Ref to the black screen drawn *behind* the video so the player never looks empty
  playing?: boolean;
  muted?: boolean;
  playbackRate?: number;
  onProgress?: (state: {
    played: number;
    playedSeconds: number;
    loaded: number;
    loadedSeconds: number;
  }) => void;
  onDuration?: (duration: number) => void;
  onEnded?: () => void;
  url?: string;
  togglePlay: () => void;
}

const VideoPreview: React.FC<IVideoPreviewProps> = React.forwardRef(
  (props, ref: any) => {
    const dispatch = useDispatch();

    const [currentVideoPosition, setVideoPositionState] = useState(undefined);
    const [target, setTarget] = useState<Element>();

    const onDragged = useCallback((position) => {
      setVideoPositionState(position);
      console.debug(position);
      dispatch(setVideoPosition(position));
    }, []);

    useEffect(() => {
      setTarget(document.querySelector('.targetVideo')!);

      if (!props.ref2.current) {
        return;
      }

      const size = {
        width: props.ref2.current.offsetWidth,
        height: props.ref2.current.offsetHeight,
      };

      dispatch(setContainerSize(size));
    }, [props.ref2?.current?.offsetHeight]);

    return (
      <div style={{ height: '100%' }} className="relative" id="goodbye">
        <div className="overflow-hidden" id="hello">
          <div className="targetVideo" onClick={props.togglePlay}>
            <ReactPlayer
              ref={ref}
              width="100%"
              height="100%"
              style={{ borderRadius: '5px', overflow: 'hidden' }}
              playing={props.playing}
              muted={props.muted}
              playbackRate={props.playbackRate}
              progressInterval={10}
              onProgress={props.onProgress}
              onDuration={props.onDuration}
              onEnded={props.onEnded}
              url={props.url}
            />
          </div>
        </div>
      </div>
    );
  },
);

export default VideoPreview;
