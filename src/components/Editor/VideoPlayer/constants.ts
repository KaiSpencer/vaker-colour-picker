import { ReactNode } from 'react';
import { NodeList } from 'subtitle';
import {
  IEditorHeadlineStyles,
  IEditorKaraokeStyles,
  IEditorLogo,
  IEditorProgressBarStyles,
  IEditorTextStyles,
  IVideoTranslation,
} from '~/redux/editor/types';
import { Dispatch } from 'redux';

// This enables a smooth seeking
export const amountOfSliderSteps = 10000;

interface ICroppingFormat {
  value: number;
  label: string;
  aspectRatio: string;
  iconFileName: string;
  widthRatio: number;
  heightRatio: number;
  icon: ReactNode;
}

export const videoFormatList = new Array<ICroppingFormat>(
  {
    value: 1,
    label: 'Original',
    aspectRatio: '',
    iconFileName: 'original.svg',
    widthRatio: 16, //temp
    heightRatio: 9, //temp
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 112 68.25">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="100"
            height="56.25"
          ></rect>
        </g>
      </svg>
    )`,
  },
  {
    value: 2,
    label: 'Instagram',
    aspectRatio: '1:1',
    iconFileName: 'square.svg',
    widthRatio: 1,
    heightRatio: 1,
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 112 112">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="100"
            height="100"
          ></rect>
        </g>
      </svg>
    )`,
  },

  {
    value: 3,
    label: 'Youtube, Vimeo',
    aspectRatio: '16:9',
    iconFileName: 'landscape.svg',
    widthRatio: 16,
    heightRatio: 9,
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 112 68.25">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="100"
            height="56.25"
          ></rect>
        </g>
      </svg>
    )`,
  },
  {
    value: 4,
    label: 'TikTok, Instagram',
    aspectRatio: '9:16',
    iconFileName: 'portrait.svg',
    widthRatio: 9,
    heightRatio: 16,
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 68.25 112">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="56.25"
            height="100"
          ></rect>
        </g>
      </svg>
    )`,
  },
  {
    value: 5,
    label: 'Facebook, Twitter',
    aspectRatio: '5:4',
    iconFileName: 'horizontal.svg',
    widthRatio: 5,
    heightRatio: 4,
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 112 92">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="100"
            height="80"
          ></rect>
        </g>
      </svg>
    )`,
  },
  {
    value: 6,
    label: 'Facebook, Twitter',
    aspectRatio: '4:5',
    iconFileName: 'vertical.svg',
    widthRatio: 4,
    heightRatio: 5,
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 92 112">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="80"
            height="100"
          ></rect>
        </g>
      </svg>
    )`,
  },
  {
    value: 7,
    label: 'Pinterest',
    aspectRatio: '2:3',
    iconFileName: 'pinterest.svg',
    widthRatio: 2,
    heightRatio: 3,
    icon: `(
      <svg width="100%" height="36px" viewBox="0 0 78.66666666666666 112">
        <g>
          <rect
            rx="10"
            id="svg_1"
            vector-effect="non-scaling-stroke"
            x="6"
            y="6"
            stroke-width="3"
            fill-opacity="0%"
            stroke="#2B2B2B"
            width="66.66666666666666"
            height="100"
          ></rect>
        </g>
      </svg>
    )`,
  },
);
