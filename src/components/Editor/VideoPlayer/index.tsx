import { FC, useEffect, useState, useRef } from 'react';
import { Slider } from 'antd';
import ReactPlayer from 'react-player';
import { useSelector } from 'react-redux';
import VideoPreview from './VidePreview';
import { RootState } from '~/redux/rootStore';
import Duration from '~/components/Editor/VideoPlayer/Duration';
import VideoPlayerControls from '~/components/Editor/VideoPlayer/VideoPlayerControls';
import SubtitlePreview from '~/components/Editor/VideoPlayer/SubtitlePreview';
import VideoProgressBar from '~/components/Editor/VideoPlayer/VideoProgressBar';
import VideoLogo from '~/components/Editor/VideoPlayer/VideoLogo';
import HeadlinePreview from '~/components/Editor/VideoPlayer/HeadlinePreview';
import { useVideoEditor } from '~/hooks/useVideoEditor';
import { amountOfSliderSteps } from './constants';

export interface IVideoPlayerProps {
  videoUrl: string;
}

const VideoPlayer: FC<IVideoPlayerProps> = ({ videoUrl }) => {
  const [seeking, setSeeking] = useState<boolean>(false);
  const [startedPlaying, setStartedPlaying] = useState<boolean>(false);
  const [playedPercentage, setPlayedPercentage] = useState<number>(0);
  const [playedSeconds, setPlayedSeconds] = useState<number>(0);
  const [muted, setMuted] = useState<boolean>(false);
  const [playbackRate, setPlaybackReate] = useState<number>(1);
  const [duration, setDuration] = useState<number>(0);

  const { playing = false, setPlaying, ...context } = useVideoEditor();

  const logo = useSelector(({ editor }: RootState) => editor?.logo);
  const { progressBarConfiguration, subtitleStyle, headlineStyle } =
    useSelector(({ editor }: RootState) => {
      const {
        progress: progressBarConfiguration,
        subtitle: subtitleStyle,
        headline: headlineStyle,
      } = editor?.styles;

      return { progressBarConfiguration, subtitleStyle, headlineStyle };
    });

  const playerRef = useRef<ReactPlayer>();
  const stageCanvasRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (context.playedSeconds !== playedSeconds) {
      context.setPlayedSeconds(playedSeconds);
    }
  }, [playedSeconds]);

  useEffect(() => {
    if (playedSeconds !== context.playedSeconds) {
      setPlayedSeconds(context.playedSeconds);

      playerRef.current?.seekTo(context.playedSeconds, 'seconds');
    }
  }, [context.playedSeconds]);

  const renderSliderFormatter = (value: any) => {
    return <Duration seconds={duration * (value / amountOfSliderSteps)} />;
  };

  const handleSeekMouseDown = (sliderStep: number) => {
    setSeeking(true);
    setPlayedPercentage(sliderStep / amountOfSliderSteps);
  };

  const handleSeekMouseUp = (sliderStep: number) => {
    setSeeking(false);
    playerRef.current?.seekTo(sliderStep / amountOfSliderSteps);
  };

  const handleVideoProgress = (state: {
    played: number;
    playedSeconds: number;
    loaded: number;
    loadedSeconds: number;
  }) => {
    if (!seeking) {
      setPlayedPercentage(state.played);
      setPlayedSeconds(state.playedSeconds);
    }
  };

  const handlePlaybackRateChange = (playbackRate: number | null) => {
    if (playbackRate !== null) {
      setPlaybackReate(playbackRate);
    }
  };

  const handleVideoDuration = (duration: any) => {
    context.setDuration(duration);
    setDuration(duration);
  };

  const handleVideoEnded = () => {
    setPlaying(false);
  };

  const handlePlayToggle = () => {
    setPlaying((playing) => !playing);
    setStartedPlaying(true);
  };

  const handleVolumeClick = () => {
    setMuted(!muted);
  };

  const goBackSeconds = (seconds = 5) => {
    changePlayerTime(seconds * -1);
  };

  const goForwardSeconds = (seconds = 5) => {
    changePlayerTime(seconds);
  };

  const changePlayerTime = (seconds: any) => {
    const played = duration * playedPercentage;
    playerRef.current?.seekTo(played + seconds, 'seconds');
  };

  const hideDefaultTextTracks = () => {
    const video = playerRef && playerRef.current?.getInternalPlayer();
    if (!video) {
      return;
    }

    Array.from(video.textTracks).forEach(
      (track: any) => (track.mode = 'hidden'),
    );
  };

  hideDefaultTextTracks();

  return (
    <div className="">
      <div className="relative">
        <div
          className="relative"
          ref={stageCanvasRef}
          style={{
            borderRadius: '5px',
            minHeight: '10vh',
            background: 'black',
            maxWidth: '100%',
            maxHeight: '100%',
          }}
        >
          <VideoPreview
            ref={playerRef}
            ref2={stageCanvasRef}
            playing={playing}
            muted={muted}
            playbackRate={playbackRate}
            onProgress={handleVideoProgress}
            onDuration={handleVideoDuration}
            onEnded={handleVideoEnded}
            url={videoUrl}
            togglePlay={handlePlayToggle}
          />

          {subtitleStyle?.active && (
            <div className="absolute w-10/12 transform -translate-x-1/2 bottom-[20px] left-1/2">
              <SubtitlePreview start={playedSeconds} />
            </div>
          )}
          {startedPlaying && headlineStyle?.active && (
            <div className="absolute w-10/12 transform -translate-x-1/2 top-0 left-1/2 ">
              <HeadlinePreview
                headline={headlineStyle.headline}
                style={headlineStyle}
              />
            </div>
          )}
        </div>
        {progressBarConfiguration.active && (
          <div
            className="absolute bottom-0 w-full"
            style={{
              backgroundColor: progressBarConfiguration.backgroundColor,
            }}
          >
            <VideoProgressBar
              percent={playedPercentage * 100}
              backgroundColor={progressBarConfiguration.backgroundColor}
              progressColor={progressBarConfiguration.progressColor}
              borderRadius={progressBarConfiguration.borderRounded}
            />
          </div>
        )}
        {logo.file !== null && logo.blobData !== null && (
          <VideoLogo
            file={logo.file}
            blobData={logo.blobData}
            size={logo.size}
            position={logo.position}
          />
        )}
      </div>

      <div className="flex mt-3">
        <Slider
          className="flex-grow"
          min={0}
          max={amountOfSliderSteps}
          value={playedPercentage * amountOfSliderSteps}
          tipFormatter={renderSliderFormatter}
          onChange={handleSeekMouseDown}
          onAfterChange={handleSeekMouseUp}
          trackStyle={{ backgroundColor: '#43597D', height: '5px' }}
          style={{ height: '5px' }}
          handleStyle={{
            transition: 'width',
          }}
        />
      </div>
      <div className="flex flex-row justify-center items-center space-x-5">
        <VideoPlayerControls
          isPlaying={playing}
          isMuted={muted}
          onPlayToggle={handlePlayToggle}
          onVolumeToggle={handleVolumeClick}
          onSeekBackward={() => goBackSeconds()}
          onSeekForward={() => goForwardSeconds()}
          onPlaybackRateChange={handlePlaybackRateChange}
        />
      </div>
    </div>
  );
};

export default VideoPlayer;
