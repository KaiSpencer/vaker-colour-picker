import { IEditorTextStyles } from '~/redux/editor/types';
import { CSSProperties } from 'react';

const getCssPropertiesFromStyleConfiguration = (
  style: IEditorTextStyles,
): CSSProperties => ({
  fontFamily: style.font,
  fontSize: `${style.fontSize}px`,
  // fontWeight: style.fontStyle?.bold ? 'bold': undefined,
  fontWeight: style.bold ? 'bold' : undefined,
  textDecoration: style.underline ? 'underline' : undefined,
  fontStyle: style.italic ? 'italic' : undefined,
  color: style.fontType === 'splitBox' ? '#fff' : style.fontColor,
  backgroundColor:
    style.fontType === 'splitBox'
      ? '#000'
      : style.backgroundActive
      ? style.backgroundColor
      : 'transparent',
  borderRadius: `${style.borderRounded}px`,
  lineHeight:
    style.fontType === 'splitBox'
      ? `${style.fontSize <= 18 ? '3' : style.fontSize <= 26 ? '2.2' : '2'}em`
      : 'normal',
  boxShadow: style.fontType === 'splitBox' ? '0 0 0 10px #000' : 'none',
  textTransform: style.textTransform,
  textAlign: style.textAlign,
  textShadow: `${
    style.fontType === 'shadow'
      ? '2px 2px 5px black'
      : style.fontType === 'outline'
      ? '0 0 1px black, 0 0 1px black, 0 0 1px black, 0 0 1px black'
      : 'none'
  }`,
});

export default getCssPropertiesFromStyleConfiguration;
