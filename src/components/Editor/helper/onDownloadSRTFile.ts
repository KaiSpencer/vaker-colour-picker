import { downloadFile, writeToBlob } from '~/utils/fileGenerationUtils';

const onDownloadSRTFile = (subCurrentArray, withTimestamps: boolean) => {
  const filename = 'download-srt-file';

  const blobParts: BlobPart[] = writeToBlob(
    'srt',
    withTimestamps,
    subCurrentArray,
  );

  downloadFile(blobParts, filename, 'srt');
};

export default onDownloadSRTFile;
