import { downloadFile, writeToBlob } from '~/utils/fileGenerationUtils';

const onDownloadTXTFile = (subCurrentArray, withTimestamps: boolean) => {
  const filename = 'download-text-file';

  const blobParts: BlobPart[] = writeToBlob(
    'txt',
    withTimestamps,
    subCurrentArray,
  );

  downloadFile(blobParts, filename, 'txt');
};

export default onDownloadTXTFile;
