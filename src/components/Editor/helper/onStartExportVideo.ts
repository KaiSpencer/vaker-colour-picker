import axios from 'axios';
import PATHS from '~/constants/paths';
import { writeToBlob } from '~/utils/fileGenerationUtils';

interface IonStartExportVideoArgs {
  user;
  project;
  subCurrentArray;
  router;
  quality: string;
  notifyWhenFinishes: boolean;
  videoFile?: File;
}

const onStartExportVideo = async ({
  user,
  project,
  subCurrentArray,
  router,
  quality,
  notifyWhenFinishes,
  videoFile,
}: IonStartExportVideoArgs) => {
  //undefined had to be permissible to pacify the gods of TypeScript
  if (!videoFile) return;
  const token = user.jwtToken;

  //create an axiosinstance
  const axiosInstance = axios.create({
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
      'Content-Type': 'multipart/form-data',
    },
  });
  const SRT_URL_PREFIX = `${process.env.API_URL}/render/upload/srt`;
  const UUID = project.selectedProject;

  // Generates and uploads subtitle file (vtt but route implies it's SRT)
  const blob: Blob[] = writeToBlob('vtt', true, subCurrentArray);
  const vttFile = new File(blob, 'subtitle.vtt');
  const vttFormData = new FormData();
  vttFormData.append('file', vttFile, `${UUID}.vtt`);
  const srtRes = await axiosInstance.post(
    `${SRT_URL_PREFIX}/${UUID}`,
    vttFormData,
  );
  console.log('srtRes', srtRes);

  //Make the video start rendering
  const renderVideoRes = await axiosInstance.post(
    `${process.env.API_URL}/render/video/${UUID}`,
    srtRes.data,
  );
  console.log('renderVideoRes', renderVideoRes);

  //Begin download of video
  router.push(`${PATHS.DASHBOARD.EDITOR}/RenderingPage`);
};

export default onStartExportVideo;
