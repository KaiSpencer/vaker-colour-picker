import React, { useMemo, useState, useRef, useEffect } from 'react';
import { NodeList } from 'subtitle';
import collect from 'collect.js';
import SubtitleEditor from '~/components/Editor/Tabs/SubtitleEditor';
import * as utils from '~/components/Editor/Tabs/SubtitleEditor/utils';
import VideoPlayer from '~/components/Editor/VideoPlayer';
import StyleEditor from '~/components/Editor/Tabs/StyleEditor';
import AdditionalEditor from '~/components/Editor/Tabs/AdditionalEditor';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '~/redux/rootStore';
import { useRouter } from 'next/router';
import EditorHeader from '~/components/Editor/EditorHeader';
import Head from 'next/head';
import ExportSRTorTXTModal from '~/components/Editor/ExportSRTorTXTModal';
import ExportVideoModal from '~/components/Editor/ExportVideoModal';
import TimelineContainer from '~/components/Editor/TimelineContainer';
import VideoEditorProvider from '~/hooks/useVideoEditor';
import {
  setCurrentTranslation,
  setSubtitles,
} from '~/redux/editor/editor.actions';
import { selectSubtitlesWithVTTNodeList } from '~/redux/editor/editor.selectors';
import { ITranslationWord, ITranslationMatch } from '~/redux/editor/types';
import Tabs from '~/components/Editor/Tabs';
import { ISubtitleTrack } from '~/components/Editor/types';
import onDownloadSRTFile from '~/components/Editor/helper/onDownloadSRTFile';
import onDownloadTXTFile from '~/components/Editor/helper/onDownloadTXTFile';
import onStartExportVideo from '~/components/Editor/helper/onStartExportVideo';
import { convertITransToISubtitle } from 'src/utils/subtitle-tab/listUtils';

// TODO: Does ReactPlayer & Wavesurfer also work with uploads?

interface IEditorProps {
  video?: {
    videoURL: string;
    name: string;
    words_grammatical: ITranslationWord[];
    wavChannelData?: Array<number>;
  };
  url?: string;
  hideExport?: boolean;
}

const Editor: React.FC<IEditorProps> = ({ video, hideExport, url }) => {
  const title = 'ProjectOne';
  const router = useRouter();
  const dispatch = useDispatch();

  const videoUrl = useMemo(
    () => video?.videoURL || url || '/assets/test.mp4',
    [video?.videoURL, url],
  );

  const [tabIndex, setTabIndex] = useState(0);
  const [isSRTModalOpen, setIsSRTModalOpen] = useState(false);
  const [isTXTModalOpen, setIsTXTModalOpen] = useState(false);
  const [isExportModalOpen, setIsExportModalOpen] = useState(false);

  const user = useSelector(({ user }: RootState) => user);
  const project = useSelector(({ project }: RootState) => project);

  const subtitles = useSelector(
    ({ editor }: RootState) => editor?.subtitles || [],
  );
  const subtitlesWithVTTNodeList = useSelector(selectSubtitlesWithVTTNodeList);
  const wordsGrammatical = useSelector(
    ({ editor }: RootState) =>
      editor?.currentVideoTranslation?.WordsGrammatical,
  );

  const currentWordsGrammatical = useMemo(
    () => video?.words_grammatical || wordsGrammatical,
    [video?.words_grammatical, wordsGrammatical],
  );

  useEffect(() => {
    if (
      JSON.stringify(wordsGrammatical) !==
      JSON.stringify(currentWordsGrammatical)
    ) {
      dispatch(
        setCurrentTranslation({ WordsGrammatical: currentWordsGrammatical }),
      );
    }
  }, [currentWordsGrammatical, wordsGrammatical]);

  useEffect(() => {
    if (!!subtitles?.length || !currentWordsGrammatical?.length) {
      return;
    }

    const firstPassSubtitles = utils.handleSubtitleChange(
      subtitles,
      currentWordsGrammatical,
    );

    const secondPassSubtitles = utils.handleSubtitleChange(
      firstPassSubtitles,
      currentWordsGrammatical,
    );

    if (JSON.stringify(subtitles) !== JSON.stringify(secondPassSubtitles)) {
      dispatch(setSubtitles(secondPassSubtitles));
    }
  }, [subtitles, currentWordsGrammatical, setSubtitles, dispatch]);

  const TABS_CONFIG = [
    {
      key: 'SUBTITLE',
      title: 'subtitles',
      icon: '/assets/editor/subtitle-panel.svg',
      view: <SubtitleEditor />,
    },
    {
      key: 'STYLE',
      title: 'style',
      icon: '/assets/editor/style-panel.svg',
      view: <StyleEditor />,
    },
    {
      key: 'ADD',
      title: 'add',
      icon: '/assets/editor/add-panel.svg',
      view: <AdditionalEditor />,
    },
  ];

  return (
    <>
      <Head>
        <title>Dashboard - {'Editor'}</title>
      </Head>

      <ExportSRTorTXTModal
        type="SRT"
        isModalOpen={isSRTModalOpen}
        setIsModalOpen={setIsSRTModalOpen}
        onDownloadFile={(withTimestamps) =>
          onDownloadSRTFile(subtitlesWithVTTNodeList, withTimestamps)
        }
      />
      <ExportSRTorTXTModal
        type="TXT"
        isModalOpen={isTXTModalOpen}
        setIsModalOpen={setIsTXTModalOpen}
        onDownloadFile={(withTimestamps) =>
          onDownloadTXTFile(subtitlesWithVTTNodeList, withTimestamps)
        }
      />
      <ExportVideoModal
        isModalOpen={isExportModalOpen}
        setIsModalOpen={setIsExportModalOpen}
        onStartExportVideo={(quality, notifyWhenFinishes) =>
          onStartExportVideo({
            quality,
            notifyWhenFinishes,
            user,
            project,
            router,
            subCurrentArray: subtitlesWithVTTNodeList,
          })
        }
      />

      <VideoEditorProvider video={{ url: videoUrl, ...video }}>
        <div className="bg-[#FCFCFC]">
          <EditorHeader
            projectName={video?.name || title}
            setIsSRTModalOpen={setIsSRTModalOpen}
            setIsTXTModalOpen={setIsTXTModalOpen}
            setIsExportModalOpen={setIsExportModalOpen}
            isDemoEditor={true}
          />
          <main className="min-h-screen py-36 sm:px-8 max-w-[1900px] mx-auto overflow-hidden">
            <div className="grid gap-2 px-4 sm:px-0 lg:grid-cols-17">
              <div className="lg:col-span-8">
                <VideoPlayer videoUrl={videoUrl} />
              </div>
              <div className="hidden lg:flex justify-center lg:col-span-1">
                <div className="h-full inline-block rounded-full bg-white w-5" />
              </div>

              <Tabs
                configuration={TABS_CONFIG}
                setTabIndex={setTabIndex}
                tabIndex={tabIndex}
              />
            </div>
            <TimelineContainer />
          </main>
        </div>
      </VideoEditorProvider>
    </>
  );
};

export default Editor;
