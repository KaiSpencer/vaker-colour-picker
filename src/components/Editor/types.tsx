export interface ISubtitleTrack {
  start: number;
  duration: number;
  text: string;
}
