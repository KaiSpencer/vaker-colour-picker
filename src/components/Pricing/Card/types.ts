export type CardInput = {
  id: string;
  plan: string;
  monthlyPriceId: string;
  monthlyPrice: number;
  yearlyPriceId: string;
  yearlyPrice: number;
  recommended?: boolean;
  currency?: string;
  features: string[];
  chatSupport: boolean;
  liveSupport: boolean;
  description: string;
  onClick?: () => void;
  action: string;
};
