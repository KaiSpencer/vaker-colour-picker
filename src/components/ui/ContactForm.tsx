import React, { useEffect } from 'react';
import { MailIcon } from '@heroicons/react/outline';
import { object, string } from 'zod';
import { Form } from '~/components/ui/Form';
import { Input } from './Form/Input';
import { TextArea } from './Form/Textarea';
import { SuccessMessage } from './Form/SuccessMessage';
import { useState } from 'react';
import { CheckCircleIcon } from '@heroicons/react/solid';
import { ErrorMessage } from './Form/ErrorMessage';
import { useZodForm } from './Form/hooks/useZodForm';
const axios = require('axios');

const contactSchema = object({
  email: string().email(),
  name: string().min(1, { message: 'Should be at least 1 character' }),
  message: string().min(1, { message: 'Should be at least 1 character' }),
  plan: string().nullable(),
});

interface Props {
  userPlan?: string;
}

function ContactForm({ userPlan }: Props) {
  const form = useZodForm({
    schema: contactSchema,
  });

  const [submitSuccess, setSubmitSuccess] = useState('');

  const submit = () => {
    setSubmitSuccess('error');
  };

  useEffect(() => {
    form.setValue('plan', userPlan || null);
  }, [userPlan]);

  return (
    <div className="relative flex flex-col z-0 items-center w-full max-w-screen-xl mx-auto mb-44">
      <div className="lg:grid-cols-9 gap-6 sm:p-8 mt-12 space-y-4 rounded-xl shadow-md bg-[#F5FAFE] flex w-full">
        <div className="px-2 sm:p-0 md:col-span-2 xl:col-span-3 z-20 flex flex-col space-y-4">
          <h6 className="text-xl font-bold sm:text-2xl">
            Get in touch with us
          </h6>
          <p className="text-sm leading-4 text-font-2 max-w-md">
            Let us know how we can help. Fill out that form and our team will
            get back to you as soon as possible. Normally in under 24h.
          </p>
          <div className="flex flex-row items-center pt-4 space-x-2 sm:pt-12">
            <MailIcon className="w-6 h-6" />
            <a href="mailto:video@vaker.ai" className="text-lg font-normal">
              {userPlan ? 'member@vaker.ai' : 'video@vaker.ai'}
            </a>
          </div>
        </div>

        <div className="p-8 md:col-span-5 xl:col-span-6 z-50 flex flex-col items-center w-full space-y-4 bg-white rounded-xl shadow-md ">
          {submitSuccess == 'sent' && (
            <SuccessMessage>
              Your contact was submitted successfully
            </SuccessMessage>
          )}
          {submitSuccess == 'error' && (
            <ErrorMessage
              title="An error has occurred"
              showFallback={true}
              fallbackContact={'video@vaker.ai'}
            >
              Your contact was submitted successfully
            </ErrorMessage>
          )}
          <Form form={form} onSubmit={submit} className="w-full">
            {userPlan && (
              <div className="w-full pb-2">
                <div className="text-font-1 flex items-center space-x-1">
                  <CheckCircleIcon className="w-6 h-6 text-green-400" />
                  <div className="pb-0.5">
                    <span className="text-font-2 text-sm">Your plan:</span>
                    <span className="text-font-1 ml-1">{userPlan}</span>
                  </div>
                </div>
              </div>
            )}

            <Input
              placeholder="Your name"
              type="text"
              autoComplete="name"
              autoFocus
              {...form.register('name')}
            />
            <Input
              placeholder="Your email adress"
              type="email"
              autoComplete="email"
              {...form.register('email')}
            />

            <TextArea
              placeholder="How can we help you?"
              className="resize-none"
              {...form.register('message')}
            />
            <button
              type="submit"
              className="w-full px-4 py-3 text-lg font-bold uppercase text-white bg-[#425B7D] rounded-lg hover:opacity-80"
            >
              Send Message
            </button>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default ContactForm;
