import { Switch } from '@headlessui/react';
import { useState } from 'react';

interface SwitchButtonProps {
  initialState: boolean;
  disabled: boolean;
}
export default function SwitchButton({
  disabled,
  initialState,
}: SwitchButtonProps) {
  const [enabled, setEnabled] = useState(initialState);

  return (
    <div className="pt-2 sm:pb-4">
      <Switch
        checked={enabled}
        disabled={disabled}
        onChange={setEnabled}
        className={`${enabled ? 'bg-[#425B7D]' : 'bg-gray-200'}
          relative inline-flex flex-shrink-0 h-[28px] sm:h-[38px] w-[64px] sm:w-[74px] border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus-visible:ring-2  focus-visible:ring-white focus-visible:ring-opacity-75`}
      >
        <span className="sr-only">Use setting</span>
        <span
          aria-hidden="true"
          className={`${enabled ? 'translate-x-9' : 'translate-x-0'}
            pointer-events-none inline-block h-[24px] w-[24px] sm:h-[34px] sm:w-[34px] rounded-full bg-white shadow-lg transform ring-0 transition ease-in-out duration-200`}
        />
      </Switch>
    </div>
  );
}
