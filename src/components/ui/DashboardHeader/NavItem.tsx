import { useRouter } from 'next/router';
import NextLink from 'next/link';

interface INavItemProps {
  label: string;
  href: string;
}

const NavItem = ({ label, href }: INavItemProps) => {
  const { route } = useRouter();

  return (
    <NextLink href={href}>
      <div className="flex flex-col px-4 sm:px-8 md:px-0 cursor-pointer justify-center md:items-center text-xl font-semibold uppercase">
        <div className="md:text-center mb-1.5 text-xs font-bold text-font-1 tracking-wider">
          {label}
        </div>
        <div
          className={`${
            route === href
              ? 'bg-gradient-to-tr opacity-75 from-main to-blue-dashboard shadow-md'
              : 'bg-font-1'
          } w-36 sm:w-28 rounded-full h-1 inline-block`}
        ></div>
      </div>
    </NextLink>
  );
};

export default NavItem;
