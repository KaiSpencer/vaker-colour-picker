import PATHS from '~/constants/paths';

export const NAVIGATION = [
  {
    href: PATHS.DASHBOARD.MY_PROJECTS,
    label: 'my projects',
  },
  {
    href: PATHS.DASHBOARD.UPGRADE,
    label: 'upgrade',
  },
  {
    href: PATHS.DASHBOARD.BILLING,
    label: 'billing',
  },
  {
    href: PATHS.DASHBOARD.SUPPORT,
    label: 'support',
  },
];
