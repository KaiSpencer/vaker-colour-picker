import PATHS from '~/constants/paths';

export const goToDashboard = () => {
  if (window.location.pathname !== PATHS.DASHBOARD.MY_PROJECTS) {
    window.location.pathname = PATHS.DASHBOARD.MY_PROJECTS;
  }
};
