import React from 'react';
import { Disclosure } from '@headlessui/react';
import { MenuIcon, XIcon } from '@heroicons/react/solid';
import NavItem from './NavItem';
import { NAVIGATION } from './constants';
import { goToDashboard } from './helper/goToDashboard';

const DashboardHeader = () => {
  return (
    <Disclosure
      as="header"
      className="fixed top-0 z-30 w-full bg-bg-1 bg-opacity-70 backdrop-filter backdrop-blur-lg"
    >
      {({ open }) => (
        <>
          <div className="sm:px-8 lg:px-12 max-w-[1900px] mx-auto overflow-hidden py-6">
            <div className="flex justify-between">
              <div className="flex items-center">
                <img
                  className="z-50 cursor-pointer w-28"
                  src="/assets/LogoVaker.png"
                  alt="Logo"
                  loading="lazy"
                  onClick={goToDashboard}
                />
              </div>
              <div className="hidden md:flex md:space-x-8">
                {NAVIGATION.map(item => (
                  <div
                    key={item.href}
                    className="flex flex-row items-center justify-center space-x-3"
                  >
                    <NavItem href={item.href} label={item.label} />
                  </div>
                ))}
              </div>

              <div className="flex items-end justify-end md:hidden">
                {/* Mobile menu button*/}
                <Disclosure.Button className="inline-flex items-center justify-center p-2 text-gray-800 rounded-md hover:text-white hover:bg-[#425B7D] focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                  <span className="sr-only">Open main menu</span>
                  {open ? (
                    <XIcon className="block w-6 h-6" aria-hidden="true" />
                  ) : (
                    <MenuIcon className="block w-6 h-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>
            </div>
          </div>
          <Disclosure.Panel className="md:hidden">
            <nav className="space-y-4 py-6">
              {NAVIGATION.map(item => (
                <NavItem key={item.href} href={item.href} label={item.label} />
              ))}
            </nav>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
};

export default DashboardHeader;
