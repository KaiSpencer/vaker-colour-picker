import React from 'react';

interface IErrorMessageProps {
  title: string;
  showFallback?: boolean;
  fallbackContact: string;
}

export const ErrorMessage: React.FC<IErrorMessageProps> = props => {
  const standardErrorMsg = 'You can still reach us at the following address:';
  const mailtoStr = 'mailto:' + (props.fallbackContact ?? '');

  return (
    <div className="w-full max-w-md p-4 space-y-1 border-2 border-red-500 border-opacity-50 rounded-md bg-red-50 dark:bg-red-900 dark:bg-opacity-10">
      {props.title && (
        <h3 className="text-sm font-medium text-red-800 dark:text-red-200">
          {props.title}
          <br />
          {props.showFallback && props.fallbackContact ? (
            <>
              {standardErrorMsg}
              <br />
              <a href={mailtoStr}> {props.fallbackContact}</a>
            </>
          ) : (
            <></>
          )}
        </h3>
      )}
    </div>
  );
};
