import { useFormContext } from 'react-hook-form';

interface IFieldErrorProps {
  name?: string;
}

export function FieldError({ name }: IFieldErrorProps) {
  const {
    formState: { errors },
  } = useFormContext();

  if (!name) {
    return null;
  }

  const error = errors[name];

  if (!error) {
    return null;
  }

  return (
    <div className="text-sm font-medium text-red-500">{error.message}</div>
  );
}
