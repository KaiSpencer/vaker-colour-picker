import clsx from 'clsx';

interface RadioButtonProps {
	setValue: (arg0: any) => void;
	actualRadioGroupValue: any;
	value: any;
	id: string;
	label: string;
	className?: string;
	enabled: boolean;
}

const RadioButton = ({
	setValue,
	actualRadioGroupValue,
	value,
	id,
	label,
	className
}: RadioButtonProps) => {
	return (
		<div
		onClick={() => setValue(value)}
			className={`flex relative items-center justify-center gap-4 group cursor-pointer ${className}`}
		>
			<div
				className={clsx('rounded-full h-4 w-4 2xl:w-5 2xl:h-5', {
					'bg-main': actualRadioGroupValue === value,
					'group-hover:bg-gray-100': actualRadioGroupValue !== value,
				})}
				id={id}
			></div>
			<label 
				className="cursor-pointer" 
				htmlFor={id}
			>
				{label}
			</label>
			<div className="h-4 w-4" />
		</div>
	);
};

export default RadioButton;
