import { Listbox } from '@headlessui/react';
import { CheckIcon } from '@heroicons/react/solid';
import clsx from 'clsx';
import React from 'react';

interface IOptionProps {
  item: any;
}

const Option: React.FC<IOptionProps> = ({ item }) => {
  return (
    <Listbox.Option
      key={(item.value as unknown) as string}
      className={({ active }) =>
        clsx(
          active ? 'text-white bg-main' : 'text-gray-900',
          'cursor-default select-none relative py-2 pl-3 pr-9',
        )
      }
      value={item.value}
    >
      {({ selected, active }) => (
        <>
          <div className="flex items-center">
            <span
              className={clsx(
                selected ? 'font-semibold' : 'font-normal',
                'ml-3 block truncate',
              )}
            >
              {item.label}
            </span>
          </div>

          {selected ? (
            <span
              className={clsx(
                active ? 'text-white' : 'text-main',
                'absolute inset-y-0 right-0 flex items-center pr-4',
              )}
            >
              <CheckIcon className="h-5 w-5" aria-hidden="true" />
            </span>
          ) : null}
        </>
      )}
    </Listbox.Option>
  );
};

export default Option;
