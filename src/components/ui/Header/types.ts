export type ModalProps = {
  state: boolean;
  name: string;
};
