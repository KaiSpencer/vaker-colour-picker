import React from 'react';
import Moveable from 'react-moveable';
import { Position, Size } from './types';

interface IMoveableProps {
  target: any;
  frame: any;
  zoom: number;
  onResize: (size: Size) => void;
  onDragged: (position: Position) => void;
  snappable: boolean;
  bounds?: object;
  verticalGuidelines?: number[];
  horizontalGuidlines?: number[];
}

const MoveableComponent: React.FC<IMoveableProps> = ({
  target,
  frame,
  zoom,
  onDragged,
  onResize,
  snappable,
  bounds,
  verticalGuidelines,
  horizontalGuidlines,
}) => {
  const onResizeStop = (width: number, height: number) => {
    let newSize = {
      width,
      height,
    };
    onResize(newSize);
  };

  const onDragStop = (x: number, y: number) => {
    let newPosition = {
      x,
      y,
    };
    onDragged(newPosition);
  };

  return (
    <Moveable
      zoom={zoom}
      container={null}
      target={target}
      draggable={true}
      throttleDrag={0}
      resizable={true}
      throttleResize={0}
      rotatable={true}
      rotationPosition={'top'}
      throttleRotate={0}
      snappable={snappable}
      bounds={bounds}
      origin={false}
      verticalGuidelines={verticalGuidelines}
      horizontalGuidelines={horizontalGuidlines}
      padding={{ left: 0, top: 0, right: 0, bottom: 0 }}
      onDragStart={({ set }) => {
        set(frame.translate);
      }}
      onDrag={({ beforeTranslate }) => {
        frame.translate = beforeTranslate;
      }}
      onDragEnd={({ target, isDrag }) => {
        if (target.style.transform) {
          let app = target.style.transform.split('(');
          let app2 = app[1].split(')');
          let app3 = app2[0].split('px');
          let x = app3[0];
          let y = app3[1].split(',');
          onDragStop(parseFloat(x), parseFloat(y[1]));
        }
      }}
      onResizeStart={({ setOrigin, dragStart }) => {
        setOrigin(['%', '%']);
        dragStart && dragStart.set(frame.translate);
      }}
      onResize={({ target, width, height, drag }) => {
        frame.translate = drag.beforeTranslate;

        target.style.width = `${width}px`;
        target.style.height = `${height}px`;
      }}
      onResizeEnd={({ target }) => {
        if (target.style) {
          let width = target.style.width.split('px', 1);
          let height = target.style.height.split('px', 1);
          onResizeStop(parseFloat(width[0]), parseFloat(height[0]));
        }
      }}
      onRotateStart={({ set }) => {
        set(frame.rotate);
      }}
      onRotate={({ beforeRotate }) => {
        frame.rotate = beforeRotate;
      }}
      onRender={({ target }) => {
        target.style.transform = `translate(${frame.translate[0]}px, ${frame.translate[1]}px) rotate(${frame.rotate}deg)`;
      }}
    />
  );
};

export default MoveableComponent;
