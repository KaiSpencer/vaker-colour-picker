export interface Size {
  width: number | 'auto';
  height: number | 'auto';
}

export interface Position {
  x: number;
  y: number;
}
