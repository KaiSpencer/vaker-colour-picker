import { XIcon } from '@heroicons/react/solid';
import { AnimatePresence, motion } from 'framer-motion';
import React, { useEffect, useRef, useState } from 'react';

interface IProgressBarProps {
  progress: number;
  exportPage?: boolean;
  handleCancel?: () => void;
}

function ProgressBar({
  progress,
  exportPage,
  handleCancel,
}: IProgressBarProps) {
  const progressRef = useRef<HTMLDivElement>(null);
  const [showCancel, setShowCancel] = useState<boolean>(false);

  useEffect(() => {
    if (progressRef && progressRef.current) {
      progressRef.current.style.transform = `scaleX(${1 - 0.01 * progress})`;
    }
  }, [progress]);

  if (exportPage) {
    return (
      <>
        <div
          onMouseLeave={() => setShowCancel(false)}
          className="group relative w-full h-7 rounded-full overflow-hidden text-center border border-white"
        >
          <div
            className={`absolute grid grid-cols-3 z-20 text-center inset-0 top-[2px] transition-all ${
              progress < 50 ? 'text-font-1' : 'text-white'
            } `}
          >
            <div />
            <div className="text-center">{progress - 1} %</div>
            <div
              className={`w-4 h-4 justify-self-end self-center mr-1 mb-0.5 ${
                progress < 98 ? 'text-font-1' : 'text-white'
              }`}
            >
              <div
                onTouchStart={handleCancel}
                onMouseEnter={() => setShowCancel(true)}
                className="cursor-pointer"
              >
                <XIcon />
              </div>
            </div>
          </div>
          <div className=""></div>
          <div className="absolute inset-0 bg-gradient-to-r from-[#F5FAFE] to-main"></div>

          <div
            ref={progressRef}
            className="h-full w-full absolute inset-0 bg-bg-2 origin-right transform"
          ></div>
          <AnimatePresence>
            {showCancel && (
              <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ type: 'just' }}
                onClick={handleCancel}
                className="cursor-pointer z-50 w-full font-medium text-font-1 absolute inset-0 bg-[#F5FAFE] uppercase opacity-0 transition-opacity"
              >
                <div className="relative top-0.5">cancel export</div>
              </motion.div>
            )}
          </AnimatePresence>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="relative w-full h-5 rounded-tl-xl rounded-tr-xl overflow-hidden text-center">
        <div
          className={`absolute z-50 text-center left-0 right-0 transition-all ${
            progress < 50 ? 'text-font-1' : 'text-white'
          } `}
        >
          {progress - 1} %
        </div>
        <div className={`h-full w-full absolute inset-0 bg-main`} />
        <div
          ref={progressRef}
          className="h-full w-full absolute inset-0 bg-bg-2 origin-right transform"
        ></div>
      </div>
    </>
  );
}

export default ProgressBar;
