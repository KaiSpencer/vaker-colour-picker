import React, { FC } from 'react';

export const tooltipParentClass = 'has-tooltip';

interface Props {
  className?: string;
}

const Tooltip: FC<Props> = ({ className = '', children }) => {
  return (
    <span
      className={`tooltip rounded shadow-lg py-2 px-4 bg-gray-100 text-black w-auto ${className}`}
    >
      {children}
    </span>
  );
};

export default Tooltip;
