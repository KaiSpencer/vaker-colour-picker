export default [
    {
        "backgroundColor": "#dc6911",
        "borderRounded": 0,
        "progressColor": "#43597D"
    },
    {
        "backgroundColor": "#ffffff",
        "borderRounded": 5,
        "progressColor": "#000000"
    }
]