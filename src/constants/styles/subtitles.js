export default [
    {
        "name": "Default Style",
        "font": "arial",
        "fontSize": 26,
        "fontColor": "#ffffff",
        "backgroundActive": true,
        "borderRounded": 0,
        "backgroundColor": "#000000",
        "textTransform": "none",
        "textAlign": "center",
        "bold": false,
        "underline": false,
        "italic": false
    },
    {
        "name": "Invisible Spell",
        "font": "avenir",
        "fontSize": 26,
        "fontColor": "#606060",
        "backgroundActive": true,
        "borderRounded": 0,
        "backgroundColor": "#000000",
        "textTransform": "none",
        "textAlign": "center",
        "bold": false,
        "underline": false,
        "italic": false
    },
    {
        "name": "Intellectual Bubble",
        "font": "helvetica",
        "fontSize": 26,
        "fontColor": "#F5A55E",
        "backgroundActive": true,
        "borderRounded": 0,
        "backgroundColor": "#000000",
        "textTransform": "uppercase",
        "textAlign": "center",
        "bold": false,
        "underline": false,
        "italic": false
    }
]