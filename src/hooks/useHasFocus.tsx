import React, { useCallback, useState, useEffect, useDebugValue } from 'react';

interface Props {
  onFocusGained?: (target: HTMLSpanElement) => void;
  onFocusLost?: (target: HTMLSpanElement) => void;
}

interface ResponseProps {
  onFocus: (e: React.FocusEvent) => void;
  onBlur: (e: React.FocusEvent) => void;
}

const useHasFocus = ({
  onFocusGained,
  onFocusLost,
}: Props): [ResponseProps, boolean] => {
  const [hasFocus, setHasFocus] = useState(false);
  const [target, setTarget] = useState<HTMLSpanElement | null>(null);

  useDebugValue(hasFocus ? 'Has Focus' : "Hasn't Focus");

  useEffect(() => {
    if (!target) {
      return;
    }

    if (!hasFocus) {
      !!onFocusLost && onFocusLost(target);
    } else {
      !!onFocusGained && onFocusGained(target);
    }
  }, [hasFocus, target]);

  const onFocus = useCallback(
    (e) => {
      if (
        (e.currentTarget === e.target ||
          !e.currentTarget.contains(e.relatedTarget)) &&
        !hasFocus
      ) {
        setHasFocus(true);
        setTarget(e.target);
      }
    },
    [setHasFocus, hasFocus, setTarget],
  );

  const onBlur = useCallback(
    (e) => {
      if (
        (e.currentTarget === e.target ||
          !e.currentTarget.contains(e.relatedTarget)) &&
        !!hasFocus
      ) {
        setHasFocus(false);
        setTarget(e.target);
      }
    },
    [setHasFocus, hasFocus, setTarget],
  );

  const props: ResponseProps = { onFocus, onBlur };

  return [props, hasFocus];
};

export default useHasFocus;
