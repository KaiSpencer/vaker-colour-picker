/**
 * @author Gerardo Perez <gperezmanjarrez@gmail.com>
 * This hook controls almost all the interactions between video player and timeline
 */
import React, { createContext, useContext, useEffect, useState } from 'react';

interface IVideoEditorContext {
  videoUrl: string;
  audioChannelData: Float32Array | null;
  playing: boolean;
  setPlaying: React.Dispatch<React.SetStateAction<boolean>>;
  duration: number;
  setDuration: React.Dispatch<React.SetStateAction<number>>;
  playedSeconds: number;
  setPlayedSeconds: React.Dispatch<React.SetStateAction<number>>;
}

export const VideoEditorContext = createContext<IVideoEditorContext>(
  {} as IVideoEditorContext,
);
export const useVideoEditor = () => useContext(VideoEditorContext);

interface IVideoEditorProvider {
  video: {
    url: string;
    wavChannelData?: number[];
  };
}

const VideoEditorProvider: React.FC<IVideoEditorProvider> = ({
  video,
  children,
}) => {
  const [audioChannelData, setAudioChannelData] = useState<Float32Array | null>(
    null,
  );
  const [playing, setPlaying] = useState<boolean>(false);
  const [duration, setDuration] = useState<number>(0);
  const [playedSeconds, setPlayedSeconds] = useState<number>(0);

  useEffect(() => {
    const getAudioChannelData = async (url: string) => {
      const response = await fetch(url, { mode: 'no-cors' });

      try {
        const arrayBuffer = await response.arrayBuffer();
        const audioContext = new AudioContext();
        const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);

        setAudioChannelData(audioBuffer.getChannelData(0));
      } catch (error) {
        console.error(error);

        setAudioChannelData(null);
      }
    };

    console.debug('Hello World!', { video });

    if (!!video.wavChannelData) {
      setAudioChannelData(Float32Array.from(video.wavChannelData));
    } else if (video.url) {
      getAudioChannelData(video.url);
    } else {
      setAudioChannelData(null);
    }
  }, [video.wavChannelData, video.url, setAudioChannelData]);

  return (
    <VideoEditorContext.Provider
      value={{
        videoUrl: video.url,
        audioChannelData,
        playing,
        setPlaying,
        duration,
        setDuration,
        playedSeconds,
        setPlayedSeconds,
      }}
    >
      {children}
    </VideoEditorContext.Provider>
  );
};

export default VideoEditorProvider;
