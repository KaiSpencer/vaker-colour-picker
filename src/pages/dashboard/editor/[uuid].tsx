import React, { FC, useEffect } from 'react';
import { useRouter } from 'next/router';
import collect from 'collect.js';
import { useDispatch } from 'react-redux';
import getVideo from '~/utils/api/getVideo';
import Editor from '.';
import getUser from '~/utils/api/getUser';
import { ITranslationWord } from '~/redux/editor/types';
import {
  setCurrentTranslation,
  setSubtitles,
} from '~/redux/editor/editor.actions';

const ProjectEditor: FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  // You must be authenticated to visit this page
  const { uuid } = router.query;

  const { user, error: userError } = getUser();
  const { video, error: videoError } = getVideo(uuid as string);

  useEffect(() => {
    dispatch(setCurrentTranslation({ WordsGrammatical: undefined }));

    dispatch(setSubtitles([]));
  }, []);

  // No SSR here
  if (typeof window === 'undefined') {
    return null;
  }

  if (!user || !video?.words_grammatical) {
    console.warn('Prevent redirect due missing dependencies', { user, video });

    if (userError) {
      console.error(userError);
    }

    if (videoError) {
      console.error(videoError);
    }

    return null;
  }

  return <Editor video={video}></Editor>;
};

export default ProjectEditor;
