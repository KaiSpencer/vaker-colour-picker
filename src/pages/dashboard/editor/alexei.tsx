import React, { FC } from 'react';
import { useRouter } from 'next/router';
import getVideo from '~/utils/api/getVideo';
import Editor from '.';
import getUser from '~/utils/api/getUser';

const ProjectEditor: FC = () => {
  const router = useRouter();

  // You must be authenticated to visit this page
  const { user, error: userError } = getUser();
  const { uuid: videoUuid } = router.query;
  const { video, error: videoError } = getVideo(videoUuid as string);

  // No SSR here
  if (typeof window === 'undefined') {
    return null;
  }

  if (!user || !video) {
    console.warn('Prevent redirect due missing dependencies');
    if (userError) console.error(userError);
    if (videoError) console.error(videoError);
    return null;
  }

  return <Editor url={'/assets/testreal.mp4'}></Editor>;
};

export default ProjectEditor;
