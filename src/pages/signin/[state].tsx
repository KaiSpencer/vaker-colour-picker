import { useEffect } from 'react';
import { NextSeo } from 'next-seo';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { login, logout } from '~/utils/loginHelper';
import { Container } from '~/components/ui/Container';
import { SuccessMessage } from '~/components/ui/Form/SuccessMessage';
import { signIn } from '~/redux/user/user.actions';
import PATHS from '~/constants/paths';

export default function SingIn(props: any) {
  const router = useRouter();
  const { state, t: jsonWebToken } = router.query;

  const dispatch = useDispatch();

  useEffect(() => {
    // This happens during server-side rendering
    if (state === undefined && jsonWebToken === undefined) {
      return;
    }

    (async () => {
      if (state === 'success' && typeof jsonWebToken === 'string') {
        const loginResult = login(jsonWebToken);
        if (loginResult) {
          dispatch(signIn(jsonWebToken));

          setTimeout(() => {
            router.push(PATHS.DASHBOARD.MY_PROJECTS);
          }, 1000);

          return;
        }
      }

      await logout();
      router.push('/');
    })();
  });

  return (
    <>
      <NextSeo title="Login Success" />
      <Container title="Redirecting">
        <SuccessMessage>
          Your signin was successful. We are redirecting you
        </SuccessMessage>
      </Container>
    </>
  );
}
