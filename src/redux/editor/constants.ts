import { cloneDeep } from 'lodash';
import {
  IEditorHeadlineStyles,
  IEditorKaraokeStyles,
  IEditorLogo,
  IEditorProgressBarStyles,
  IEditorState,
  IEditorTextStyles,
  IEditorVideoStyles,
  IFontStyles,
} from './types';
import demoConstants from './demo.constants';

export const defaultfontStyles: IFontStyles = {
  bold: false,
  underline: false,
  italic: false,
};

export const defaultSubtitleTextStyles: IEditorTextStyles = {
  template: 0,
  name: 'Default Style',
  active: true,
  font: 'arial',
  fontSize: 26,
  fontColor: '#ffffff',
  backgroundActive: true,
  borderRounded: 0,
  backgroundColor: '#000000',
  textTransform: 'none',
  textAlign: 'center',
  bold: false,
  underline: false,
  italic: false,
};

export const defaultKaraokeTextStyles: IEditorKaraokeStyles = {
  active: false,
  fontColor: '#82d6f2',
  effectType: false,
};

export const defaultHeadlineTextStyles: IEditorHeadlineStyles = {
  template: 0,
  name: 'Default Style',
  active: false,
  headline: 'Add Headline',
  font: 'arial',
  fontSize: 36,
  fontColor: '#ffffff',
  backgroundActive: true,
  borderRounded: 0,
  backgroundColor: '#000000',
  textTransform: 'none',
  textAlign: 'center',
  bold: false,
  underline: false,
  italic: false,
};

export const defaultProgressBarStyles: IEditorProgressBarStyles = {
  template: 0,
  active: false,
  backgroundColor: '#dc6911',
  borderRounded: 0,
  progressColor: '#43597D',
};

export const defaultVideoLogo: IEditorLogo = {
  file: null,
  blobData: null,
  size: {
    width: 100,
    height: 'auto',
  },
  position: {
    x: 0,
    y: 0,
  },
};

export const defaultVideoStyles: IEditorVideoStyles = {
  backgroundColor: '#000000',
};

export const demoState: IEditorState = {
  currentVideoTranslation: {
    id: 'e6769b6372674907a8e00f1c8f356053',
    recognitionStatus: 'Demo',
    offset: 8600000,
    duration: 301100000,
    displayText:
      "Nothing in the universe is static in the Milky Way. Billions of stars orbit the galactic center or some like our sun are pretty consistent. Keeping a distance of around 30,000 light years from the Galactic Center completing an orbit every 230,000,000 years. This dance is not an orderly ballet, more like a skating rink filled with drunk toddlers. This chaos makes the Galaxy dangerous. Our solar neighborhood is constantly changing. With stars moving hundreds of kilometres every second, only the vast distances between objects protect us from the dangers out there, but we might get unlucky in the future. At some point we could encounter a star going supernova or a massive object passing by and showering Earth with asteroids. If something like this were to happen, we would likely know thousands, if not millions of years in advance, but we still couldn't do much about it. Unless we move our whole solar system out of the way. To move the solar system, we need a stellar engine, a megastructure used to steer a star through the Galaxy. It's the kind of thing that might be built by an advanced civilization with Dyson sphere level technology. That's thinking about their future millions of years ahead of time. But how do we possibly move the hundreds of thousands of objects? In the solar system, the good news is we can ignore all of that. We only need to move the sun or the other stuff is glued to it by gravity and will follow it wherever it decides to go. There are lots of ideas about what a stellar engine might look like and how it would work. We've picked two grounded in our current understanding of physics that could be built. In theory, the simplest kind of stellar engine is this cut off thruster, a giant mirror, it works. On the same principle as a rocket like rocket fuel, the photons released as solar radiation carry momentum. Not a lot, but a bit. For example, if an astronaut turned on a flashlight in space, it would push them backwards very, very slowly. A stellar engine will work a little better than a flashlight because the sun produces a lot of photons. The basic idea of the shuttle thruster is to reflect up to half of the solar radiation to create thrust and slowly push the sun where we wanted to go. In order for this cutoff thruster to work, it must be kept in the same place, not orbiting the sun. Although the Suns gravity will try to put it in, it would be supported by radiation pressure, which props the mirror up. This means the mirror would have to be very light made of Micron thin reflecting foil from materials like aluminium alloys. The mirror shape is important too. Enveloping the sun in a giant spherical shell wouldn't work, because that would refocus like that to the sun, heating it up and creating all sorts of unpleasant problems. Instead, we use a parabola, which sends most of the photons around the sun, and in the same direction, which maximizes thrust. To prevent accidentally burning or freezing earth with too much or too little sunlight, the only safe place to build a scatter thruster is over the sun's poles. This means we can only move the sun vertically in the plane of the solar system and One Direction in the Milky Way, which limits our travel options a base. But that is basically it. Before a civilization capable of building a Dyson sphere, this is a relatively simple endeavor, not complicated, just very hard to build. At full throttle, the solar system could probably be moved by about 100 light years over 230,000,000 years. Over a few billion years, it gives us near complete control over the Sun's orbit in the Galaxy, but in the short term this might not be fast enough to dodge a deadly supernova. That's why we thought we could do better, so we asked our astrophysicist friend if he could design A faster stellar engine for this video he did. And wrote a paper about it that's been published in a peer reviewed journal. You can find it in our sources document. We're going to call our new stellar engine the Caplan thruster. It works a lot like a traditional rocket shoot exhaust. One way to push yourself the other. It's a large space station platform powered by a Dyson sphere that gathers matter from the Sun to power nuclear fusion. It shoots out a very fast jet of particles that nearly 1% the speed of light out of the solar system. A second jet pushes the sun along like a tugboat. The Captain thruster requires a lot of fuel. Millions of tons per second to gather this fuel. Our thruster uses very large electromagnetic fields to funnel hydrogen and helium from the solar wind into the engine. The solar wind alone doesn't provide enough for your vote, and that's where the Dyson sphere comes in. Using its power, sunlight can be refocused to the surface of the sun. This heat small regions to extreme temperatures, lifting billions of tons of mass off the sun. This mass can be collected and separated into hydrogen and helium. The helium is burned explosively in thermonuclear fusion reactors. A jet of radioactive oxygen at a temperature of nearly a billion degrees is expelled and becomes our primary source of propulsion from our stellar engine to prevent the engine from just crashing into the sun, it needs to balance itself. To do this, we accelerate the collected hydrogen with electromagnetic fields using particle accelerators, and shoot a jet back at the Sun. This balances. The thruster and transfers the thrust of our engine banked the sun in as little as a million years. This engine can move the sun by 50 light years, more than enough to Dodger supernova at full throttle. The solar system can be completely redirected in its galactic orbit in 10 million years. But wait, what we use up the sun this way. Fortunately, the sun is so massive that even billions of tons of material will barely scratch the surface. In fact, this megastructure will actually extend our son's life, since lower mass stars burn slower, keeping the solar system in habitable for many more billions of years with a Caplan thruster, we could turn the entire solar system into our spaceship, for example by orbiting backwards in the Galaxy. And colonizing hundreds or thousands of stars as we pass by them. It may even be possible to escape the Galaxy entirely and expand beyond the Milky Way. Stellar engines are the kind of machines built by civilizations, thinking not in terms of years or decades but eons. Since we know that our sun will die one day, a stellar engine could allow the far future descendants of humans to travel to other stars without ever having to venture into the terrifying dark abyss of interstellar space. Until we build a stellar engine where adrift and subject to the whims of the galactic, see, we may not like where it leads us. Maybe our descendants will set sail and become an interstellar species for millions of years to come. This was our last video for the year 12,000 and 19 of this human error and what a year it was. So much stuff happened everywhere to so many different people. Calendars and holidays are just imaginary, but they help us to cut our lives into pieces that our brains can handle. We're leaving 12,019 behind with a weird mixture of disillusion and hope the world is screwed up, but we can fix it in a few days. This year will be over and we all get to try again. Thank you for watching our videos and for sticking around for so many years. See you all in 12,000 and 20.",
    WordsGrammatical: demoConstants.words_grammatical,
  },
  subtitles: [],
  styles: {
    subtitle: cloneDeep(defaultSubtitleTextStyles),
    karaokesubtitle: cloneDeep(defaultSubtitleTextStyles),
    karaoke: cloneDeep(defaultKaraokeTextStyles),
    headline: cloneDeep(defaultHeadlineTextStyles),
    progress: cloneDeep(defaultProgressBarStyles),
    video: cloneDeep(defaultVideoStyles),
  },
  logo: cloneDeep(defaultVideoLogo),
};

export const initialState: IEditorState = {
  currentVideoTranslation: {
    id: '',
    recognitionStatus: 'Empty',
    offset: 0,
    duration: 0,
    displayText: 'This is the default state, you should not see this message',
    WordsGrammatical: [],
  },
  subtitles: [],
  styles: {
    subtitle: cloneDeep(defaultSubtitleTextStyles),
    karaokesubtitle: cloneDeep(defaultSubtitleTextStyles),
    karaoke: cloneDeep(defaultKaraokeTextStyles),
    headline: cloneDeep(defaultHeadlineTextStyles),
    progress: cloneDeep(defaultProgressBarStyles),
    video: cloneDeep(defaultVideoStyles),
  },
  logo: cloneDeep(defaultVideoLogo),
};
