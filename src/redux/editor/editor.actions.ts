import collect from 'collect.js';
import {
  CURRENT_TRANSLATIONS,
  RESET_VIDEO_LOGO,
  SET_SETTINGS,
  SET_SUBTITLES,
  SET_VIDEO_LOGO,
  SET_VIDEO_LOGO_POSITION,
  SET_VIDEO_LOGO_SIZE,
  SET_VIDEO_SUBTITLE_POSITION,
  SET_VIDEO_SUBTITLE_SIZE,
  SET_VIDEO_HEAD_SIZE,
  SET_VIDEO_HEAD_POSITION,
  SET_VIDEO_SIZE,
  SET_VIDEO_POSITION,
  SET_CONTAINER_SIZE,
} from '~/redux/editor/editor.types';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
  IVideoTranslation,
} from '~/redux/editor/types';
import { RootState } from '~/redux/rootStore';
import { generateVttNodeList } from '~/utils/generateVttFile';
import convertSubtitleWordsToSentences from '~/components/Editor/Tabs/SubtitleEditor/HtmlEditor/utils/convertSubtitleWordsToSentences';
import convertSubtitleSentenceToWords from '~/components/Editor/Tabs/SubtitleEditor/HtmlEditor/utils/convertSubtitleSentenceToWords';

export const setCurrentTranslation = (
  currentVideoTranslation: Partial<IVideoTranslation>,
) => {
  return (dispatch, getState) => {
    const { editor }: RootState = getState();

    return {
      type: CURRENT_TRANSLATIONS,
      currentVideoTranslation: {
        ...editor.currentVideoTranslation,
        ...currentVideoTranslation,
      },
    };
  };
};

export const setSettings = (
  configKey: string,
  name: string,
  value: string | number | boolean,
) => {
  return {
    type: SET_SETTINGS,
    configKey,
    name,
    value,
  };
};

export const setSubtitles = (subtitles: Array<ITranslationWord>) => ({
  type: SET_SUBTITLES,
  subtitles: subtitles.map((subtitle, index) => ({ ...subtitle, id: index })),
});

export const updateSubtitleWithVTTNode = (sentence) => {
  return (dispatch, getState) => {
    const { subtitles: previousSubtitles } = getState().editor;

    const {
      originalStartId,
      originalEndId,
      words = [],
      duration = '0',
      offset = '0',
    } = collect(convertSubtitleWordsToSentences(previousSubtitles)).firstWhere(
      'text',
      sentence.text,
    );

    const MULTIPLIER = 10000 * 1000;

    const changeInDuration =
      sentence.duration * MULTIPLIER - parseFloat(duration.toString());
    const changeInOffset =
      sentence.start * MULTIPLIER - parseFloat(offset.toString());

    const totalDuration: number = words
      .map(({ Duration: duration }): number =>
        parseFloat(duration?.toString() ?? '0'),
      )
      .reduce((accumulator, currentValue) => accumulator + currentValue);

    const recalculateWordOffsetAndDuration = (
      words,
      index = 0,
      runningOffset = 0,
    ) => {
      const nextIndex = index + 1;
      if (nextIndex > words.length) {
        return words;
      }

      const { Duration: duration, ...word } = words[index];

      const changeInDurationForThisWord =
        changeInDuration * (duration / totalDuration);

      const offset =
        index === 0
          ? sentence.start * MULTIPLIER
          : word.Offset + runningOffset + changeInOffset;

      words[index] = {
        ...word,
        Offset: offset,
        Duration: duration + changeInDurationForThisWord,
      };

      return recalculateWordOffsetAndDuration(
        words,
        nextIndex,
        changeInDurationForThisWord + runningOffset,
      );
    };
    const replacementSubtitles = recalculateWordOffsetAndDuration([...words]);

    const startIndex = previousSubtitles.findIndex(
      ({ id }) => id === originalStartId,
    );
    const length =
      previousSubtitles.findIndex(({ id }) => id === originalEndId) -
      startIndex +
      1;

    const newSubtitles = [...previousSubtitles];
    newSubtitles.splice(startIndex, length, ...replacementSubtitles);

    dispatch(setSubtitles(newSubtitles));
  };
};

export const setVideoLogo = (logo: File, blobData: string) => {
  return {
    type: SET_VIDEO_LOGO,
    logo,
    blobData,
  };
};

export const setVideoLogoSize = (size: {
  width: number | 'auto';
  height: number | 'auto';
}) => {
  return {
    type: SET_VIDEO_LOGO_SIZE,
    size,
  };
};

export const setVideoLogoPosition = (position: { x: number; y: number }) => {
  return {
    type: SET_VIDEO_LOGO_POSITION,
    position,
  };
};

export const setVideoSubtitleSize = (size: {
  width: number | 'auto';
  height: number | 'auto';
}) => {
  return {
    type: SET_VIDEO_SUBTITLE_SIZE,
    size,
  };
};

export const setVideoSubtitlePosition = (position: {
  x: number;
  y: number;
}) => {
  return {
    type: SET_VIDEO_SUBTITLE_POSITION,
    position,
  };
};
export const setVideoHeadSize = (size: {
  width: number | 'auto';
  height: number | 'auto';
}) => {
  return {
    type: SET_VIDEO_HEAD_SIZE,
    size,
  };
};

export const setVideoHeadPosition = (position: { x: number; y: number }) => {
  return {
    type: SET_VIDEO_HEAD_POSITION,
    position,
  };
};
export const setVideoSize = (size: {
  width: number | 'auto';
  height: number | 'auto';
}) => {
  return {
    type: SET_VIDEO_SIZE,
    size,
  };
};

export const setContainerSize = (size: {
  width: number | 'auto';
  height: number | 'auto';
}) => {
  return {
    type: SET_CONTAINER_SIZE,
    size,
  };
};

export const setVideoPosition = (position: { x: number; y: number }) => {
  return {
    type: SET_VIDEO_POSITION,
    position,
  };
};
export const resetVideoLogo = () => {
  return {
    type: RESET_VIDEO_LOGO,
  };
};
