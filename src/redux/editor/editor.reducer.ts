/* eslint-disable max-len */
import {
  CURRENT_TRANSLATIONS,
  RESET_VIDEO_LOGO,
  SET_SETTINGS,
  SET_SUBTITLES,
  SET_VIDEO_LOGO,
  SET_VIDEO_LOGO_POSITION,
  SET_VIDEO_LOGO_SIZE,
} from '~/redux/editor/editor.types';
import { cloneDeep } from 'lodash';
import { EditorStyleName, IEditorState } from './types';
import { defaultVideoLogo, demoState } from './constants';

const ACTION_HANDLERS: any = {
  [CURRENT_TRANSLATIONS]: (
    state: IEditorState,
    { currentVideoTranslation }: any,
  ) => ({
    ...state,
    currentVideoTranslation,
  }),
  [SET_SETTINGS]: (state: IEditorState, { configKey, name, value }: any) => ({
    ...state,
    styles: {
      ...state?.styles,
      [configKey]: {
        ...(state?.styles || {})[configKey as EditorStyleName],
        [name]: value,
      },
    },
  }),
  [SET_SUBTITLES]: (state: IEditorState, { subtitles }: any) => ({
    ...state,
    subtitles,
  }),
  [SET_VIDEO_LOGO]: (state: IEditorState, { logo, blobData }: any) => ({
    ...state,
    logo: {
      ...state.logo,
      file: logo,
      blobData,
    },
  }),
  [SET_VIDEO_LOGO_SIZE]: (state: IEditorState, { size }: any) => ({
    ...state,
    logo: {
      ...state.logo,
      size,
    },
  }),
  [SET_VIDEO_LOGO_POSITION]: (state: IEditorState, { position }: any) => ({
    ...state,
    logo: {
      ...state.logo,
      position,
    },
  }),
  [RESET_VIDEO_LOGO]: (state: IEditorState) => ({
    ...state,
    logo: cloneDeep(defaultVideoLogo),
  }),
};

export default function editorReducer(state = demoState, action: any) {
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(state, action) : state;
}

export { defaultVideoLogo };
