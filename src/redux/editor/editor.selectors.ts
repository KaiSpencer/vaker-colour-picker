import { createSelector } from 'reselect';
import { NodeList } from 'subtitle';
import { RootState } from '~/redux/rootStore';
import { ITranslationWord } from '~/redux/editor/types';
import { generateVttNodeList } from '~/utils/generateVttFile';

export const selectSubtitlesWithVTTNodeList = createSelector(
  ({ editor }: RootState) => editor?.subtitles,
  (subtitles: ITranslationWord[]): NodeList => generateVttNodeList(subtitles),
);
