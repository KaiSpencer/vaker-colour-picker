export const CURRENT_TRANSLATIONS = 'CURRENT_TRANSLATIONS';
export const SET_SETTINGS = 'SET_SETTINGS';
export const SET_SUBTITLES = 'SET_SUBTITLES';
export const SET_VIDEO_LOGO = 'SET_VIDEO_LOGO';
export const SET_VIDEO_LOGO_SIZE = 'SET_VIDEO_LOGO_SIZE';
export const SET_VIDEO_LOGO_POSITION = 'SET_VIDEO_LOGO_POSITION';
export const SET_VIDEO_SUBTITLE_SIZE = 'SET_VIDEO_SUBTITLE_SIZE';
export const SET_VIDEO_SUBTITLE_POSITION = 'SET_VIDEO_SUBTITLE_POSITION';
export const SET_VIDEO_HEAD_SIZE = 'SET_VIDEO_HEAD_SIZE';
export const SET_VIDEO_HEAD_POSITION = 'SET_VIDEO_HEAD_POSITION';
export const SET_VIDEO_SIZE = 'SET_VIDEO_SIZE';
export const SET_VIDEO_POSITION = 'SET_VIDEO_POSITION';
export const RESET_VIDEO_LOGO = 'RESET_VIDEO_LOGO';
export const SET_CONTAINER_SIZE = 'SET_CONTAINER_SIZE';
