import { CSSProperties } from 'react';

export enum TranslationWordTypeEnum {
  WORD = 'word',
  DIVIDER = 'spacer',
}

// Some types, extracted from the initial data, based on Azure speech service I guess
export interface ITranslationWord {
  id?: number;
  Word?: string;
  Duration?: number;
  Offset?: string | number;
  type?: TranslationWordTypeEnum;
}

export interface ITranslationMatch {
  Confidence: number;
  Lexical: string;
  ITN: string;
  MaskedITN: string;
  Display: string;
  Words: Array<ITranslationWord>;
}

export interface IVideoTranslation {
  id: string;
  displayText: string;
  recognitionStatus: string;
  offset: number;
  duration: number;
  WordsGrammatical: ITranslationWord[];
}

export interface IFontStyles {
  //NOT USED
  bold?: boolean;
  underline?: boolean;
  italic?: false;
}
export interface IEditorTextStyles {
  template: number;
  name: string;
  active: boolean;
  font: string;
  fontSize: number;
  fontColor: string;
  backgroundActive: boolean;
  backgroundColor: string;
  borderRounded: number;
  fontStyle?: IFontStyles;
  bold: boolean;
  underline: boolean;
  italic: boolean;
  fontType?: 'outline' | 'splitBox' | 'shadow';
  textTransform: CSSProperties['textTransform'];
  textAlign: CSSProperties['textAlign'];
}

export interface IEditorHeadlineStyles extends IEditorTextStyles {
  headline: string;
}

export interface IEditorKaraokeStyles {
  active: boolean;
  fontColor: string;
  effectType: boolean;
}

export interface IEditorProgressBarStyles {
  template: number;
  active: boolean;
  backgroundColor: string;
  borderRounded: number;
  progressColor?: string;
}

export interface IEditorLogo {
  file: File | null;
  blobData: string | null;
  size: {
    width: number | 'auto';
    height: number | 'auto';
  };
  position: {
    x: number;
    y: number;
  };
}

export interface IEditorVideoStyles {
  backgroundColor: string;
}

export interface IEditorSizePosition {
  size: {
    width: number | 'auto';
    height: number | 'auto';
  };
  position: {
    x: number;
    y: number;
  };
}

export interface Container {
  size: {
    width: number;
    height: number;
  };
}

export interface IEditorState {
  currentVideoTranslation: IVideoTranslation;
  subtitles: ITranslationWord[];
  styles: {
    subtitle: IEditorTextStyles;
    karaokesubtitle: IEditorTextStyles;
    karaoke: IEditorKaraokeStyles;
    headline: IEditorHeadlineStyles;
    progress: IEditorProgressBarStyles;
    video: IEditorVideoStyles;
  };
  logo: IEditorLogo;
  subtitle?: IEditorSizePosition;
  head?: IEditorSizePosition;
  video?: IEditorSizePosition;
  container?: Container;
}

export type EditorStyleName = keyof IEditorState['styles'];
