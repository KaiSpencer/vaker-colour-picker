import { SET_PROJECT } from './project.types';
import CookieStorage from '~/utils/CookieStorage';

export const setProject = (uuid: string) => {
  CookieStorage.setValue('selectedProject', uuid);
  return {
    type: SET_PROJECT,
    selectedProject: uuid,
  };
};
