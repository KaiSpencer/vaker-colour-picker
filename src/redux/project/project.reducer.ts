import { SET_PROJECT } from './project.types';
import { getSelectedProject } from '~/utils/projectHelper';

export interface IProjectState {
  selectedProject: string;
}
const ACTION_HANDLERS: any = {
  [SET_PROJECT]: (state: any, { selectedProject }: any) => ({
    ...state,
    selectedProject,
  }),
};

const getInitialState = () => {
  return {
    selectedProject: getSelectedProject(),
  };
};

export default function projectReducer(state = null, action: any) {
  const projectState = state === null ? getInitialState() : state;
  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(projectState, action) : projectState;
}
