import {
  applyMiddleware,
  CombinedState,
  combineReducers,
  createStore,
  Reducer,
} from 'redux';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from './middlewares/logger';
import user, { IUserState } from '~/redux/user/user.reducer';
import editor from '~/redux/editor/editor.reducer';
import { IEditorState } from '~/redux/editor/types';
import project, { IProjectState } from '~/redux/project/project.reducer';

export type RootState = CombinedState<{
  user: IUserState;
  editor: IEditorState;
  project: IProjectState;
}>;

const combinedReducer: Reducer<RootState, any> = combineReducers({
  user,
  editor,
  project,
});

const bindMiddleware = (middleware: any) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');

    return composeWithDevTools(applyMiddleware(...middleware));
  } else {
    return applyMiddleware(...middleware);
  }
};

const reducer = (state: any, action: any) => {
  if (action.type === HYDRATE) {
    return { ...state, ...action.payload };
  } else {
    return combinedReducer(state, action);
  }
};

const initStore = () => {
  return createStore(
    reducer,
    bindMiddleware([loggerMiddleware, thunkMiddleware]),
  );
};

export const wrapper = createWrapper(initStore);
