import { SIGNIN, SIGNOUT } from '~/redux/user/user.types';
import decodeJwtToken from '~/utils/jwtToken';

/**
 * Basic redux setup
 */
export const signIn = (jwtToken: string) => {
  return {
    type: SIGNIN,
    jwtToken,
    tokenData: decodeJwtToken(jwtToken),
    isLoggedIn: true,
    subscriberStatus: 2,
  };
};

export const signOut = () => {
  //TODO Implement proper signout function
  // return {
  // 	type: SIGNOUT,
  // 	isLoggedIn: false,
  // };
  console.log('dummy function in user.actions.ts');
};
