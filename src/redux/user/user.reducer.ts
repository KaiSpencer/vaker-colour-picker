import { SIGNIN, SIGNOUT } from './user.types';
import { getLoginToken } from '~/utils/loginHelper';
import decodeJwtToken from '~/utils/jwtToken';

export interface IUserState {
  tokenData: {
    exp?: number;
    iat?: number;
    provider?: string;
    thirdPartyId?: string;
  };
  jwtToken: string | null;
  isLoggedIn: boolean;
  subscriberStatus: number;
}

enum subscriberStatus {
  CREATOR = 0,
  PROFESSIONAL = 1,
  PREMIUM = 2,
}

const initialEmptyState: IUserState = {
  tokenData: {},
  isLoggedIn: false,
  jwtToken: null,
  subscriberStatus: subscriberStatus.CREATOR,
};

const ACTION_HANDLERS: any = {
  [SIGNIN]: (
    state: any,
    { tokenData, jwtToken, isLoggedIn, subscriberStatus }: any,
  ) => ({
    ...state,
    tokenData,
    jwtToken,
    isLoggedIn,
    subscriberStatus,
  }),
};

const getInitialState = () => {
  const loginToken = getLoginToken();
  if (loginToken === null) {
    return initialEmptyState;
  }
  return {
    jwtToken: loginToken,
    tokenData: decodeJwtToken(loginToken),
    isLoggedIn: true,
    subscriberStatus: subscriberStatus.CREATOR,
  };
};

export default function userReducer(state = null, action: any) {
  const userState = state === null ? getInitialState() : state;

  const handler = ACTION_HANDLERS[action.type];

  return handler ? handler(userState, action) : userState;
}
