export const downloadBlob = async (
  UUID: string,
  token: string | null,
): Promise<any> => {
  if (!token) {
    return Promise.reject(new Error('No token provided'));
  }
  const axios = require('axios');

  const config = {
    method: 'get',
    url: `/render/video/${UUID}/download/`,
    responseType: 'blob',
    maxContentLength: Infinity,
    maxBodyLength: Infinity,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const response = await axios(config);
  return response;
};
