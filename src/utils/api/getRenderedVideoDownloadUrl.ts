import { getLoginToken } from '~/utils/loginHelper';
import axiosInterceptor from '~/utils/axiosInterceptor';
import isSuccessResponse from '~/utils/api/isSuccessResponse';

const getRenderedVideoDownloadUrl = async (videoUuid: string) => {
  const userToken = getLoginToken();

  const response = await axiosInterceptor.get(
    `${process.env.API_URL}/render/video/${videoUuid}/download`,
    {
      responseType: 'blob',
      headers: {
        Authorization: `Bearer ${userToken}`,
      },
    },
  );

  console.log('[Video Download] response', response);

  if (!isSuccessResponse(response)) {
    return {
      url: null,
      error: new Error(response.statusText),
    };
  }

  const downloadUrl = URL.createObjectURL(new Blob([response.data]));

  return {
    url: downloadUrl,
    error: null,
  };
};

export default getRenderedVideoDownloadUrl;
