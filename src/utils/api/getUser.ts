import useSWR from 'swr';

const getUser = () => {
  const { data, error } = useSWR(`/users/current`);

  return {
    user: data,
    error,
  };
};

export default getUser;
