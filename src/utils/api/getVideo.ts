import useSWR from 'swr';
import { ITranslationMatch, ITranslationWord } from '~/redux/editor/types';

export interface ApiVideo {
  NBest: [ITranslationMatch[]];
  words_grammatical: ITranslationWord[];
  uuid: string;
  userThirdPartyId: string;
  transcription: string;
  name: string;
  description: string;
  length: number;
  blobName: string;
  videoURL: string;
  uploadDate: Date;
  ffmpegMetadata?: {
    streams: Array<object>;
    format: {
      duration?: number;
    };
    chapters: Array<object>;
  };
  wavChannelData?: Array<number>;
}

const getVideo = (uuid: string) => {
  const { data, error } = useSWR<ApiVideo, Error>(`/videos/project/${uuid}`);

  return {
    video: data,
    error,
  };
};

export default getVideo;
