import useSWR from 'swr';
import { IVideoTranslation } from '~/redux/editor/types';

export interface IApiVideo {
  uuid: string;
  userThirdPartyId: string;
  transcription?: IVideoTranslation;
  name: string;
  description: string;
  length: number;
  blobName: string;
  videoURL: string;
  uploadDate: Date;
  ffmpegMetadata?: {
    streams: Array<object>;
    format: {
      duration?: number;
    };
    chapters: Array<object>;
  };
  wavChannelData?: Array<number>;
}

const getVideoData = (videoUuid: string) => {
  const { data, error } = useSWR<IApiVideo, Error>(
    `/videos/project/${videoUuid}`,
  );

  return {
    video: data,
    error,
  };
};

export default getVideoData;
