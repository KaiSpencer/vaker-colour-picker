const isSuccessResponse = (response: any): boolean =>
  response.status >= 200 && response.status < 300;

export default isSuccessResponse;
