const isUnauthorizedResponse = (response: any): boolean =>
  response?.statusCode === 401 || response?.message === 'Unauthorized';

export default isUnauthorizedResponse;
