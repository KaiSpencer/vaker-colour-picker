import axiosInterceptor from '~/utils/axiosInterceptor';
import { getLoginToken } from '~/utils/loginHelper';
import { RootState } from '~/redux/rootStore';
import isSuccessResponse from '~/utils/api/isSuccessResponse';

const renderVideo = async (
  videoUuid: string,
  subtitleStyles: RootState['editor']['styles'],
) => {
  const userToken = getLoginToken();

  const response = await axiosInterceptor.post(
    `${process.env.API_URL}/render/video/${videoUuid}`,
    {
      parameters: subtitleStyles,
    },
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
    },
  );

  console.log('[Video Render] response', response);

  if (isSuccessResponse(response)) {
    return {
      data: response.data,
      error: null,
    };
  }

  return {
    data: null,
    error: new Error(response.statusText),
  };
};

export default renderVideo;
