import { getLoginToken } from '~/utils/loginHelper';

const requestApi = (resource: RequestInfo, init?: RequestInit) => {
  const userToken = getLoginToken();

  const requestConfiguration: RequestInit = {
    ...(init ?? {}),
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${userToken}`,
      ...(init?.headers ?? {}),
    },
  };

  console.log('[API] Request:', resource);

  return fetch(`${process.env.API_URL}${resource}`, requestConfiguration).then(
    (res) => res.json(),
  );
};

export default requestApi;
