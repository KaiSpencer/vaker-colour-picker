import axiosInterceptor from '~/utils/axiosInterceptor';
import { getLoginToken } from '~/utils/loginHelper';

const uploadSrt = async (
  projectUuid: string,
  srtFileBlob: Blob,
  onProgress?: (event: any) => void,
) => {
  const userToken = getLoginToken();

  const formData = new FormData();
  formData.append('file', srtFileBlob);

  const response = await axiosInterceptor.post(
    `${process.env.API_URL}/render/upload/srt/${projectUuid}`,
    formData,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      onUploadProgress: onProgress,
    },
  );

  console.log('[SRT Upload] response', response);

  if (response.status >= 200 && response.status < 300) {
    return {
      data: response.data,
      error: null,
    };
  }

  return {
    data: null,
    error: new Error(response.statusText),
  };
};

export default uploadSrt;
