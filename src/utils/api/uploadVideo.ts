import axiosInterceptor from '~/utils/axiosInterceptor';
import { getLoginToken } from '~/utils/loginHelper';
import isSuccessResponse from '~/utils/api/isSuccessResponse';

const uploadVideo = async (file: File, onProgress: (event: any) => void) => {
  const userToken = getLoginToken();

  const formData = new FormData();
  formData.append('file', file);

  const response = await axiosInterceptor.post(
    `${process.env.API_URL}/videos/upload`,
    formData,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userToken}`,
      },
      onUploadProgress: onProgress,
    },
  );

  console.log('[Video Upload] response', response);

  if (isSuccessResponse(response)) {
    return {
      uuid: response.data,
      error: null,
    };
  }

  return {
    uuid: null,
    error: new Error(response.statusText),
  };
};

export default uploadVideo;
