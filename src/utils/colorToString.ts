export const colorToString = (color: any) => {
  let colorString =
    'rgb(' +
    color.rgb.r +
    ',' +
    color.rgb.g +
    ',' +
    color.rgb.b +
    ',' +
    color.rgb.a +
    ')';
  return colorString;
};

export const stringToColor = (color: any) => {
  let a = color.replace('rgb(', '').replace(')', '').split(',');
  let b = {
    r: parseInt(a[0]),
    g: parseInt(a[1]),
    b: parseInt(a[2]),
    a: parseFloat(a[3]),
  };

  return b;
};

export const rgba2hex = (orig) => {
  var a,
    isPercent,
    rgb = orig
      .replace(/\s/g, '')
      .match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i),
    alpha = ((rgb && rgb[4]) || '').trim(),
    hex = rgb
      ? (rgb[1] | (1 << 8)).toString(16).slice(1) +
        (rgb[2] | (1 << 8)).toString(16).slice(1) +
        (rgb[3] | (1 << 8)).toString(16).slice(1)
      : orig;

  if (alpha !== '') {
    a = alpha;
  } else {
    a = 1;
  }
  // multiply before convert to HEX
  a = ((a * 255) | (1 << 8)).toString(16).slice(1);
  // hex = hex + a; for alpha

  hex = hex.toUpperCase();

  if (hex.startsWith('#')) {
    return hex;
  } else {
    return '#' + hex;
  }
};
