import collect from 'collect.js';
import { NodeList } from 'subtitle';

export function convertSecondsToTime(seconds: number): string {
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds - hours * 3600) / 60);
  const secs = (seconds - hours * 3600 - minutes * 60).toFixed(3);

  return `${hours}:${minutes}:${secs}`;
}

export const downloadFile = (
  blobParts: BlobPart[],
  filename: string,
  extension: string,
) => {
  const fullName: string = `${filename}.${extension}`;
  const file: File = new File(blobParts, fullName, { type: 'text/plain' });
  const downloadLink = document.createElement('a');
  downloadLink.href = URL.createObjectURL(file);
  downloadLink.download = fullName;
  downloadLink.click();
};

export const generateVttFile = (
  blobParts: BlobPart[],
  filename: string,
): File => {
  return new File(blobParts, filename, { type: 'text/vtt' });
};

//This is going to get complicated as more extensions are added
//May be worth moving to a utils module but will need to get a reference
//to subtitleTracks
export const writeToBlob = (
  extension: string,
  withTimestamps: boolean,
  subtitles: NodeList,
  subtype: string = 'plain',
): Blob[] => {
  return collect(subtitles)
    .skip(1)
    .map(({ data }, index) => {
      /*
			Line1: track number
			Line2: start time (in seconds)
			Line3: end time (in seconds)
			Line4: text
			*/

      const line1 = `${index + 1}\n`;
      //TODO create a function that adds a line break if text exceeds 46 characters but only between words
      const line4 = `${data.text}\n\n`;

      const generateFullLine = (): string => {
        if (withTimestamps) {
          let line2 = `${convertSecondsToTime(data.start)} --> `;
          let line3 = `${convertSecondsToTime(data.end)}\n`;

          if (extension === 'srt') {
            line2 = line2.replaceAll('.', ',');
            line3 = line3.replaceAll('.', ',');
          }

          return line1 + line2 + line3 + line4;
        } else {
          return line1 + line4;
        }
      };

      return new Blob([generateFullLine()], { type: `text/${subtype}` });
    })
    .when(extension === 'webvtt', (collection) =>
      collection.splice(0, 0, new Blob(['WEBVTT'])),
    )
    .toArray() as Blob[];
};
