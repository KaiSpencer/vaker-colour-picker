import { NodeCue, NodeList, stringifySync } from 'subtitle';
import collect from 'collect.js';
import {
  TranslationWordTypeEnum,
  ITranslationWord,
} from '~/redux/editor/types';

const generateVttNodeList = (rawData: Array<ITranslationWord>): NodeList => {
  // TODO: Find the correct format of these durations
  const TIME_MULTIPLIER = 10000;

  const generateCueNode = (
    words: Array<ITranslationWord>,
    startOffset: number,
  ): NodeCue => {
    const duration: number = words
      .map(({ Duration: duration }): number =>
        parseFloat(duration?.toString() ?? '0'),
      )
      .reduce((accumulator, currentValue) => accumulator + currentValue);

    const text = collect(words).pluck('Word').join(' ');

    return {
      type: 'cue',
      data: {
        text,
        start: startOffset,
        end: (startOffset + duration) / TIME_MULTIPLIER,
      },
    };
  };

  const nodeList: NodeList = [
    {
      type: 'header',
      data: 'WebVTT',
    },
  ];

  const sectionParts = new Array<ITranslationWord>();
  let currentOffset = 0;

  for (const entry of [...rawData, { type: TranslationWordTypeEnum.DIVIDER }]) {
    if (entry.type === TranslationWordTypeEnum.DIVIDER) {
      if (sectionParts.length === 0) {
        continue;
      }

      const [firstSectionPart] = sectionParts;
      const lastSectionPart = collect(sectionParts).last();

      currentOffset = Number(firstSectionPart.Offset) / TIME_MULTIPLIER;

      const cueNode = generateCueNode(sectionParts, currentOffset);
      cueNode.data.start = currentOffset;
      cueNode.data.end =
        (Number(lastSectionPart.Offset) + Number(lastSectionPart.Duration)) /
        TIME_MULTIPLIER;

      if (!cueNode.data.end || isNaN(cueNode.data.end)) {
        const firstEntryOfNextCueNode = collect(rawData).firstWhere(
          'id',
          Number(entry.id) + 1,
        );
        cueNode.data.end = firstEntryOfNextCueNode
          ? Number(firstEntryOfNextCueNode?.Offset) / TIME_MULTIPLIER
          : currentOffset + 1000;
      }

      nodeList.push(cueNode);

      currentOffset = cueNode.data.end;
      sectionParts.splice(0);
    } else {
      sectionParts.push(entry);
    }
  }

  if (sectionParts.length > 0) {
    const cueNode = generateCueNode(sectionParts, currentOffset);

    nodeList.push(cueNode);
  }

  return nodeList;
};

const generateSrtUsingSubtitles = (rawData: Array<ITranslationWord>) => {
  const nodeList = generateVttNodeList(rawData);

  const vttData = stringifySync(nodeList, { format: 'WebVTT' });

  return vttData.replace(/^WebVTT/, 'WEBVTT');
};

export default generateSrtUsingSubtitles;

export { generateVttNodeList };
