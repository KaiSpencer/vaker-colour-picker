import { NextRouter } from 'next/router';
import { logout } from '~/utils/loginHelper';
import { Dispatch } from 'redux';
import { signOut } from '~/redux/user/user.actions';

const handleUnauthorizedResponse = (dispatcher: Dispatch<any>) => {
  // Remove login cookie
  logout();

  // Update stores
  dispatcher(signOut());
};

export default handleUnauthorizedResponse;
