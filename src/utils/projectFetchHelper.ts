import axios, { AxiosInstance, AxiosResponse } from 'axios';

const uploadEndpoint = `${process.env.API_URL}/videos/upload`;
const getProjectEndpoint = `${process.env.API_URL}/videos/project/`; // Needs {uuid} param
const fetchProjectDetails: string = `${process.env.API_URL}/videos/fetch-projects/`;
const patchProjectEndpoint = `${process.env.API_URL}/videos/patch-video/`;

export async function uploadVideoFile(
  file: File,
  user: any,
): Promise<AxiosResponse<any>> {
  const secureAxios = getSecureAxiosInstance(user);

  // Uploads file to database
  const URL_STRING = uploadEndpoint;
  const fileFormData = new FormData();
  fileFormData.append('file', file);

  const res: AxiosResponse = await secureAxios.post(URL_STRING, fileFormData);
  return res;
}

export async function getProjectDetails(
  user: any,
  filter: Array<string>,
): Promise<AxiosResponse<any>> {
  const secureAxios = getSecureAxiosInstance(user);
  filter.push('uuid');
  const params = filter.toString();
  console.log('jwt: ', user.tokenData.thirdPartyId);
  const URL_STRING =
    fetchProjectDetails + user.tokenData.thirdPartyId + '/' + params;
  const res = await secureAxios.get(URL_STRING);
  return res;
}

export async function getProject(
  uuid: string,
  user: any,
): Promise<AxiosResponse<any>> {
  const secureAxios = getSecureAxiosInstance(user);
  const URL_STRING = getProjectEndpoint + uuid;

  const res: AxiosResponse = await secureAxios.get(URL_STRING);
  return res;
}

export async function patchProject(
  user: any,
  data: any,
): Promise<AxiosResponse<any>> {
  const secureAxios = getSecureAxiosInstance(user);
  const URL_STRING = patchProjectEndpoint;

  const res: AxiosResponse = await secureAxios.patch(URL_STRING, data);
  return res;
}

const getSecureAxiosInstance = (user: any): AxiosInstance => {
  return axios.create({
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${user.jwtToken}`,
    },
  });
};
