import CookieStorage from '~/utils/CookieStorage';

const TOKEN_COOKIE_NAME = 'selectedProject';

export function getSelectedProject(): string | null {
  return CookieStorage.getValue(TOKEN_COOKIE_NAME);
}
