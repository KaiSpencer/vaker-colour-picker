module.exports = {
	// Enable JIT for a faster development experience:
	// https://tailwindcss.com/docs/just-in-time-mode
	mode: 'jit',
	// Inform Tailwind of where our classes will be defined:
	// https://tailwindcss.com/docs/optimizing-for-production
	purge: ['./src/**/*.{js,ts,jsx,tsx}'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		screens: {
			xxs: '380px',
			// => @media (min-width: 380px) { ... }

			xs: '450px',
			// => @media (min-width: 380px) { ... }

			sm: '640px',
			// => @media (min-width: 640px) { ... }

			md: '768px',
			// => @media (min-width: 768px) { ... }

			lg: '1024px',
			// => @media (min-width: 1024px) { ... }

			// Created specifically because Leon wanted this breakpoint for the HelperTools
			ht: '1140px',

			xl: '1280px',
			// => @media (min-width: 1280px) { ... }

			'2xl': '1536px',
			// => @media (min-width: 1536px) { ... }
		},
		extend: {
			fontFamily: {
				manrope: ['Manrope'],
			},
			gridTemplateColumns: {
				17: 'repeat(17, minmax(0, 1fr))',
			},
			boxShadow: {
				editor: '0px 0px 20px 4px rgba(0, 0, 0, 0.05)',
			},
			colors: {
				main: '#43597D',
				font1: '#333539',
				font2: '#929292',
				fontIcon: '#D5D5D5',
				bg1: '#FCFCFC',
				bg2: '#FFFFFF',
				bg3: '#F5FAFE',
				blueDashboard: '#8AE0FB',
			},
		},
	},
	variants: {
		extend: {},
		scrollbars: ['rounded'],
	},
	// Add some basic Tailwind plugins to add additional features:
	plugins: [
		require('@tailwindcss/typography'),
		require('@tailwindcss/aspect-ratio'),
		require('@tailwindcss/forms'),
		require('tailwind-scrollbar'),
	],
};
